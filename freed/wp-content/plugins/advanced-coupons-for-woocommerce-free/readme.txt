=== Advanced Coupons for WooCommerce Coupons ===
Contributors: jkohlbach, RymeraWebCo, Rymera01
Donate link:
Tags: woocommerce coupons, woocommerce bogo, woocommerce url coupons, advanced coupons, woocommerce coupon plugin
Requires at least: 5.0
Tested up to: 5.5.3
Stable tag: 1.3.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Smart WooCommerce coupons with Advanced Coupons for WooCommerce. Add extra WooCommerce coupon features like WooCommerce BOGO, url coupons & more.

== DESCRIPTION ==

Get extra smart WooCommerce coupons features with Advanced Coupons for WooCommerce! The free plugin that makes your WooCommerce coupons better.

= Best & Smartest WooCommerce Coupons Plugin =

Every store owner deserves to have the best features on their WooCommerce coupons - it's the key to marketing your store better!

That's why we built this 100% free WooCommerce coupon plugin.

**WOOCOMMERCE ADVANCED COUPONS FREE VERSION**

***<a href="https://advancedcouponsplugin.com/?utm_source=wprepo&utm_medium=link&utm_campaign=acfwf" rel="friend">Advanced Coupons for WooCommerce</a> (Free Version)*** gives you extra features on your WooCommerce coupons so they can market your store better.

= ADVANCED COUPONS MAKES YOUR WOOCOMMERCE COUPONS BETTER =

We've made WooCommerce coupons better so you can:

1. Run WooCommerce BOGO deals (amazing new coupon type)
1. Protect against accidental discount usage with Cart Conditions (coupon rules)
1. Organize your WooCommerce coupons with coupon categories
1. Apply coupons with a URL easily (WooCommerce URL coupons)
1. Restrict WooCommerce coupons by user role
1. Show WooCommerce coupons on the quick order preview box

> <strong>ADVANCED COUPONS PREMIUM ADD-ON</strong><br />
> This plugin is the free version of the highly rated Advanced Coupons Premium plugin. The premium version adds EVEN MORE WooCommerce coupon enhancements and features.<br /><br />Click here to compare features and purchase the <a href="https://advancedcouponsplugin.com/?utm_source=wprepo&utm_medium=link&utm_campaign=acfwf" rel="friend">Advanced Coupons for WooCommerce Premium Add-on</a>.

Here are some FREE VERSION features at a glance:

= WooCommerce BOGO Deals =

***WooCommerce BOGO coupons***, otherwise known as Buy One Get One deals, are VERY common in the offline world. So why can't you run those kinds of deals on your WooCommerce store as well?!

Now you can – Advanced Coupons WooCommerce BOGO coupons let you easily run a BOGO deal (Buy One Get One deal) on your store.

It's a much more flexible WooCommerce coupon type compared to straight discounts that your customers will love! It's also much more profitable for your store as well when compared to standard discount coupons (read on).

= Protect Your Profits With Cart Conditions (Coupon Rules) =

WooCommerce coupon misuse is a huge problem for store owners worldwide. Smart store owners are using coupon rules, what we call "Cart Conditions", to reduce this significantly. By setting the right conditions on your WooCommerce coupons you can control exactly when they are allowed to be used.

For example:
* Only apply a coupon when products from a particular category are in the cart
* Only apply a coupon after a subtotal has been reached
* Only apply a coupon when a specific product is in the cart in a specific quantity
* Only apply a coupon on their first use (check their total spend is zero!)

And there's dozens more! You can also combine and mix and match cart conditions to create more specific coupon rules.

= WooCommerce URL Coupons (Apply WooCommerce Coupons With A Link) =

***WooCommerce URL coupons*** let you give your customers a coupon link to apply a coupon.

When they click the coupon URL it will apply the coupon to the cart so they don't have to type it.

Use WooCommerce URL coupons on buttons, images and text. Show them on your sidebar, ads, email marketing, blog posts – basically wherever you can put a link you can put a URL coupon!

= Smart Coupons Categories =

If you've been running a store for a while chances are you have dozens, if not hundreds, of WooCommerce coupons!

WooCommerce Advanced Coupons lets you get smart about how you organise your WooCommerce coupons by giving you the ability to put them into coupon categories. 

You can name categories anything, such as "Support Coupons", "Site wide coupon deals", "WooCommerce BOGO deals", "Affiliate Coupons", "Partner Offers" and more.

= Restrict WooCommerce Coupons By Role =

If you run a store with multiple user roles, (for example if you're using [WooCommerce Wholesale Prices](https://wordpress.org/plugins/woocommerce-wholesale-prices/) and have wholesale customers to worry about, you will be please to know you can now create coupons that are just for those users.

Likewise, you can exclude certain user roles from being able to use WooCommerce coupons. It's very flexible!

= Show WooCommerce Coupons On Order Preview Popup =

The new quick order preview button on the Orders List in WooCommerce is great, but it doesn't show coupons that were used on the order.

Advanced Coupons will add a list of the WooCommerce coupons that were used on an order so you can quickly see without having to go into the Order edit screen. 

These small but helpful enhancements speed up your processes dramatically. We're always looking for ways to improve people's workflow.

= Advanced Coupons Is Compatible With Other Plugins =

WooCommerce Advanced Coupons is compatible with lots of complementary plugins. Hundreds of shipping and payment gateways, WooCommerce Currency Switcher by Aelia (even with our free Advanced Coupons plugin!), Wholesale Suite, plus loads more.

If you want WooCommerce coupons that are advanced AND compatible with all the existing tools you are using then Advanced Coupons is the tool for you.

= The Best WooCommerce Coupons Plugin (3 Reasons) =

When it comes to marketing your store, standard WooCommerce coupons are too underpowered to be useful. That why you need extra WooCommerce coupons features.

The mission of Advanced Coupons is to be the undisputed BEST WooCommerce coupons features extender on the market. Here are 3 top reasons you need Advanced Coupons:

**Reason #1: WooCommerce BOGO coupons are more PROFITABLE (It's simple math!)**

Let me illustrate why WooCommerce BOGO coupons make more profit by comparing a “30% off deal” offer vs. a “Buy 2 get 1 free” offer.

*Standard 30% off coupon deal*
- 1x $100 pair of Jeans (discounted to $70 after 30% off)
- Minus $30 cost price
- *$40 profit margin*

Standard WooCommerce coupons for a “30% off deal” would take 30% off the revenue meaning its $70 profit margin would get reduced to $40 profit margin.

*WooCommerce BOGO (Buy 2 get 1 free) deal*
- 3x pairs of Jeans is $300
- Minus $90 cost price ($30 cost price each)
- Minus $100 as 1x product is free for the deal
- *$110 profit margin*

Each time the customer takes the WooCommerce BOGO deal you make $110 profit. I’ll take that any day of the week.

**Reason #2: Cart Conditions save your money**

Have you ever had a customer use a coupon they weren't meant to? Did they get more discount than they were suppose to? 

Cart conditions are like a set of rules your customer needs to abide by before they can apply a coupon. And if they apply the coupon and then later become ineligible, the cart conditions will remove that coupon.

Gone are the days of losing out by having to put up with customers looking to scrape something extra. You can set up complex rules for your WooCommerce Coupons to abide by which, over time, will save you bucket loads.

**Reason #3: A WooCommerce coupons plugin dedicated to your success!**

There's lots of other "features" that I could give you as reasons of why this is the best WooCommerce coupons extender plugin. But really there's only one more reason you should care about.

Unlike many others WooCommerce coupons plugins on the market our company, Rymera Web Co, is a professional WooCommerce extension company.

We are 100% dedicated to helping STORE OWNERS. These are people just like you who are out there trying to make a living selling online via WooCommerce.

We believe in this industry so much that we've dedicated our entire company's existence to helping WooCommerce store owners.

Our mission is "To help store owners succeed with professional-grade tools that help them grow". Installing this *WooCommerce coupons* tool is just the first step in us helping you get to the next level.

== Installation ==

1. Upload the `advanced-coupons-for-woocommerce-free/` folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Visit our <a href="https://advancedcouponsplugin.com/knowledgebase/getting-started-features-overview/?utm_source=wprepo&utm_medium=link&utm_campaign=acfwf" rel="friend">getting started guide</a> to help you get up to speed

== Frequently asked questions ==

We have hundreds of guides and frequently asked questions answered in our online <a href="https://advancedcouponsplugin.com/knowledge-base/?utm_source=wprepo&utm_medium=link&utm_campaign=acfwf" rel="friend">knowledge base</a>.

= Who should use Advanced Coupons? =

Advanced Coupons is perfect for store owners using WooCommerce that want more advanced features on their WooCommercec upon. If you need to run more interesting deals like BOGO deals and you want features like being able to restrict when and how a coupon is applied then Advanced Coupons is for you.

= Do I need to have coding skills to use Advanced Coupons? =

Absolutely not. You can create WooCommerce coupons easily through the normal interface and there is nothing complex about the plugin that would require you to have coding skills.

That said, there are a few features that are designed specifically to help developers and web designers deliver complex coupons for their store owner clients. But you still don't need heaps of extra knowledge to make them work. We're dedicated to being the friendliest and best WooCommerce coupons plugin.

= What extra things can I do with my WooCommerce coupons with Advanced Coupons? =

Advanced Coupons lets you create lots of new and interesting WooCommerce deals. Here are some extra features you can expect on your WooCommerce coupons:

* WooCommerce BOGO coupons
* Add Products on apply
* Cart conditions (coupon rules)
* Schedule coupons
* Auto apply coupons
* URL coupons
* Shipping coupons
* Run a loyalty program

= What kinds of things can I test for with Cart Conditions (Coupon Rules)? =

You can use Cart Conditions to test the current customer's shopping cart, their past interactions with your store and things about their user account. All of this happens as the customer attempts to apply the WooCommerce coupons to their cart.

Cart Conditions are also compatible with our Premium features for auto applying coupons and showing 1-click apply notices.

* Cart Condition: Product Category Exists In Cart
* Cart Condition: Customer Logged In Status
* Cart Condition: Allowed Customer User Role
* Cart Condition: Disallowed Customer User Role
* Cart Condition: Cart Quantity
* Cart Condition: Cart Subtotal
* Cart Condition: Product Quantity In The Cart
* Cart Condition: Custom Taxonomy Exists In The Cart
* Cart Condition: Within Hours After Customer Registered
* Cart Condition: Within Hours After Customer Last Order
* Cart Condition: Custom User Meta
* Cart Condition: Custom Cart Item Meta
* Cart Condition: Total Customer Spend
* Cart Condition: Has Ordered Before
* Cart Condition: Shipping Zone And Region

Some of these are only available in the premium version, but many are included in the free version.

= Can I run WooCommerce BOGO deals across multiple products or product categories? =

In the free version you can only run WooCommerce BOGO deals with a single product as the trigger and a single product as the apply product. They don't have to be the same product.

In the premium version you get much more advanced WooCommerce BOGO functionality with the ability to apply BOGO deals across multiple products as the trigger and apply type and even whole product categories.

* BOGO Deal Coupons (Specific Products)
* BOGO Deal Coupons (Any Combination Of Products)
* BOGO Deal Coupons (Product Categories)

Combined, these different BOGO features can make running WooCommerce BOGO deals across large amounts of products a breeze.

= Can I customise the message a user sees if a coupon fails to apply? =

Yes, in Advanced Coupons you can set what we call a Custom Non-Qualifying Message. This is shown to the customer when a coupon is not applied. You'll find it under the Advanced Settings on Cart Conditions when you edit a coupon.

= Can I organize my WooCommerce coupons into categories?  =

Yes, coupon categories let you organise you coupons into distinct categories which makes it much simpler to manage your coupons when you have been running your store for a long period.

We suggest creating as many WooCommerce coupons categories as you need to separate the different types of coupons you and your team create.

= Is Advanced Coupons translation ready? =

Yes, Advanced Coupons for WooCommerce is fully i18n compliant and ready for translation.

= I'd like access to all features, how can I get them? =

Purchasing an Advanced Coupons Premium license gives you access to the full feature set of Advanced Coupons. This includes automatic updates, priority support, and more!

<a href="https://advancedcouponsplugin.com/pricing/?utm_source=wprepo&utm_medium=link&utm_campaign=acfwf" rel="friend">Click here to compare the features and buy Advanced Coupons Premium</a>

== Screenshots ==

1. BOGO deals are the best WooCommerce Coupons
2. WooCommerce coupon roles
3. Settings

== Notes ==

Advanced Coupons is absolutely, positively the best <a href="https://advancedcouponsplugin.com/?utm_source=wprepo&utm_medium=link&utm_campaign=acfwf" rel="friend" title="Best WooCommerce Coupons Plugin">WooCommerce coupons plugin</a> on the market. It is both easy and powerful.

We took the pain out of creating advanced coupon deals in WooCommerce and made it easy. Check out all <a href="https://advancedcouponsplugin.com/pricing/?utm_source=wprepo&utm_medium=link&utm_campaign=acfwf" rel="friend">Advanced Coupons features</a>.

Also, I'm the founder of <a href="https://wholesalesuiteplugin.com/?utm_source=wprepo&utm_medium=link&utm_campaign=acfwf" rel="friend">Wholesale Suite</a>, the biggest wholesale solution for WooCommerce. I'm obsessed with helping store owners grow their stores and I hope you enjoy using Advanced Coupons.

Thanks,
Josh Kohlbach

== Changelog ==

= 1.3.4 =
* Bug Fix: BOGO Deals apply table data is empty for combination products and product categories types after version 1.3.3

= 1.3.3 =
* Improvement: WooCommerce 4.8 compatibility
* Improvement: Add French Translation (props <a href="https://wordpress.org/support/users/madmax4ever/" rel="nofollow">@madmax4ever</a>)
* Bug Fix: Fix deprecated error with WooCommerce Admin Note function due to changes in WooCommerce 4.8
* Bug Fix: Can't save comma as decimal separator on cart Sub-total condition

= 1.3.2 =
* Improvement: Add method to force URL coupon even when excluded with an auto apply coupon
* Improvement: Update and improve translation file

= 1.3.1 =
* Bug Fix: Coupon with combination of BOGO and ADD PRODUCT with the same product name has incorrect discount

= 1.3 =
* Feature: Integration: WPML compatibility
* Improvement: Add filter to all ACFWP upsell links
* Improvement: Big code improvements
* Bug Fix: Coupon code with Ampersand '&' does not apply to guest users
* Bug Fix: PHP Notice error when ACFWF and WooCommerce Zapier is both activated

= 1.2.3 =
* Improvement: WC 4.4 compatibility
* Improvement: WP 5.5 compatibility
* Improvement: Code improvements

= 1.2.2 =
* Bug Fix: Minor code issues with WC Admin.

= 1.2.1 =
* Bug Fix: Compatibility with User Switching plugin
* Bug Fix: Add placeholder values for BOGO and URL coupon settings

= 1.2 =
* Feature: Add Suggestions and actions to WC Inbox
* Feature: WooCommerce Subscriptions Integration
* Improvement: Allow quantity check for product categories cart condition
* Improvement: Code improvements
* Bug Fix: Able to set negative value in a BOGO specific product discounts
* Bug Fix: Premium plugin upsell notice still showing when ACFWP added but not installed

= 1.1 =
* Improvement: Add getting started guide on activation
* Improvement: Fix naming on shipping zone/region cart condition
* Bug Fix: Duplicate BOGO notices in certain environments

= 1.0 =
* Initial release

== Upgrade notice ==

There is a new version of Advanced Coupons for WooCommerce Free available.
