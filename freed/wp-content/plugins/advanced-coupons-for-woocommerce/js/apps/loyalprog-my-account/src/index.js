/* global jQuery */
import "./assets/styles/index.scss";
import { my_account_events } from "./my_account";

jQuery( document ).ready( function($) {

    my_account_events($);
});