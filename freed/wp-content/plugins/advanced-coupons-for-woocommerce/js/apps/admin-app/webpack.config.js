const path = require('path');
const UglifyJSPlugin = require( 'uglifyjs-webpack-plugin' );
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const {
    NODE_ENV = 'production'
} = process.env;

const externals = {
    'react': { this: [ 'acfwpElements' , 'element' ] }
};

module.exports = {
    entry: './src/index.tsx',
    mode: NODE_ENV,
    devtool: 'source-map',
    watch: NODE_ENV === 'development',
    externals,
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js' ]
    },
    output: {
        filename: 'admin-app.js',
        path: path.resolve(__dirname, 'dist')
    },
    optimization: {
        minimizer: [
            new OptimizeCSSAssetsPlugin({})
        ]
    },
    plugins: [
        new UglifyJSPlugin({sourceMap : true}),
        new MiniCssExtractPlugin({ filename: 'admin-app.css' })
    ]
};
