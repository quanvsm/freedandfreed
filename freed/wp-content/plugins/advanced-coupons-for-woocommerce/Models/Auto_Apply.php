<?php
namespace ACFWP\Models;

use ACFWP\Abstracts\Abstract_Main_Plugin_Class;

use ACFWP\Interfaces\Model_Interface;
use ACFWP\Interfaces\Initiable_Interface;

use ACFWP\Models\Objects\Advanced_Coupon;

use ACFWP\Helpers\Plugin_Constants;
use ACFWP\Helpers\Helper_Functions;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Model that houses the logic of the Auto_Apply module.
 *
 * @since 2.0
 */
class Auto_Apply implements Model_Interface , Initiable_Interface {

    /*
    |--------------------------------------------------------------------------
    | Class Properties
    |--------------------------------------------------------------------------
    */

    /**
     * Property that holds the single main instance of URL_Coupon.
     *
     * @since 2.0
     * @access private
     * @var Auto_Apply
     */
    private static $_instance;

    /**
     * Model that houses all the plugin constants.
     *
     * @since 2.0
     * @access private
     * @var Plugin_Constants
     */
    private $_constants;

    /**
     * Property that houses all the helper functions of the plugin.
     *
     * @since 2.0
     * @access private
     * @var Helper_Functions
     */
    private $_helper_functions;

    /**
     * Coupon base url.
     *
     * @since 2.0
     * @access private
     * @var string
     */
    private $_coupon_base_url;




    /*
    |--------------------------------------------------------------------------
    | Class Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Class constructor.
     *
     * @since 2.0
     * @access public
     *
     * @param Abstract_Main_Plugin_Class $main_plugin      Main plugin object.
     * @param Plugin_Constants           $constants        Plugin constants object.
     * @param Helper_Functions           $helper_functions Helper functions object.
     */
    public function __construct( Abstract_Main_Plugin_Class $main_plugin , Plugin_Constants $constants , Helper_Functions $helper_functions ) {

        $this->_constants        = $constants;
        $this->_helper_functions = $helper_functions;

        $main_plugin->add_to_all_plugin_models( $this );
        $main_plugin->add_to_public_models( $this );

    }

    /**
     * Ensure that only one instance of this class is loaded or can be loaded ( Singleton Pattern ).
     *
     * @since 2.0
     * @access public
     *
     * @param Abstract_Main_Plugin_Class $main_plugin      Main plugin object.
     * @param Plugin_Constants           $constants        Plugin constants object.
     * @param Helper_Functions           $helper_functions Helper functions object.
     * @return Auto_Apply
     */
    public static function get_instance( Abstract_Main_Plugin_Class $main_plugin , Plugin_Constants $constants , Helper_Functions $helper_functions ) {

        if ( !self::$_instance instanceof self )
            self::$_instance = new self( $main_plugin , $constants , $helper_functions );

        return self::$_instance;

    }




    /*
    |--------------------------------------------------------------------------
    | Auto_Apply implementation
    |--------------------------------------------------------------------------
    */

    /**
     * Auto apply single coupon.
     * 
     * @since 2.0
     * @access public
     * 
     * @param Advanced_Coupon $coupon    Advanced coupon object.
     * @param WC_Discounts    $discounts WooCommerce discounts object.
     */
    private function _auto_apply_single_coupon( $coupon , $discounts ) {

        if ( ! $this->_validate_auto_apply_coupon( $coupon ) ) return;

        if ( is_wp_error( $discounts->is_coupon_valid( $coupon ) ) ) {
            do_action( 'acfw_auto_apply_coupon_invalid', $coupon );
            return;
        }

        if ( in_array( $coupon->get_code() , \WC()->cart->get_applied_coupons() ) )
            return;
       
        $check = \WC()->cart->apply_coupon( $coupon->get_code() );
    }

    /**
     * Validate coupon for auto apply.
     * 
     * @since 2.0
     * @access private
     * 
     * @param WC_Coupon $coupon WooCommerce coupon object.
     * @return bool True if valid, false otherwise.
     */
    private function _validate_auto_apply_coupon( $coupon ) {

        if ( ! $coupon->get_id() || get_post_status( $coupon->get_id() ) !== 'publish' ) return false;
        
        // ACFWP-160 disable auto apply for coupons with usage limits.
        if ( $coupon->get_usage_limit() || $coupon->get_usage_limit_per_user() ) return false;

        // disable auto apply for coupons that has value for allowed emails meta.
        $allowed_emails = get_post_meta( $coupon->get_id() , 'customer_email' , true );
        if ( is_array( $allowed_emails ) && ! empty( $allowed_emails ) ) return false;

        return true;
    }

    /**
     * Implement auto apply coupons.
     * 
     * @since 2.0
     * @access public
     */
    public function implement_auto_apply_coupons() {

        $auto_coupons = get_option( $this->_constants->AUTO_APPLY_COUPONS , array() );

        if ( ! is_array( $auto_coupons ) || empty( $auto_coupons ) )
            return;

        $applied_coupons = \WC()->cart->get_applied_coupons();
        $discounts       = new \WC_Discounts( \WC()->cart );

        foreach ( $auto_coupons as $coupon_id ) {

            if ( get_post_type( $coupon_id ) !== 'shop_coupon' ) continue;

            $coupon = new Advanced_Coupon( $coupon_id );

            if ( in_array( $coupon->get_code() , $applied_coupons ) ) continue;
        
            $this->_auto_apply_single_coupon( $coupon , $discounts );
        }
    }

    /**
     * Force create session when cart is empty and there are coupons to be auto applied.
     * 
     * @since 2.4
     * @access public
     */
    public function force_create_cart_session() {

        // function should only run on either cart or checkout pages.
        if ( ! is_cart() && ! is_checkout() ) return;

        $auto_coupons = get_option( $this->_constants->AUTO_APPLY_COUPONS , array() );
        
        // create session.
        if ( \WC()->cart->is_empty() && is_array( $auto_coupons ) && ! empty( $auto_coupons ) )
            \WC()->session->set_customer_session_cookie( true );
    }

    /**
     * Hide the "remove coupon" link in cart totals table for auto applied coupons.
     * 
     * @since 2.0
     * @access public
     * 
     * @param string    $coupon_html WC coupon cart total table row html markup.
     * @param WC_Coupon $coupon      Current coupon loaded WC_Coupon object.
     * @return string Filtered WC coupon cart total table row html markup.
     */
    public function hide_remove_coupon_link_in_cart_totals( $coupon_html , $coupon ) {

        if ( ! $this->_validate_auto_apply_coupon( $coupon ) ) return $coupon_html;

        $auto_coupons = get_option( $this->_constants->AUTO_APPLY_COUPONS , array() );

        if ( is_array( $auto_coupons ) && ! empty( $auto_coupons ) && in_array( $coupon->get_id() , $auto_coupons ) )
            $coupon_html = preg_replace('#<a.*?>.*?</a>#i', '' , $coupon_html );

        return $coupon_html;
    }
    
    /**
     * Clear auto apply cache.
     * 
     * @since 2.0
     */
    private function _clear_auto_apply_cache() {

        update_option( $this->_constants->AUTO_APPLY_COUPONS , array() );
    }

    /**
     *  Rebuild auto apply cache.
     * 
     * @since 2.0
     * @return array List of auto apply coupon ids.
     */
    private function _rebuild_auto_apply_cache() {

        $auto_coupons = get_option( $this->_constants->AUTO_APPLY_COUPONS , array() );
        $verified     = array_filter( $auto_coupons , function( $c ) {
            return get_post_type( $c ) == 'shop_coupon' && get_post_status( $c ) == 'publish';
        } );

        update_option( $this->_constants->AUTO_APPLY_COUPONS , array_unique( $verified ) );
        return $verified;
    }

    /**
     * Render clear auto apply cache settings field.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array $value Field value data.
     */
    public function render_rebuild_auto_apply_cache_setting_field( $value ) {

        $spinner_image = $this->_constants->IMAGES_ROOT_URL . 'spinner.gif';

        include $this->_constants->VIEWS_ROOT_PATH . 'settings' . DIRECTORY_SEPARATOR . 'view-render-rebuild-auto-apply-cache-settting-field.php';
    }

    /**
     * AJAX rebuild auto apply cache.
     * 
     * @since 2.0
     * @access public
     */
    public function ajax_rebuild_auto_apply_cache() {

        if ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX )
            $response = array( 'status' => 'fail' , 'error_msg' => __( 'Invalid AJAX call' , 'advanced-coupons-for-woocommerce' ) );
        elseif ( ! isset( $_POST[ 'nonce' ] ) || ! wp_verify_nonce( $_POST[ 'nonce' ] , 'acfw_rebuild_auto_apply_cache' ) || ! current_user_can( 'manage_woocommerce' ) )
            $response = array( 'status' => 'fail' , 'error_msg' => __( 'You are not allowed to do this' , 'advanced-coupons-for-woocommerce' ) );
        else {

            if ( $_POST[ 'type' ] == 'clear' ) {

                $this->_clear_auto_apply_cache();
                $response = array(
                    'status' => 'success',
                    'message' => __( 'Auto apply coupons cache have been cleared successfully.' , 'advanced-coupons-for-woocommerce' )
                );

            } else {

                $verified = $this->_rebuild_auto_apply_cache();
                $response = array(
                    'status'  => 'success',
                    'message' => sprintf( __( 'Auto apply coupons cache has been rebuilt successfully. %s coupon(s) have been validated.' , 'advanced-coupons-for-woocommerce' ) , count( $verified ) )
                );
            }            
        }

        @header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
        echo wp_json_encode( $response );
        wp_die();
    }




    /*
    |--------------------------------------------------------------------------
    | Fulfill implemented interface contracts
    |--------------------------------------------------------------------------
    */

    /**
     * Execute codes that needs to run plugin activation.
     *
     * @since 2.0
     * @access public
     * @implements ACFWP\Interfaces\Initiable_Interface
     */
    public function initialize() {

        add_action( 'wp_ajax_acfw_rebuild_auto_apply_cache' , array( $this , 'ajax_rebuild_auto_apply_cache' ) );
    }

    /**
     * Execute Auto_Apply class.
     *
     * @since 2.0
     * @access public
     * @inherit ACFWP\Interfaces\Model_Interface
     */
    public function run() {

        add_action( 'woocommerce_admin_field_acfw_rebuild_auto_apply_cache' , array( $this , 'render_rebuild_auto_apply_cache_setting_field' ) );

        if ( ! \ACFWF()->Helper_Functions->is_module( Plugin_Constants::AUTO_APPLY_MODULE ) )
            return;
            
        add_action( 'wp' , array( $this , 'force_create_cart_session' ) );
        add_action( 'woocommerce_check_cart_items' , array( $this , 'implement_auto_apply_coupons' ) );
        add_action( 'woocommerce_before_checkout_form' , array( $this , 'implement_auto_apply_coupons' ) );
        add_filter( 'woocommerce_cart_totals_coupon_html' , array( $this , 'hide_remove_coupon_link_in_cart_totals' ) , 10 , 2 );
    }

}
