<?php
namespace ACFWP\Models;

use ACFWP\Abstracts\Abstract_Main_Plugin_Class;

use ACFWP\Interfaces\Model_Interface;
use ACFWP\Interfaces\Initiable_Interface;

use ACFWP\Models\Objects\Advanced_Coupon;
use ACFWP\Models\Objects\Loyalty_Programs_Settings;


use ACFWP\Helpers\Plugin_Constants;
use ACFWP\Helpers\Helper_Functions;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Model that houses the logic of extending the coupon system of woocommerce.
 * It houses the logic of handling coupon url.
 * Public Model.
 *
 * @since 2.0
 */
class Module_Settings implements Model_Interface , Initiable_Interface {

    /*
    |--------------------------------------------------------------------------
    | Class Properties
    |--------------------------------------------------------------------------
    */

    /**
     * Property that holds the single main instance of URL_Coupon.
     *
     * @since 2.0
     * @access private
     * @var Module_Settings
     */
    private static $_instance;

    /**
     * Model that houses all the plugin constants.
     *
     * @since 2.0
     * @access private
     * @var Plugin_Constants
     */
    private $_constants;

    /**
     * Property that houses all the helper functions of the plugin.
     *
     * @since 2.0
     * @access private
     * @var Helper_Functions
     */
    private $_helper_functions;

    /*
    |--------------------------------------------------------------------------
    | Class Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Class constructor.
     *
     * @since 2.0
     * @access public
     *
     * @param Abstract_Main_Plugin_Class $main_plugin      Main plugin object.
     * @param Plugin_Constants           $constants        Plugin constants object.
     * @param Helper_Functions           $helper_functions Helper functions object.
     */
    public function __construct( Abstract_Main_Plugin_Class $main_plugin , Plugin_Constants $constants , Helper_Functions $helper_functions ) {

        $this->_constants        = $constants;
        $this->_helper_functions = $helper_functions;

        $main_plugin->add_to_all_plugin_models( $this );
        $main_plugin->add_to_public_models( $this );

    }

    /**
     * Ensure that only one instance of this class is loaded or can be loaded ( Singleton Pattern ).
     *
     * @since 2.0
     * @access public
     *
     * @param Abstract_Main_Plugin_Class $main_plugin      Main plugin object.
     * @param Plugin_Constants           $constants        Plugin constants object.
     * @param Helper_Functions           $helper_functions Helper functions object.
     * @return Module_Settings
     */
    public static function get_instance( Abstract_Main_Plugin_Class $main_plugin , Plugin_Constants $constants , Helper_Functions $helper_functions ) {

        if ( !self::$_instance instanceof self )
            self::$_instance = new self( $main_plugin , $constants , $helper_functions );

        return self::$_instance;

    }




    /*
    |--------------------------------------------------------------------------
    | Module settings
    |--------------------------------------------------------------------------
    */

    /**
     * Register premium settings sections.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array  $sections Settings sections.
     * @return array Filtered settings sections.
     */
    public function register_premium_settings_sections( $sections ) {

        $rearranage = array();
        foreach ( $sections as $key => $label ) {

            $rearranage[ $key ] = $label;

            // add after BOGO Deals settings tab.
            if ( $key === 'acfw_setting_bogo_deals_section' ) {
                if ( \ACFWF()->Helper_Functions->is_module( Plugin_Constants::SCHEDULER_MODULE ) )
                    $rearranage[ 'acfw_setting_scheduler_section' ] = __( 'Scheduler' , 'advanced-coupons-for-woocommerce' );
            }
        }

        return $rearranage;
    }

    /**
     * Register premium modules.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array $modules Modules settings list.
     * @return array Filtered modules settings list.
     */
    public function register_premium_modules_settings( $modules ) {
        
        $modules[] = array(
            'title'   => __( 'Auto Apply' , 'advanced-coupons-for-woocommerce' ),
            'type'    => 'checkbox',
            'desc'    => __( "Have your coupon automatically apply once it's able to be applied." , 'advanced-coupons-for-woocommerce' ),
            'id'      => Plugin_Constants::AUTO_APPLY_MODULE,
            'default' => 'yes'
        );

        $modules[] = array(
            'title'   => __( 'Coupon Scheduler' , 'advanced-coupons-for-woocommerce' ),
            'type'    => 'checkbox',
            'desc'    => __( 'Schedule start and end dates for coupons.' , 'advanced-coupons-for-woocommerce' ),
            'id'      => Plugin_Constants::SCHEDULER_MODULE,
            'default' => 'yes'
        );

        $modules[] = array(
            'title'   => __( 'Advanced Usage Limits' , 'advanced-coupons-for-woocommerce' ),
            'type'    => 'checkbox',
            'desc'    => __( 'Improves the usage limits feature of coupons, allowing you to set a time period to reset the usage counts.' , 'advanced-coupons-for-woocommerce' ),
            'id'      => Plugin_Constants::USAGE_LIMITS_MODULE,
            'default' => 'yes'
        );
        
        $modules[] = array(
            'title'   => __( 'Shipping Overrides' , 'advanced-coupons-for-woocommerce' ),
            'type'    => 'checkbox',
            'desc'    => __( 'Lets you provide coupons that can discount shipping prices for any shipping method.' , 'advanced-coupons-for-woocommerce' ),
            'id'      => Plugin_Constants::SHIPPING_OVERRIDES_MODULE,
            'default' => 'yes'
        );

        $modules[] = array(
            'title'   => __( 'Add Products' , 'advanced-coupons-for-woocommerce' ),
            'type'    => 'checkbox',
            'desc'    => __( 'On application of the coupon add certain products to the cart automatically after applying coupon.' , 'advanced-coupons-for-woocommerce' ),
            'id'      => Plugin_Constants::ADD_PRODUCTS_MODULE,
            'default' => 'yes'
        );

        $modules[] = array(
            'title'   => __( 'One Click Apply Notification' , 'advanced-coupons-for-woocommerce' ),
            'type'    => 'checkbox',
            'desc'    => __( 'Lets you show a WooCommerce notice to a customer if the coupon is able to be applied with a button to apply it.' , 'advanced-coupons-for-woocommerce' ),
            'id'      => Plugin_Constants::APPLY_NOTIFICATION_MODULE,
            'default' => 'yes'
        );

        $modules[] = array(
            'title'   => __( 'Loyalty Program' , 'advanced-coupons-for-woocommerce' ),
            'type'    => 'checkbox',
            'desc'    => __( 'Run a loyalty program on your store to reward your customers with points and reward them with coupons.' , 'advanced-coupons-for-woocommerce' ),
            'id'      => Plugin_Constants::LOYALTY_PROGRAM_MODULE,
            'default' => ''
        );

        return $modules;
    }

    /**
     * Get premium settings fields.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array  $settings        Settings list.
     * @param string $current_section Current section name.
     * @return array Filtered settings list.
     */
    public function get_premium_settings_fields( $settings , $current_section ) {

        $module = '';
        switch ( $current_section ) {

            case 'acfw_setting_scheduler_section' :
                $module   = Plugin_Constants::SCHEDULER_MODULE;
                $settings = apply_filters( 'acfw_setting_scheduler_options' , $this->_get_scheduler_section_options() );
                break;

        }

        // if module is disabled then set settings to empty array.
        if ( $module && ! \ACFWF()->Helper_Functions->is_module( $module ) )
            $settings = array();

        return $settings;
    }

    /**
     * Get scheduler section options.
     *
     * @since 2.0
     * @access private
     *
     * @return array
     */
    private function _get_scheduler_section_options() {

        return array(

            array(
                'title' => __( 'Scheduler Options', 'advanced-coupons-for-woocommerce' ),
                'type'  => 'title',
                'desc'  => '',
                'id'    => 'acfw_scheduler_main_title'
            ),

            array(
                'title'       => __( 'Schedule Start Error Message (global)' , 'advanced-coupons-for-woocommerce' ),
                'type'        => 'textarea',
                'desc'        => __( "Optional. Message that will be displayed when the coupon being applied hasnt started yet. Leave blank to use the default message." , 'advanced-coupons-for-woocommerce' ),
                'id'          => $this->_constants->SCHEDULER_START_ERROR_MESSAGE,
                'css'         => 'width: 500px; display: block;',
                'placeholder' => __( "This coupon has not started yet." , 'advanced-coupons-for-woocommerce' )
            ),

            array(
                'title'       => __( 'Schedule Expire Error Message (global)' , 'advanced-coupons-for-woocommerce' ),
                'type'        => 'textarea',
                'desc'        => __( "Optional. Message that will be displayed when the coupon being applied has already expired. Leave blank to use the default message." , 'advanced-coupons-for-woocommerce' ),
                'id'          => $this->_constants->SCHEDULER_EXPIRE_ERROR_MESSAGE,
                'css'         => 'width: 500px; display: block;',
                'placeholder' => __( "This coupon has expired." , 'advanced-coupons-for-woocommerce' )
            ),

            array(
                'type' => 'sectionend',
                'id'   => 'acfw_scheduler_sectionend'
            )
        );
    }

    /**
     * Register Loyalty Programs settings page.
     * 
     * @since 2.2
     * @access public
     * 
     * @param string $toplevel_menu Top level menu slug.
     */
    public function register_loyalty_programs_submenu( $toplevel_slug ) {

        if ( ! \ACFWF()->Helper_Functions->is_module( Plugin_Constants::LOYALTY_PROGRAM_MODULE ) ) return;

        add_submenu_page(
            $toplevel_slug,
            __( 'Loyalty Program' , 'advanced-coupons-for-woocommerce' ),
            __( 'Loyalty Program' , 'advanced-coupons-for-woocommerce' ),
            'manage_woocommerce',
            'acfw-loyalty-program',
            array( $this , 'display_loyalty_programs_settings_page' )
        );

        if ( ACFWF()->Helper_Functions->is_wc_admin_active() && function_exists( 'wc_admin_connect_page' ) ) {

            wc_admin_connect_page(
                array(
                    'id'        => 'acfw-loyalty-program',
                    'title'     => __( 'Advanced Coupons' , 'advanced-coupons-for-woocommerce' ),
                    'screen_id' => 'coupons_page_acfw-loyalty-program',
                    'path'      => 'admin.php?page=acfw-loyalty-program',
                    'js_page'   => false
                )
            );
        }
    }

    /**
     * Display loyalty programs settings page.
     * 
     * @since 2.2
     * @access public
     */
    public function display_loyalty_programs_settings_page() {

        global $current_tab;

        $current_tab = 'acfw_loyalty_programs';
        
        if ( ! class_exists( 'WC_Settings_Page' , false ) ) {
            include_once WP_PLUGIN_DIR . '/woocommerce/includes/admin/settings/class-wc-settings-page.php';
            include_once WP_PLUGIN_DIR . '/woocommerce/includes/admin/class-wc-admin-settings.php';
        }

        $settings = new Loyalty_Programs_Settings( $this->_constants , $this->_helper_functions );

        if ( ! empty( $_POST['save'] ) )
            \WC_Admin_Settings::save();

        include $this->_constants->VIEWS_ROOT_PATH . 'settings/view-loyalty-program-settings.php';
    }

    /**
     * Get Loyalty Programs section options.
     * 
     * @deprecated 2.2
     *
     * @since 1.9
     * @access private
     *
     * @return array
     */
    private function _get_loyalty_program_section_options() {
    }




    /*
    |--------------------------------------------------------------------------
    | Fulfill implemented interface contracts
    |--------------------------------------------------------------------------
    */

    /**
     * Filter help section options.
     * 
     * @since 2.0
     * @access public
     */
    public function filter_help_section_options( $settings ) {

        // get the last key of the array.
        $end_key     = key( array_slice( $settings , -1 , 1 , true ) );
        $section_end = array( $settings[ $end_key ] );

        unset( $settings[ $end_key ] );

        $fields = array(

            array(
                'title' => __(  'Utilities' , 'advanced-coupons-for-woocommerce' ),
                'type'  => 'acfw_divider_row',
                'id'    => 'acfw_utilities_divider_row'
            ),

            array(
                'title' => __( 'Rebuild/Clear Auto Apply Coupons Cache' , 'advanced-coupons-for-woocommerce' ),
                'type'  => 'acfw_rebuild_auto_apply_cache',
                'desc'  => __( "Manually rebuild and validate all auto apply coupons within the cache or clear the cache entirely." , 'advanced-coupons-for-woocommerce' ),
                'id'    => 'acfw_rebuild_auto_apply_cache'
            ),

            array(
                'title' => __( 'Rebuild/Clear Apply Notification Coupons Cache' , 'advanced-coupons-for-woocommerce' ),
                'type'  => 'acfw_rebuild_apply_notification_cache',
                'desc'  => __( "Manually rebuild and validate all apply notification coupons within the cache or clear the cache entirely." , 'advanced-coupons-for-woocommerce' ),
                'id'    => 'acfw_rebuild_apply_notifications_cache'
            ),

            array(
                'title' => __( 'Reset coupons usage limit' , 'advanced-coupons-for-woocommerce' ),
                'type'  => 'acfw_reset_coupon_usage_limit',
                'desc'  => __( 'Manually run cron for resetting usage limit for all applicable coupons.' , 'advanced-coupons-for-woocommerce' )
            ),

        );

        return array_merge( $settings , $fields , $section_end );
    }




    /*
    |--------------------------------------------------------------------------
    | REST API
    |--------------------------------------------------------------------------
    */

    /**
     * Register ACFWP API settings sections.
     * 
     * @since 2.2
     * @access public
     * 
     * @param array  $sections        Settings sections
     * @param string $current_section Current section.
     * @return array Filtered settings section.
     */
    public function register_acfwp_api_settings_sections( $sections , $current_section ) {

        $rearranage = array();
        foreach( $sections as $section ) {

            $rearranage[] = $section;

            // add after BOGO Deals section
            if ( 'bogo_deals_section' === $section[ 'id' ] ) {
                $rearranage[] = array(
                    'id'     => 'scheduler_section',
                    'title'  => __( 'Scheduler' , 'advanced-coupons-for-woocommerce' ),
                    'fields' => $current_section === 'scheduler_section' ? ACFWF()->Helper_Functions->prepare_setting_fields_for_api( $this->_get_scheduler_section_options() , $current_section ) : array(),
                    'show'   => ACFWF()->Helper_Functions->is_module( Plugin_Constants::SCHEDULER_MODULE ),
                    'module' => Plugin_Constants::SCHEDULER_MODULE
                );
            }
                
        }

        return $rearranage;
    }




    /*
    |--------------------------------------------------------------------------
    | Fulfill implemented interface contracts
    |--------------------------------------------------------------------------
    */

    /**
     * Execute codes that needs to run plugin activation.
     *
     * @since 2.0
     * @access public
     * @implements ACFWP\Interfaces\Initializable_Interface
     */
    public function initialize() {
    }

    /**
     * Execute Module_Settings class.
     *
     * @since 2.0
     * @access public
     * @inherit ACFWP\Interfaces\Model_Interface
     */
    public function run() {

        add_filter( 'woocommerce_get_sections_acfw_settings' , array( $this , 'register_premium_settings_sections' ) );
        add_filter( 'acfw_modules_settings' , array( $this , 'register_premium_modules_settings' ) ) ;
        add_filter( 'woocommerce_get_settings_acfw_settings' , array( $this , 'get_premium_settings_fields' ) , 10 , 2 );
        add_filter( 'acfw_settings_help_section_options' , array( $this , 'filter_help_section_options' ) );

        add_filter( 'acfw_api_settings_get_sections' , array( $this , 'register_acfwp_api_settings_sections' ) , 10 , 2 );
        add_action( 'acfw_register_admin_submenus' , array( $this , 'register_loyalty_programs_submenu' ) , 9 );

    }

}
