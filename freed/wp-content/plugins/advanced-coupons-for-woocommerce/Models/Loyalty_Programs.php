<?php
namespace ACFWP\Models;

use ACFWP\Abstracts\Abstract_Main_Plugin_Class;

use ACFWP\Interfaces\Model_Interface;
use ACFWP\Interfaces\Initiable_Interface;
use ACFWP\Interfaces\Activatable_Interface;

use ACFWP\Models\Objects\Advanced_Coupon;

use ACFWP\Helpers\Plugin_Constants;
use ACFWP\Helpers\Helper_Functions;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Model that houses the logic of extending the coupon system of woocommerce.
 * It houses the logic of handling coupon url.
 * Public Model.
 *
 * @since 2.0
 */
class Loyalty_Programs implements Model_Interface , Initiable_Interface , Activatable_Interface {

    /*
    |--------------------------------------------------------------------------
    | Class Properties
    |--------------------------------------------------------------------------
    */

    /**
     * Property that holds the single main instance of URL_Coupon.
     *
     * @since 2.0
     * @access private
     * @var Loyalty_Programs
     */
    private static $_instance;

    /**
     * Model that houses all the plugin constants.
     *
     * @since 2.0
     * @access private
     * @var Plugin_Constants
     */
    private $_constants;

    /**
     * Property that houses all the helper functions of the plugin.
     *
     * @since 2.0
     * @access private
     * @var Helper_Functions
     */
    private $_helper_functions;

    /**
     * Property that houses the datetime object of the user's last active date.
     * 
     * @since 2.0
     * @access private
     * @var DateTime
     */
    private $_last_active;

    /*
    |--------------------------------------------------------------------------
    | Class Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Class constructor.
     *
     * @since 2.0
     * @access public
     *
     * @param Abstract_Main_Plugin_Class $main_plugin      Main plugin object.
     * @param Plugin_Constants           $constants        Plugin constants object.
     * @param Helper_Functions           $helper_functions Helper functions object.
     */
    public function __construct( Abstract_Main_Plugin_Class $main_plugin , Plugin_Constants $constants , Helper_Functions $helper_functions ) {

        $this->_constants        = $constants;
        $this->_helper_functions = $helper_functions;

        $main_plugin->add_to_all_plugin_models( $this );
        $main_plugin->add_to_public_models( $this );

    }

    /**
     * Ensure that only one instance of this class is loaded or can be loaded ( Singleton Pattern ).
     *
     * @since 2.0
     * @access public
     *
     * @param Abstract_Main_Plugin_Class $main_plugin      Main plugin object.
     * @param Plugin_Constants           $constants        Plugin constants object.
     * @param Helper_Functions           $helper_functions Helper functions object.
     * @return Loyalty_Programs
     */
    public static function get_instance( Abstract_Main_Plugin_Class $main_plugin , Plugin_Constants $constants , Helper_Functions $helper_functions ) {

        if ( !self::$_instance instanceof self )
            self::$_instance = new self( $main_plugin , $constants , $helper_functions );

        return self::$_instance;

    }




    /*
    |--------------------------------------------------------------------------
    | Points earn related methods.
    |--------------------------------------------------------------------------
    */

    /**
     * Validate user roles.
     * 
     * @since 2.0
     * @access private
     * 
     * @global wpdb $wpdb Object that contains a set of functions used to interact with a database.
     * 
     * @param int $user_id User ID.
     * @return bool True if valid, false otherwise.
     */
    private function _validate_user_roles( $user_id ) {

        global $wpdb;

        $disallowed_roles = \ACFWF()->Helper_Functions->get_option( $this->_constants->LP_DISALLOWED_ROLES , array() );
        
        if ( ! empty( $disallowed_roles ) ) {

            $user_caps  = get_user_meta( $user_id , $wpdb->prefix . 'capabilities' , true );
            $user_roles = is_array( $user_caps ) && ! empty( $user_caps ) ? array_keys( $user_caps ) : array();
            $intersect  = array_intersect( $disallowed_roles , $user_roles );

            if ( ! empty( $intersect ) ) return false;
        }
        
        return true;
    }

    /**
     * Validate coupon to make sure only the redeemer can apply it.
     *
     * @since 2.0
     * @access public
     *
     * @param bool      $return Filter return value.
     * @param WC_Coupon $coupon WC_Coupon object.
     * @return bool True if valid, false otherwise.
     */
    public function validate_coupon_user( $return , $coupon ) {
        
        $current_user = wp_get_current_user();
        $coupon_user  = absint( get_post_meta( $coupon->get_id() , $this->_constants->META_PREFIX . 'loyalty_program_user' , true ) );

        if ( $coupon_user && $coupon_user != $current_user->ID )
            throw new \Exception( __( "Invalid coupon." , 'advanced-coupons-for-woocommerce' ) );

        return $return;
    }

    /**
     * Trigger earn_points_buy_product_action method when status is either changed to 'processing' or 'completed'.
     * 
     * @since 2.0
     * @access public
     * 
     * @param int    $order_id   Order ID.
     * @param string $old_status Order old status.
     * @param string $new_status Order new status.
     */
    public function trigger_earn_points_buy_product_order_status_change( $order_id , $old_status , $new_status ) {

        if ( in_array( $new_status , array( 'processing' , 'completed' ) ) ) {

            $this->earn_points_buy_product_action( $order_id );
            $this->earn_points_first_order_action( $order_id );
            $this->earn_points_high_spend_breakpoint( $order_id );
            $this->earn_points_order_within_period_action( $order_id );
        }
    }

    /**
     * Get total based on points calculated options.
     * 
     * @since 2.0
     * @access private
     * 
     * @param WC_Order | null $order Order object.
     * @return float Total calculated amount.
     */
    private function _get_total_based_on_points_calculate_options( $order = null ) {

        $raw        = get_option( $this->_constants->LP_POINTS_CALCULATION_OPTIONS , array( 'discounts' => 'yes' , 'tax' => 'yes' ) );
        $options    = is_array( $raw ) ? array_keys( $raw ) : array();
        $total      = is_object( $order ) ? $order->get_total( 'edit' ) : \WC()->cart->get_total( 'edit' );
        $subtotal   = is_object( $order ) ? $order->get_subtotal() : \WC()->cart->get_subtotal();
        $shipping   = is_object( $order ) ? $order->get_shipping_total( 'edit' ) : \WC()->cart->get_shipping_total();
        $calculated = (float) $total;

        // get total fees
        if ( is_object( $order ) ) {

            $fees_total = 0;
            foreach( $order->get_fees() as $item ) {

                $fee_total = $item->get_total();

                if ( 0 > $fee_total ) {
                    $max_discount = round( $subtotal + $fees_total + $shipping , wc_get_price_decimals() ) * -1;

                    if ( $fee_total < $max_discount ) {
                        $item->set_total( $max_discount );
                    }
                }

                $fees_total += $item->get_total();
            }

        } else
            $fees_total = \WC()->cart->get_fee_total();

        // get shipping overrides discount.
        $shipping_discount = 0;
        if ( is_object( $order ) ) {
            $shipping_discount = array_reduce( $order->get_fees() , function( $c , $f ) {
                return strpos( $f->get_name() , '[shipping_discount]' ) !== false ? $c + $f->get_total() : $c;
            } , 0 );
        } else {
            $shipping_discount = array_reduce( \WC()->cart->get_fees() , function( $c , $f ) {
                return strpos( $f->name , '[shipping_discount]' ) !== false ? $c + $f->total : $c;
            } , 0 );
        }

        // deduct shipping overrides discount from fees total.
        $fees_total = max( 0 , $fees_total - $shipping_discount );

        // discounts
        if ( ! in_array( 'discounts' , $options ) ) {
            $discounts   = is_object( $order ) ? $order->get_total_discount() : \WC()->cart->get_discount_total();
            $calculated += $discounts;
        }

        // tax
        if ( ! in_array( 'tax' , $options ) ) {
            $tax_total   = is_object( $order ) ? $order->get_total_tax( 'edit' ) : \WC()->cart->get_total_tax();
            $calculated -= $tax_total; 
        }

        // shipping
        if ( ! in_array( 'shipping' , $options ) ) {
            $calculated -= $shipping;
        }

        // shipping overrides discount
        if ( ! in_array( 'discounts' , $options ) || ! in_array( 'shipping' , $options ) ) {
            $calculated += abs( $shipping_discount );
        }

        // fees
        if ( ! in_array( 'fees' , $options ) ) {
            $calculated -= $fees_total;
        }

        $calculated = apply_filters( 'acfw_loyalprog_calculate_totals' , max( 0 , $calculated ) , $order );

        return apply_filters( 'acfw_filter_amount' , $calculated , true );
    }

    /**
     * Calculate the total points earned with a given cart/order subtotal.
     * 
     * @since 2.0
     * @access private
     * 
     * @param double $calc_total Cart/order calculated total.
     * @return int Points equivalent.
     */
    private function _calculate_points_earn( $calc_total ) {

        $multiplier = abs( \ACFWF()->Helper_Functions->sanitize_price( get_option( $this->_constants->LP_COST_POINTS_RATIO , '1' ) ) );
        return intval( $calc_total * $multiplier );
    }

    /**
     * Get minimum threshold value.
     * 
     * @since 2.0
     * @access private
     * 
     * @return float Minimum threshold value.
     */
    private function _get_minimum_threshold() {

        $raw = get_option( $this->_constants->LP_MINIMUM_POINTS_THRESHOLD , '0' );
        $amount = abs( \ACFWF()->Helper_Functions->sanitize_price( $raw ) );

        return $amount;
    }

    /**
     * Earn points action when products bought (run on order payment completion).
     * 
     * @since 2.0
     * @access public
     * 
     * @param int $order_id Order ID.
     */
    public function earn_points_buy_product_action( $order_id ) {

        if ( get_option( $this->_constants->LP_EARN_ACTION_BUY_PRODUCT , 'yes' ) !== 'yes' ) return;

        $order   = wc_get_order( $order_id );
        $user_id = $order->get_customer_id();

        if ( ! $user_id || ! $this->_validate_user_roles( $user_id ) || get_post_meta( $order_id , $this->_constants->LP_ENTRY_ID_META , true ) ) return;

        $calc_total   = $this->_get_total_based_on_points_calculate_options( $order );
        $points_total = $calc_total >= $this->_get_minimum_threshold() ? $this->_calculate_points_earn( $calc_total ) : 0;

        if ( ! $points_total ) return;

        $entry_id = $this->_insert_entry( $user_id , 'earn' , 'buy_product' , intval( $points_total ) , $order_id );
        update_post_meta( $order_id , $this->_constants->LP_ENTRY_ID_META , $entry_id );
    }

    /**
     * Earn points action on customer first order.
     * 
     * @since 2.0
     * @access public
     * 
     * @param int $order_id Order ID.
     */
    public function earn_points_first_order_action( $order_id ) {

        if ( get_option( $this->_constants->LP_EARN_ACTION_FIRST_ORDER , 'yes' ) !== 'yes' ) return;

        $order   = wc_get_order( $order_id );
        $user_id = $order->get_customer_id();

        if ( ! $user_id || ! $this->_validate_user_roles( $user_id ) || get_user_meta( $user_id , $this->_constants->LP_FIRST_ORDER_ENTRY_ID_META , true ) ) return;

        $query = new \WP_Query( array(
            'post_type'    => 'shop_order',
            'post_status'  => array( 'wc-completed' , 'wc-processing' ),
            'fields'       => 'ids',
            'post__not_in' => array( $order_id ),
            'meta_query'   => array(
                array(
                    'key'     => '_customer_user',
                    'value'   => $user_id,
                    'compare' => '='
                )
            )
        ) );

        if ( ! empty( $query->posts ) ) return;

        $points   = \ACFWF()->Helper_Functions->get_option( $this->_constants->LP_EARN_POINTS_FIRST_ORDER , '10' );
        $entry_id = $this->_insert_entry( $user_id , 'earn' , 'first_order' , $points , $order_id );
        update_user_meta( $user_id , $this->_constants->LP_FIRST_ORDER_ENTRY_ID_META , $entry_id );
    }

    /**
     * Earn points action when user is created.
     * 
     * @since 2.0
     * @access public
     * 
     * @param int $user_id User ID.
     */
    public function earn_points_user_register_action( $user_id ) {

        if ( get_option( $this->_constants->LP_EARN_ACTION_USER_REGISTER , 'yes' ) !== 'yes' || ! $this->_validate_user_roles( $user_id ) || get_user_meta( $user_id , $this->_constants->LP_USER_REGISTER_ENTRY_ID_META , true ) ) return;

        $points   = \ACFWF()->Helper_Functions->get_option( $this->_constants->LP_EARN_POINTS_USER_REGISTER , '10' );
        $entry_id = $this->_insert_entry( $user_id , 'earn' , 'user_register' , intval( $points ) );
        update_user_meta( $user_id , $this->_constants->LP_USER_REGISTER_ENTRY_ID_META , $entry_id );
    }

    /**
     * Calculate high spend points based on subtotal value excluding discounts.
     * 
     * @since 2.0
     * @access private
     * 
     * @param float $order_total Order/cart total amount.
     * @param array $breakpoints High spend breakpoints.
     * @return int High spend calculated points.
     */
    private function _calculate_high_spend_points( $order_total , $breakpoints ) {

        $order_total = (float) $order_total;

        // sort breakpoints from highest to lowest to get concise value.
        usort( $breakpoints , function( $a , $b ) {
            if ( $a[ 'sanitized' ] == $b[ 'sanitized' ] ) return 0;
            return ( $a[ 'sanitized' ] > $b[ 'sanitized' ] ) ? -1 : 1;
        } );

        $points = 0;
        foreach ( $breakpoints as $breakpoint ) {

            $amount = (float) $breakpoint[ 'sanitized' ];

            if ( $order_total >= $amount ) {
                $points = absint( $breakpoint[ 'points' ] );
                break;
            }
        }

        return $points;
    }

    /**
     * Earn points action when customer spends equal or more than set breakpoints.
     * 
     * @since 2.0
     * @access public
     * 
     * @param int $order_id Order ID.
     */
    public function earn_points_high_spend_breakpoint( $order_id ) {

        $breakpoints = get_option( $this->_constants->LP_EARN_POINTS_BREAKPOINTS , array() );
        $breakpoints = is_array( $breakpoints ) ? $breakpoints : json_decode( $breakpoints , true ); 

        if ( get_option( $this->_constants->LP_EARN_ACTION_BREAKPOINTS , 'yes' ) !== 'yes' || ! is_array( $breakpoints ) || empty( $breakpoints ) ) return;

        $order   = wc_get_order( $order_id );
        $user_id = $order->get_customer_id();
        
        if ( ! $user_id || ! $this->_validate_user_roles( $user_id ) || get_post_meta( $order_id , $this->_constants->LP_BREAKPOINTS_ENTRY_ID_META , true ) ) return;

        $calc_total = $this->_get_total_based_on_points_calculate_options( $order );
        $points     = $calc_total >= $this->_get_minimum_threshold() ? $this->_calculate_high_spend_points( $calc_total , $breakpoints ) : 0;

        if ( ! $points ) return;

        $entry_id = $this->_insert_entry( $user_id , 'earn' , 'high_spend' , $points , $order_id );
        update_post_meta( $order_id , $this->_constants->LP_BREAKPOINTS_ENTRY_ID_META , $entry_id );
    }

    /**
     * Get matching period points based on order.
     * 
     * @since 2.0
     * @access private
     * 
     * @param WC_DateTime $order_date Order date object.
     * @return int Earned points.
     */
    private function _get_matching_period_points( $order_date ) {

        $points = 0;
        $data   = get_option( $this->_constants->LP_EARN_POINTS_ORDER_PERIOD , array() );
        $data   = is_array( $data ) ? $data : json_decode( $data , true );

        if ( ! is_array( $data ) || empty( $data ) ) return $points;

        $timezone   = $order_date->getTimezone();
        foreach ( $data as $row ) {

            $start_date = \WC_DateTime::createFromFormat( 'm/d/Y g:i A' , $row[ 'sdate' ] . ' ' . $row[ 'stime' ] , $timezone );
            $end_date   = \WC_DateTime::createFromFormat( 'm/d/Y g:i A' , $row[ 'edate' ] . ' ' . $row[ 'etime' ] , $timezone );
        
            if ( $order_date >= $start_date && $order_date <= $end_date )
                return absint( $row[ 'points' ] );
        }

        return $points;
    }

    /**
     * Earn points action when order is done within set period.
     * 
     * @since 2.0
     * @access public
     * 
     * @param int $order_id Order ID.
     */
    public function earn_points_order_within_period_action( $order_id ) {

        if ( get_option( $this->_constants->LP_EARN_ACTION_ORDER_PERIOD , 'yes' ) !== 'yes' ) return;

        $order   = wc_get_order( $order_id );
        $user_id = $order->get_customer_id();

        if ( ! $user_id || ! $this->_validate_user_roles( $user_id ) || get_post_meta( $order_id , $this->_constants->LP_WITHIN_PERIOD_ENTRY_ID_META , true ) ) return;

        $calc_total = $this->_get_total_based_on_points_calculate_options( $order );
        $points     = $calc_total >= $this->_get_minimum_threshold() ? $this->_get_matching_period_points( $order->get_date_created() ) : 0;

        if ( ! $points ) return;

        $entry_id = $this->_insert_entry( $user_id , 'earn' , 'within_period' , $points , $order_id );
        update_post_meta( $order_id , $this->_constants->LP_WITHIN_PERIOD_ENTRY_ID_META , $entry_id );
    }

    /**
     * Trigger comment related earn actions on comment post.
     * 
     * @since 2.0
     * @access public
     * 
     * @param int        $comment_id  Comment ID.
     * @param int|string $is_approved Check if comment is approved, not approved or spam.
     * @param array      $commentdata Comment data.
     */
    public function trigger_comment_earn_actions_on_insert( $comment_id , $is_approved , $commentdata ) {

        $user_id = isset( $commentdata[ 'user_ID' ] ) ? $commentdata[ 'user_ID' ] : 0;
        if ( 1 !== $is_approved || ! $user_id ) return;

        if ( $commentdata[ 'comment_type' ] == 'review' )
            $this->earn_points_product_review_action( $comment_id , $user_id );
        else
            $this->earn_points_blog_comment_action( $comment_id , $user_id );
    }

    /**
     * Trigger comment related earn actions on comment status change.
     * 
     * @since 2.0
     * @access public
     * 
     * @param string     $new_status New comment status.
     * @param string     $old_status Old comment status.
     * @param WP_Comment $comment    Comment object.
     */
    public function trigger_comment_earn_actions_on_status_change( $new_status , $old_status , $comment ) {

        if ( 'approved' !== $new_status || ! $comment->user_id ) return;

        if ( $comment->comment_type == 'review' )
            $this->earn_points_product_review_action( $comment->comment_ID , $comment->user_id );
        else
            $this->earn_points_blog_comment_action( $comment->comment_ID , $comment->user_id );
    }

    /**
     * Earn points action on blog comment posting/approval.
     * 
     * @since 2.0
     * @access public
     * 
     * @param int $comment_id Comment ID.
     * @param int $user_id    User ID.
     */
    public function earn_points_blog_comment_action( $comment_id , $user_id ) {
        
        if ( get_option( $this->_constants->LP_EARN_ACTION_BLOG_COMMENT , 'yes' ) !== 'yes' ) return;

        if ( ! $user_id || ! $this->_validate_user_roles( $user_id ) || get_comment_meta( $comment_id , $this->_constants->LP_COMMENT_ENTRY_ID_META , true ) ) return;
        
        $points = (int) \ACFWF()->Helper_Functions->get_option( $this->_constants->LP_EARN_POINTS_BLOG_COMMENT );
        
        if ( $points ) {
            $entry_id = $this->_insert_entry( $user_id , 'earn' , 'blog_comment' , $points , $comment_id );
            update_comment_meta( $comment_id , $this->_constants->LP_COMMENT_ENTRY_ID_META , $entry_id );
        }
    }

    /**
     * Earn points action on product review posting/approval.
     * 
     * @since 2.0
     * @access public
     * 
     * @param int $comment_id Comment ID.
     * @param int $user_id    User ID.
     */
    public function earn_points_product_review_action( $comment_id , $user_id ) {

        if ( get_option( $this->_constants->LP_EARN_ACTION_PRODUCT_REVIEW , 'yes' ) !== 'yes' ) return;

        if ( ! $user_id || ! $this->_validate_user_roles( $user_id ) || get_comment_meta( $comment_id , $this->_constants->LP_COMMENT_ENTRY_ID_META , true ) ) return;
    
        $points = (int) \ACFWF()->Helper_Functions->get_option( $this->_constants->LP_EARN_POINTS_PRODUCT_REVIEW );
        
        if ( $points ) {
            $entry_id = $this->_insert_entry( $user_id , 'earn' , 'product_review' , $points , $comment_id );
            update_comment_meta( $comment_id , $this->_constants->LP_COMMENT_ENTRY_ID_META , $entry_id );
        }
    }




    /*
    |--------------------------------------------------------------------------
    | Points redemption related methods.
    |--------------------------------------------------------------------------
    */

    /**
     * Redeem points for user by converting points to a coupon only usable by the user.
     * 
     * @since 2.0
     * @access private
     * 
     * @param int $points  Points to redeem.
     * @param int $user_id User ID.
     */
    private function _redeem_points_for_user( $points , $user_id ) {

        $user_points = $this->_get_user_total_points( $user_id );
        $min_points  = (int) \ACFWF()->Helper_Functions->get_option( $this->_constants->LP_MINIMUM_POINTS_REDEEM , '0' );

        if ( ! $points || $points > $user_points || $points < $min_points ) return;

        $coupon = $this->_create_user_redeem_coupon( $points , $user_id );
        
        if ( $coupon instanceof Advanced_Coupon ) 
            $this->_insert_entry( $user_id , 'redeem' , 'coupon' , $points , $coupon->get_id() );

        return $coupon;
    }

    /**
     * Get schedule expire for redeemed coupon based on settings.
     * 
     * @since 2.0
     * @access private 
     * 
     * @return DateTime Expiry date time object.
     */
    private function _get_reedemed_coupon_schedule_expire() {

        $expire_period = (int) get_option( $this->_constants->LP_COUPON_EXPIRE_PERIOD , 365 );

        if ( ! $expire_period ) return;

        $timezone  = new \DateTimeZone( $this->_helper_functions->get_site_current_timezone() );
        $datetime  = new \DateTime( "today" , $timezone );
        $timestamp = $datetime->getTimestamp () + ( $expire_period * DAY_IN_SECONDS );

        $datetime->setTimestamp( $timestamp );

        return $datetime;
    }

    /**
     * Create user redeem coupon.
     * 
     * @since 2.0
     * @access private
     * 
     * @param int $points  Points to redeem.
     * @param int $user_id User ID.
     * @return Advanced_Coupon Advanced coupon object.
     */
    private function _create_user_redeem_coupon( $points , $user_id ) {

        $code   = $user_id . $this->_helper_functions->random_str( 6 );
        $coupon = new Advanced_Coupon( $code );
        $amount = $this->_calculate_redeem_points_worth( $points , false );

        if ( ! $amount ) return;

        $coupon->set_id( 0 );
        $coupon->set_code( $code );
        $coupon->set_discount_type( 'fixed_cart' );
        $coupon->set_amount( $amount );
        $coupon->set_id( $coupon->save() );
        $coupon->save_meta_data();

        if ( $coupon_id = $coupon->get_id() ) {

            update_post_meta( $coupon_id , $this->_constants->META_PREFIX . 'loyalty_program_user' , $user_id );
            update_post_meta( $coupon_id , $this->_constants->META_PREFIX . 'loyalty_program_points' , $points );
            update_post_meta( $coupon_id , 'usage_limit' , 1 );

            $datetime = $this->_get_reedemed_coupon_schedule_expire();
            $format   = is_object( $datetime ) ? $datetime->format( 'Y-m-d' ) : '';
            
            update_post_meta( $coupon_id , 'expiry_date' , $format );
            if ( is_object( $datetime ) ) update_post_meta( $coupon_id , 'date_expires' , $datetime->getTimestamp() );
            
            $this->_save_with_default_redeemed_coupon_category( $coupon_id );
        }
        
        return $coupon;
    }

    /**
     * Save coupon with default coupon category.
     * 
     * @since 2.0
     * @access private
     * 
     * @param int $coupon_id Coupon ID.
     */
    private function _save_with_default_redeemed_coupon_category( $coupon_id ) {

        $default_category = (int) get_option( $this->_constants->DEFAULT_REDEEM_COUPON_CAT );

        // create the default term if it doesn't exist
        if ( ! term_exists( $default_category , $this->_constants->COUPON_CAT_TAXONOMY ) ) {

            $default_cat_name = __( 'Redeemed' , 'advanced-coupons-for-woocommerce' );
            wp_insert_term( $default_cat_name , $this->_constants->COUPON_CAT_TAXONOMY );

            $default_term = get_term_by( 'name' , $default_cat_name , $this->_constants->COUPON_CAT_TAXONOMY );
            update_option( $this->_constants->DEFAULT_REDEEM_COUPON_CAT , $default_term->term_id );

        } else 
            $default_term = get_term_by( 'id' , $default_category , $this->_constants->COUPON_CAT_TAXONOMY );

        wp_set_post_terms( $coupon_id , $default_term->term_id , $this->_constants->COUPON_CAT_TAXONOMY );
    }

    /**
     * Calculate points worth.
     * 
     * @since 2.0
     * @access private
     * 
     * @param int $points User points.
     * @return float Amount points worth.
     */
    private function _calculate_redeem_points_worth( $points , $is_filter = true ) {

        $ratio = (int) get_option( $this->_constants->LP_REDEEM_POINTS_RATIO , '10' );
        $value = max( 0 , $points / $ratio );

        return $is_filter ? apply_filters( 'acfw_filter_amount' , $value ) : $value;
    }




    /*
    |--------------------------------------------------------------------------
    | Loyalty Program entries database and CRUD related methods.
    |--------------------------------------------------------------------------
    */

    /**
     * Create Loyal Programs SQL Table.
     * 
     * @since 2.0
     * @access private
     * 
     * @global wpdb $wpdb Object that contains a set of functions used to interact with a database.
     */
    private function _create_sql_table() {

        global $wpdb;

        if ( get_option( $this->_constants->LP_DB_TABLES_CREATED ) === 'yes' )
            return;

        $lp_entries_db   = $wpdb->prefix . $this->_constants->LP_DB_TABLE_NAME;
        $charset_collate = $wpdb->get_charset_collate();

        $sql  = "CREATE TABLE $lp_entries_db (
            entry_id bigint(20) NOT NULL AUTO_INCREMENT,
            user_id bigint(20) NOT NULL,
            entry_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            entry_type varchar(20) NOT NULL,
            entry_action varchar(20) NOT NULL,
            entry_amount bigint(20) NOT NULL,
            object_id bigint(20) NOT NULL,
            PRIMARY KEY (entry_id)
        ) $charset_collate;\n";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );

        update_option( $this->_constants->LP_DB_TABLES_CREATED , 'yes' );
    }

    /**
     * Get entries by user.
     * 
     * @since 2.0
     * @access private
     * 
     * @global wpdb $wpdb Object that contains a set of functions used to interact with a database.
     * 
     * @return array List of entries for user.
     */
    private function _get_entries_by_user( $user_id , $limit = 0 ) {

        global $wpdb;

        $lp_entries_db = $wpdb->prefix . $this->_constants->LP_DB_TABLE_NAME;
        $query = $wpdb->prepare( "SELECT * from $lp_entries_db WHERE `user_id` = %d ORDER BY entry_date DESC" , $user_id );
    
        if ( $limit ) 
            $query .= ' LIMIT ' . intval( $limit );

        return $wpdb->get_results( $query );
    }

    /**
     * Validate user last active date.
     * 
     * @since 2.0
     * @access private
     * 
     * @param string $last_active Date user was last active in mysql date format.
     * @return bool True if still active, false otherwise.
     */
    private function _validate_user_last_active( $last_active ) {

        $expire_period = (int) get_option( $this->_constants->LP_INACTIVE_DAYS_POINTS_EXPIRE , 365 );

        if ( ! $last_active || ! $expire_period ) return true;

        $utc              = new \DateTimeZone( 'UTC' );
        $timezone         = new \DateTimeZone( $this->_helper_functions->get_site_current_timezone() );
        $datetime         = new \DateTime( "now" , $timezone );
        $expire_timestamp = $datetime->getTimestamp() - ( $expire_period * DAY_IN_SECONDS );

        $this->_last_active = \DateTime::createFromFormat( 'Y-m-d H:i:s' , $last_active , $utc );
        $this->_last_active->setTimezone( $timezone );

        return $this->_last_active->getTimestamp() > $expire_timestamp;
    }

    /**
     * Get total points of user.
     * 
     * @since 2.0
     * @access private
     * 
     * @global wpdb $wpdb Object that contains a set of functions used to interact with a database.
     * 
     * @param int $user_id User ID.
     * @return int User total points.
     */
    private function _get_user_total_points( $user_id ) {

        global $wpdb;

        $lp_entries_db = $wpdb->prefix . $this->_constants->LP_DB_TABLE_NAME;
        $last_active   = $wpdb->get_var( $wpdb->prepare( "SELECT entry_date FROM `$lp_entries_db` WHERE user_id = %d ORDER BY entry_date DESC" , $user_id ) );
        
        $query = $wpdb->prepare( "(SELECT SUM(entry_amount) FROM `$lp_entries_db` WHERE user_id = %d AND entry_type != 'redeem') UNION (SELECT SUM(entry_amount) FROM `$lp_entries_db` WHERE user_id = %d AND entry_type = 'redeem')" , $user_id , $user_id );
        $data  = $wpdb->get_results( $query , ARRAY_N );

        // subtract reedeemed from earned. if value is negative, then return zero.
        $earned = isset( $data[0] ) ? intval( $data[0][0] ) : 0;
        $redeem = isset( $data[1] ) ? intval( $data[1][0] ) : $earned; // if second result is not set, it means that the total redeemed is equal to the total earned.
        $points = max( 0 , $earned - $redeem );

        if ( ! $this->_validate_user_last_active( $last_active ) && $points > 0 ) {
            $this->_insert_entry( $user_id , 'redeem' , 'expire' , $points );
            $points = 0;
        }
        
        // update cached value in user meta.
        update_user_meta( $user_id , $this->_constants->LP_USER_TOTAL_POINTS , $points );
        
        return $points;
    }

    /**
     * Get user redeemed coupons
     * 
     * @since 2.0
     * @access private
     * 
     * @global wpdb $wpdb Object that contains a set of functions used to interact with a database.
     * 
     * @param int $user_id User ID.
     * @return array User redeemed coupons.
     */
    private function _get_user_redeemed_coupons( $user_id ) {

        global $wpdb;

        $timezone = new \DateTimeZone( $this->_helper_functions->get_site_current_timezone() );
        $datetime = new \DateTime( "today" , $timezone );
        $today    = $datetime->format( 'U' );

        $lp_entries_db = $wpdb->prefix . $this->_constants->LP_DB_TABLE_NAME;
        $user_id       = absint( esc_sql( $user_id ) );

        $query = "SELECT object_id AS ID, posts.post_title AS code, amount.meta_value AS amount, posts.post_date AS date, entry_amount AS points 
            FROM $lp_entries_db
            INNER JOIN $wpdb->posts AS posts ON ( posts.ID = object_id )
            INNER JOIN $wpdb->postmeta AS amount ON ( amount.post_id = object_id AND amount.meta_key = 'coupon_amount' )
            INNER JOIN $wpdb->postmeta AS usage_count ON ( usage_count.post_id = object_id AND usage_count.meta_key = 'usage_count' )
            INNER JOIN $wpdb->postmeta AS coupon_expire ON ( coupon_expire.post_id = object_id AND coupon_expire.meta_key = 'date_expires' )
            WHERE user_id = $user_id 
                AND entry_type = 'redeem'
                AND posts.post_status = 'publish'
                AND posts.post_type = 'shop_coupon'
                AND usage_count.meta_value = 0 
                AND ( coupon_expire.meta_value = '' OR coupon_expire.meta_value IS NULL OR coupon_expire.meta_value > $today )
                GROUP BY object_id
                ORDER BY posts.post_date DESC
        ";

        $data = $wpdb->get_results( $query );

        return $data;
    }

    /**
     * Insert points entry.
     * 
     * @since 2.0
     * @access private
     * 
     * @global wpdb $wpdb Object that contains a set of functions used to interact with a database.
     * 
     * @param int    $user_id   User id.
     * @param string $type      Entry type (redeem or earn).
     * @param string $action    Entry action.
     * @param int    $amount    Entry amount.
     * @param int    $object_id Related object ID (posts, order, comments, etc.).
     * @return bool True if inserted, false otherwise.
     */
    private function _insert_entry( $user_id , $type , $action , $amount , $object_id = 0 ) {

        global $wpdb;

        $lp_entries_db = $wpdb->prefix . $this->_constants->LP_DB_TABLE_NAME;

        $check = $wpdb->insert( 
            $lp_entries_db,
            array(
                'user_id'      => $user_id,
                'entry_date'   => current_time( 'mysql' , true ),
                'entry_type'   => $type,
                'entry_action' => $action,
                'entry_amount' => $amount,
                'object_id'    => $object_id
            )
        );

        return $check ? $wpdb->insert_id : 0;
    }

    /**
     * Update points entry.
     * 
     * @since 2.0
     * @access private
     * 
     * @global wpdb $wpdb Object that contains a set of functions used to interact with a database.
     * 
     * @param int   $entry_id Entry ID.
     * @param array $changes  List of entry changes.
     * @return bool True if updated, false otherwise.
     */
    private function _update_entry( $entry_id , $changes = array() ) {

        global $wpdb;

        if ( ! $entry_id ) return;

        $lp_entries_db = $wpdb->prefix . $this->_constants->LP_DB_TABLE_NAME;
        $verified      = array();

        if ( isset( $changes[ 'user_id' ] ) && gettype( $changes[ 'user_id' ] ) == 'integer' && $changes[ 'user_id' ] )
            $verified[ 'user_id' ] = $changes[ 'user_id' ];

        if ( isset( $changes[ 'entry_date' ] ) && $changes[ 'entry_date' ] )
            $verified[ 'entry_date' ] = $changes[ 'entry_date' ];

        if ( isset( $changes[ 'entry_type' ] ) && $changes[ 'entry_type' ] )
            $verified[ 'entry_type' ] = $changes[ 'entry_type' ];

        if ( isset( $changes[ 'entry_action' ] ) && $changes[ 'entry_action' ] )
            $verified[ 'entry_action' ] = $changes[ 'entry_action' ];

        if ( isset( $changes[ 'entry_amount' ] ) && gettype( $changes[ 'entry_amount' ] ) == 'integer' && $changes[ 'entry_amount' ] )
            $verified[ 'entry_amount' ] = $changes[ 'entry_amount' ];

        return $wpdb->update(
            $lp_entries_db,
            $verified,
            array( 'entry_id' => absint( $entry_id ) )
        );
    }

    /**
     * Delete points entry.
     * 
     * @since 2.0
     * @access private
     * 
     * @global wpdb $wpdb Object that contains a set of functions used to interact with a database.
     * 
     * @param int $entry_id Entry ID.
     * @return bool True if deleted, false otherwise.
     */
    private function _delete_entry( $entry_id ) {

        global $wpdb;

        if ( ! $entry_id ) return;

        $lp_entries_db = $wpdb->prefix . $this->_constants->LP_DB_TABLE_NAME;

        return $wpdb->delete( $lp_entries_db , array( 'entry_id' => absint( $entry_id ) ) );
    }




    /*
    |--------------------------------------------------------------------------
    | User / Frontend related methods.
    |--------------------------------------------------------------------------
    */

    /**
     * Register loyalty program menu item in My Account navigation.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array $items My account menu items.
     * @return array Filtered my account menu items.
     */
    public function register_myaccount_menu_item( $items ) {

        if ( ! $this->_validate_user_roles( get_current_user_id() ) ) return $items;

        $logout      = isset( $items[ 'customer-logout' ] ) ? $items[ 'customer-logout' ] : '';
        $points_name = \ACFWF()->Helper_Functions->get_option( $this->_constants->LP_POINTS_NAME , __( 'Points' , 'advanced-coupons-for-woocommerce' ) );
        $endpoint    = $this->_helper_functions->get_loyalprog_myaccount_endpoint();

        unset( $items[ 'customer-logout' ] );
        $items[ $endpoint ] = sprintf( __( 'My %s' , 'advanced-coupons-for-woocommerce' ) , $points_name );
        
        if ( $logout ) $items[ 'customer-logout' ] = $logout;

        return $items;
    }

    /**
     * Register loyalty program my account tab endpoint.
     * 
     * @since 2.0
     * @access public
     */
    public function register_custom_endpoint() {

        $endpoint = $this->_helper_functions->get_loyalprog_myaccount_endpoint();
        add_rewrite_endpoint( $endpoint , EP_ROOT | EP_PAGES );
    }

    /**
     * Register loyalty program my account tab endpoint.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array $vars WP query vars.
     * @return array Filtered query vars.
     */
    public function register_endpoint_query_vars( $vars ) {

        $vars[] = $this->_helper_functions->get_loyalprog_myaccount_endpoint();
        return $vars;
    }

    /**
     * Set My Account tab endpoint title.
     * 
     * @since 2.0
     * @access public
     * 
     * @param string $title Page title.
     * @return string Filtered page title.
     */
    public function myaccount_tab_endpoint_title( $title ) {
        
        global $wp_query;

        $is_endpoint = isset( $wp_query->query_vars[ $this->_helper_functions->get_loyalprog_myaccount_endpoint() ] );

        if ( $is_endpoint && ! is_admin() && is_main_query() && in_the_loop() && is_account_page() ) {
            
            $points_name = \ACFWF()->Helper_Functions->get_option( $this->_constants->LP_POINTS_NAME , __( 'Points' , 'advanced-coupons-for-woocommerce' ) );
            $title       = sprintf( __( 'My %s' , 'advanced-coupons-for-woocommerce' ) , $points_name );
            remove_filter( 'the_title', array( $this, 'myaccount_tab_endpoint_title' ) );
        }

        return $title;
    }

    /**
     * My account tab endpoint content.
     * 
     * @since 2.0
     * @access public
     */
    public function myaccount_tab_endpoint_content() {

        $user = wp_get_current_user();
        if ( ! $this->_validate_user_roles( $user->ID ) ) return;

        $user_points  = (int) get_user_meta( $user->ID , $this->_constants->LP_USER_TOTAL_POINTS , true );
        $points_worth = wc_price( $this->_calculate_redeem_points_worth( $user_points ) );
        $single_point = wc_price( $this->_calculate_redeem_points_worth( 1 ) );
        $user_coupons = $this->_get_user_redeemed_coupons( $user->ID );
        $is_uc_module = \ACFWF()->Helper_Functions->is_module( Plugin_Constants::URL_COUPONS_MODULE );
        $points_name  = \ACFWF()->Helper_Functions->get_option( $this->_constants->LP_POINTS_NAME , __( 'Points' , 'advanced-coupons-for-woocommerce' ) );
        $minimum      = \ACFWF()->Helper_Functions->get_option( $this->_constants->LP_MINIMUM_POINTS_REDEEM , '0' );
        $minimum_text = $minimum ? sprintf( __( '<em>minimum %s</em> — ' , 'advanced-coupons-for-woocommerce' ) , $minimum ) : '';
        $spinner_img  = $this->_constants->IMAGES_ROOT_URL . 'spinner-2x.gif';

        include $this->_constants->VIEWS_ROOT_PATH . 'loyalprog/view-my-account-loyalty-programs-tab.php';
    }

    /**
     * Get total points to be earned for cart/checkout preview.
     * 
     * @since 2.0
     * @access private
     * 
     * @return int Points to earn.
     */
    private function _get_cart_points_earn_preview() {

        $calc_total  = $this->_get_total_based_on_points_calculate_options();
        $points      = get_option( $this->_constants->LP_EARN_ACTION_BUY_PRODUCT , 'yes' ) == 'yes' ? $this->_calculate_points_earn( $calc_total ) : 0;
        $breakpoints = get_option( $this->_constants->LP_EARN_POINTS_BREAKPOINTS , array() );
        $breakpoints = is_array( $breakpoints ) ? $breakpoints : json_decode( $breakpoints , true ); 
        

        if ( get_option( $this->_constants->LP_EARN_ACTION_BREAKPOINTS , 'yes' ) === 'yes' && is_array( $breakpoints ) && ! empty( $breakpoints ) )
            $points = $points + $this->_calculate_high_spend_points( $calc_total , $breakpoints );

        if ( get_option( $this->_constants->LP_EARN_ACTION_ORDER_PERIOD ) === 'yes' ) {
            $datetime = new \WC_DateTime();
            $datetime->setTimezone( new \DateTimeZone( $this->_helper_functions->get_site_current_timezone() ) );
            $points = $points + $this->_get_matching_period_points( $datetime );
        }

        return $points;
    }

    /**
     * Get points earned preview message.
     * 
     * @since 2.0
     * @access private
     * 
     * @return string Points preview message.
     */
    private function _get_points_earn_message_preview( $message = '' ) {

        if ( ! $this->_validate_user_roles( get_current_user_id() ) ) return;
        
        $calc_total = $this->_get_total_based_on_points_calculate_options();

        if ( ! $message || $calc_total < $this->_get_minimum_threshold() ) return;

        $points = $this->_get_cart_points_earn_preview();

        if ( ! $points ) return;

        $message = strpos( $message , '{points}' ) === false ? $message . ' <strong>{points}</strong>' : $message;
        $message = str_replace( '{points}' , $points , $message );
        
        ob_start();
        wc_print_notice( sprintf( '<span class="acfw-notice-text">%s</span>' , $message ) , 'notice' );
        
        return ob_get_clean();
    }

    /**
     * Display earned points on cart page.
     * 
     * @since 2.0
     * @access public
     */
    public function points_earn_message_in_cart() {

        $message   = get_option( $this->_constants->LP_POINTS_EARN_CART_MESSAGE , sprintf( __( 'This order will earn %s points.' , 'advanced-coupons-for-woocommerce' ) , '{points}' ) );
        echo sprintf( '<div class="acfw-loyalprog-notice">%s</div>' , $this->_get_points_earn_message_preview( $message ) ); ?>

        <script type="text/javascript">
        (function($){

            $( '.woocommerce-notices-wrapper .acfw-notice-text' ).closest( '.woocommerce-info' ).addClass( 'acfw-notice' );      
            $( '.acfw-notice' ).remove();
            $( '.acfw-loyalprog-notice .woocommerce-info' ).addClass( 'acfw-notice' ).appendTo( '.woocommerce-notices-wrapper' );
        })(jQuery);
        </script>
        <?php
    }

    /**
     * Display earned points on checkout page.
     * 
     * @since 2.0
     * @access public
     */
    public function points_earn_message_in_checkout() {

        if ( ! $this->_validate_user_roles( get_current_user_id() ) ) return;

        echo '<div class="acfw-loyalprog-notice-checkout"></div>';
    }

    /**
     * Append updated points earned message for checkout in WC order review fragments.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array $fragments Order review fragments.
     * @param array Filtered order review fragments.
     */
    public function points_earn_message_checkout_fragments( $fragments ) {

        $selector = ".acfw-loyalprog-notice-checkout";
        $message  = get_option( $this->_constants->LP_POINTS_EARN_CHECKOUT_MESSAGE , sprintf( __( 'This order will earn %s points.' , 'advanced-coupons-for-woocommerce' ) , '{points}' ) );

        $fragments[ $selector ] = sprintf( '<div class="acfw-loyalprog-notice-checkout">%s</div>' , $this->_get_points_earn_message_preview( $message ) );

        return $fragments;
    }

    /**
     * Display earned points on single product page.
     * 
     * @since 2.0
     * @access public
     */
    public function points_earn_message_single_product() {

        global $post;

        if ( ! $this->_validate_user_roles( get_current_user_id() ) || get_option( $this->_constants->LP_EARN_ACTION_BUY_PRODUCT , 'yes' ) !== 'yes' ) return;

        $message = \ACFWF()->Helper_Functions->get_option( $this->_constants->LP_POINTS_EARN_PRODUCT_MESSAGE );
        if ( ! $message ) return;

        $multiplier   = abs( \ACFWF()->Helper_Functions->sanitize_price( get_option( $this->_constants->LP_COST_POINTS_RATIO , '1' ) ) );
        $product      = wc_get_product( $post->ID );
        $display      = is_a( $product , 'WC_Product_Variable' ) ? 'style="display:none;"' : '';
        $calc_options = get_option( $this->_constants->LP_POINTS_CALCULATION_OPTIONS , array( 'discounts' => 'yes' , 'tax' => 'yes' ) );
        $calc_options = is_array( $calc_options ) ? array_keys( $calc_options ) : array();
        $include_tax  = in_array( 'tax' , $calc_options ) ? 'yes' : 'no';
        $price        = $product->get_type() != 'variable' ? $this->_get_single_product_preview_price( $product , $include_tax ) : $product->get_price();
        $price        = apply_filters( 'acfw_filter_amount' , $price , true );
        $points       = intval( $price * $multiplier );
        $message      = strpos( $message , '{points}' ) === false ? $message . ' <strong>{points}</strong>' : $message;
        $notice       = str_replace( '{points}' , $points , $message );
               
        include $this->_constants->VIEWS_ROOT_PATH . 'loyalprog/view-single-product-earn-message.php';
    }

    /**
     * Get single product preview price with WWP/P support.
     * 
     * @since 2.0
     * @access private
     * 
     * @param WC_Product $product     Product object.
     * @param string     $include_tax Include tax check (yes|no).
     * @return float Relative roduct price.
     */
    private function _get_single_product_preview_price( $product , $include_tax ) {

        $tax_display = get_option( 'woocommerce_tax_display_shop' , 'incl' );
    
        if ( class_exists( 'WWP_Wholesale_Prices' ) && method_exists( 'WWP_Wholesale_Prices' , 'get_product_wholesale_price_on_shop_v3' ) ) {

            $wwp_roles_obj = \WWP_Wholesale_Roles::getInstance();
            $wholesa_roles = $wwp_roles_obj->getUserWholesaleRole();

            if ( ! empty( $wholesa_roles ) ) {
                
                $wholesale_prices = \WWP_Wholesale_Prices::get_product_wholesale_price_on_shop_v3( $product->get_id() , $wholesa_roles );

                if ( $wholesale_prices[ 'wholesale_price' ] && $include_tax == 'yes' )
                    return $tax_display == 'incl' ? (float) $wholesale_prices[ 'wholesale_price' ] : (float) $wholesale_prices[ 'wholesale_price_raw' ];
                elseif ( $wholesale_prices[ 'wholesale_price_with_no_tax' ] && $include_tax == 'no' )
                    return (float) $wholesale_prices[ 'wholesale_price_with_no_tax' ];
            }
        }

        if ( $include_tax == 'yes' )
            return wc_get_price_including_tax( $product , array( 'qty' => 1 , 'price' => $product->get_price() ) );     
        else
            return wc_get_price_excluding_tax( $product , array( 'qty' => 1 , 'price' => $product->get_price() ) );         
    }

    /**
     * Add the display price without tax to the variation data on the single product page form.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array                $data      Variation data.
     * @param WC_Product_Variable  $parent    Parent variable product object.
     * @param WC_Product_Variation $variation Variation product object.
     */
    public function add_price_without_tax_to_variation_data( $data , $parent , $variation ) {

        $tax_display = get_option( 'woocommerce_tax_display_shop' , 'incl' );

        $data[ 'display_price_no_tax' ]   = (float) wc_get_price_excluding_tax( $variation , array( 'qty' => 1 , $variation->get_price() ) );
        $data[ 'display_price_with_tax' ] = (float) wc_get_price_including_tax( $variation , array( 'qty' => 1 , $variation->get_price() ) );
        return $data;
    }




    /*
    |--------------------------------------------------------------------------
    | Admin / Settings UI related.
    |--------------------------------------------------------------------------
    */

    /**
     * Render the earn breakpoints table settings field.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array $value Field value data.
     */
    public function render_earn_breakpoints_settings_field( $value ) {

        $data          = get_option( $this->_constants->LP_EARN_POINTS_BREAKPOINTS , array() );
        $data          = is_array( $data ) ? $data : json_decode( $data , true );
        $num           = 0;
        $decimal       = wc_get_price_decimal_separator();
        $price_pattern = '^[+]?([' . $decimal . ']\d+|\d+([' . $decimal . ']\d+)?)$';
        $field_desc    = \WC_Admin_Settings::get_field_description( $value );
        $desc          = $field_desc[ 'description' ];
        $tooltip       = $field_desc[ 'tooltip_html' ];

        include $this->_constants->VIEWS_ROOT_PATH . 'loyalprog/view-settings-earn-breakpoints-field.php';
    }

    /**
     * Render the earn orders within period table settings field.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array $value Field value data.
     */
    public function render_earn_orders_within_period_table_field( $value ) {

        $data       = get_option( $this->_constants->LP_EARN_POINTS_ORDER_PERIOD , array() );
        $data       = is_array( $data ) ? $data : json_decode( $data , true );
        $num        = 0;
        $field_desc = \WC_Admin_Settings::get_field_description( $value );
        $desc       = $field_desc[ 'description' ];
        $tooltip    = $field_desc[ 'tooltip_html' ];

        include $this->_constants->VIEWS_ROOT_PATH . 'loyalprog/view-settings-earn-orders-period-table-field.php';
    }

    /**
     * Render the ooints calculation options field.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array $value Field value data.
     */
    public function render_points_calculation_options_field( $value ) {

        $default      = isset( $value[ 'default' ] ) && is_array( $value[ 'default' ] ) ? $value[ 'default' ] : array();
        $calc_options = get_option( $value[ 'id' ] , $default );
        $calc_options = is_array( $calc_options ) ? $calc_options : array();
        $field_desc   = \WC_Admin_Settings::get_field_description( $value );
        $desc         = $field_desc[ 'description' ];
        $tooltip      = $field_desc[ 'tooltip_html' ];
        $tooltips     = isset( $value[ 'tooltips' ] ) ? $value[ 'tooltips' ] : array();

        include $this->_constants->VIEWS_ROOT_PATH . 'loyalprog/view-settings-points-calculation-options-field.php';
    }

    /**
     * Sort high spend breakpoints data ascendingly based on the amount breakpoint value.
     * 
     * @since 2.0
     * @access public
     */
    public function sort_high_spend_breakpoints_data( $data ) {

        $filtered = array();
        $amounts  = array();

        foreach ( $data as $row ) {

            if ( $row[ 'amount' ] && $row[ 'points' ] && ! in_array( $row[ 'amount' ] , $amounts ) )
                $filtered[] = $row;

            $amounts[] = $row[ 'amount' ];
        }

        $sanitized = array_map( function( $d ) {
            return array_merge( $d , array( 'sanitized' => \ACFWF()->Helper_Functions->sanitize_price( $d[ 'amount' ] ) ) );
        } , $filtered );
        
        usort( $sanitized , function( $a , $b ) {
            if ( $a[ 'sanitized' ] == $b[ 'sanitized' ] ) return 0;
            return ( $a[ 'sanitized' ] < $b[ 'sanitized' ] ) ? -1 : 1;
        } );

        return json_encode( $sanitized );
    }

    /**
     * Filter order within period data to make sure all required values are present.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array $data Option value.
     * @return array Filtered option value.
     */
    public function filter_order_within_period_data( $data ) {

        $filtered = @array_filter( $data , function($r) {
            return $r[ 'sdate' ] && $r[ 'stime' ] && $r[ 'edate' ] && $r[ 'etime' ] && $r[ 'points' ];
        } );
        
        $dates = array();
        foreach( $filtered as $key => $r ) {

            $dstring = sprintf( "%s%s%s%s" , $r[ 'sdate' ] , $r[ 'stime' ] , $r[ 'edate' ] , $r[ 'etime' ] );
            
            if ( in_array( $dstring , $dates ) ) {
                unset( $filtered[ $key ] );
                continue;
            }

            $dates[] = $dstring;
        }

        usort( $filtered , function( $a , $b ) {
            $atime = strtotime( $a[ 'sdate' ] . ' ' . $a[ 'stime' ] );
            $btime = strtotime( $b[ 'sdate' ] . ' ' . $b[ 'stime' ] );

            if ( $atime == $btime ) return 0;
            return ( $atime < $btime ) ? -1 : 1;
        } );

        return json_encode( $filtered );
    }




    /*
    |--------------------------------------------------------------------------
    | Admin / Settings UI related.
    |--------------------------------------------------------------------------
    */

    /**
     * Render manage user points field.
     * 
     * @since 2.0
     * @access public
     * 
     * @param WP_User $user User object.
     */
    public function render_manage_user_points_field( $user ) {

        $field_name  = $this->_constants->LP_USER_TOTAL_POINTS;
        $user_points = (int) get_user_meta( $user->ID , $this->_constants->LP_USER_TOTAL_POINTS , true );
        $points_name = \ACFWF()->Helper_Functions->get_option( $this->_constants->LP_POINTS_NAME , __( 'Points' , 'advanced-coupons-for-woocommerce' ) );

        include $this->_constants->VIEWS_ROOT_PATH . 'loyalprog/view-manage-user-points-field.php';
    }

    /**
     * Update user points as admin.
     * This adds a new entry in the `acfw_loyalprog_entries` table to adjust the points for a user.
     * 
     * @since 2.0
     * @access public
     * 
     * @param int $user_id User ID.
     */
    public function admin_adjust_update_user_points( $user_id ) {

        if ( ! isset( $_POST[ $this->_constants->LP_USER_TOTAL_POINTS ] ) || ! current_user_can( 'manage_woocommerce' ) ) return;

        $points_val = absint( $_POST[ $this->_constants->LP_USER_TOTAL_POINTS ] );
        $current    = (int) $this->_get_user_total_points( $user_id );

        if ( $current == $points_val ) return;

        $type       = $points_val > $current ? 'earn' : 'redeem';
        $adjust_val = $type == 'earn' ? $points_val - $current : $current - $points_val;

        $entry_id = $this->_insert_entry( $user_id , $type , 'admin_adjust' , $adjust_val );
    }

    /**
     * Redirect users with invalid roles back to my account page when visiting my points page.
     * 
     * @since 2.0
     * @access public
     */
    public function redirect_to_my_account_for_invalid_users() {

        global $wp_query;

        // only run when on my points page.
        if ( ! isset( $wp_query->query_vars[ $this->_helper_functions->get_loyalprog_myaccount_endpoint() ] ) ) return;

        // if user role is valid then skip.
        if ( $this->_validate_user_roles( get_current_user_id() ) ) return;

        wp_redirect( wc_get_page_permalink( 'myaccount' ) );
        exit;
    }




    /*
    |--------------------------------------------------------------------------
    | AJAX Functions.
    |--------------------------------------------------------------------------
    */

    /**
     * AJAX Redeem points for user.
     * 
     * @since 2.0
     * @access public
     */
    public function ajax_redeem_points_for_user() {

        if ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX )
            $response = array( 'status' => 'fail' , 'error_msg' => __( 'Invalid AJAX call' , 'advanced-coupons-for-woocommerce' ) );
        elseif ( ! isset( $_POST[ '_wpnonce' ] ) || ! wp_verify_nonce( $_POST[ '_wpnonce' ] , 'acfw_redeem_points_for_user' ) )
            $response = array( 'status' => 'fail' , 'error_msg' => __( 'You are not allowed to do this' , 'advanced-coupons-for-woocommerce' ) );
        elseif ( ! isset( $_POST[ 'redeem_points' ] ) || ! $_POST[ 'redeem_points' ] )
            $response = array( 'status' => 'fail' , 'error_msg' => __( 'Redemption failed. Please make sure that you have sufficient points or that the points redeemed is above the set minimum.' , 'advanced-coupons-for-woocommerce' ) );
        else {

            $points = intval( $_POST[ 'redeem_points' ] );
            $user   = wp_get_current_user();

            $coupon = $this->_redeem_points_for_user( $points , $user->ID );

            if ( $coupon instanceof Advanced_Coupon ) {

                $points   = (int) $this->_get_user_total_points( $user->ID );
                $response = array( 
                    'status'  => 'success',
                    'message' => __( 'Points successfully redeemed.' , 'advanced-coupons-for-woocommerce' ),
                    'code'    => $coupon->get_code(),
                    'amount'  => wc_price( $coupon->get_amount() ),
                    'date'    => $coupon->get_date_created()->date_i18n( 'F j, Y g:i a' ),
                    'points'  => $points,
                    'action'  => get_permalink( $coupon->get_id() ),
                    'worth'   => wc_price( $this->_calculate_redeem_points_worth( $points ) )
                );

            } else
                $response = array( 'status' => 'fail' , 'error_msg' => __( 'Redemption failed. Please make sure that you have sufficient points or that the points redeemed is above the set minimum.' , 'advanced-coupons-for-woocommerce' ) );
        }

        @header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
        echo wp_json_encode( $response );
        wp_die();
    }

    /**
     * AJAX User refresh points.
     * 
     * @since 2.0
     * @access public
     */
    public function ajax_user_refresh_points() {
        
        if ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX )
            $response = array( 'status' => 'fail' , 'error_msg' => __( 'Invalid AJAX call' , 'advanced-coupons-for-woocommerce' ) );
        elseif ( ! isset( $_POST[ 'nonce' ] ) || ! wp_verify_nonce( $_POST[ 'nonce' ] , 'acfw_loyalprog_user_refresh_points' ) )
            $response = array( 'status' => 'fail' , 'error_msg' => __( 'You are not allowed to do this' , 'advanced-coupons-for-woocommerce' ) );
        else {
            
            $user       = isset( $_POST[ 'user' ] ) && current_user_can( 'manage_woocommerce' ) ? get_user_by( 'ID' , $_POST[ 'user' ] ) : wp_get_current_user();
            $points     = (int) $this->_get_user_total_points( $user->ID );
            $expire_msg = get_option( $this->_constants->LP_POINTS_EXPIRY_MESSAGE , sprintf( __( 'Points is valid until %s. Redeem or earn more points to extend validity.' , 'advanced-coupons-for-woocommerce' ) , '{date_expire}' ) );

            if ( isset( $_POST[ 'admin' ] ) )
                $expire_msg = sprintf( __( 'User points will expire on <strong>%s</strong>.' , 'advanced-coupons-for-woocommerce' ) , '{date_expire}' );
            
            if ( is_object( $this->_last_active ) && $expire_msg && $points ) {

                $valid_days = (int) get_option( $this->_constants->LP_INACTIVE_DAYS_POINTS_EXPIRE , 365 );
                $timestamp  = $this->_last_active->getTimestamp() + ( $valid_days * DAY_IN_SECONDS );
                $this->_last_active->setTimestamp( $timestamp );

                $expire_msg = str_replace( '{date_expire}' , $this->_last_active->format( 'F j, Y g:i a' ) , $expire_msg );
            } else
                $expire_msg = '';
            
            $response = array( 
                'status'      => 'success',
                'user_points' => $points,
                'worth'       => wc_price( $this->_calculate_redeem_points_worth( $points ) ),
                'expire_msg'  => $expire_msg,
            );
        }

        @header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
        echo wp_json_encode( $response );
        wp_die();
    }






    /*
    |--------------------------------------------------------------------------
    | Fulfill implemented interface contracts
    |--------------------------------------------------------------------------
    */

    /**
     * Execute codes that needs to run plugin activation.
     *
     * @since 1.9
     * @access public
     * @implements ACFWP\Interfaces\Activatable_Interface
     */
    public function activate() {

        $this->_create_sql_table();
    }

    /**
     * Execute codes that needs to run plugin activation.
     *
     * @since 2.0
     * @access public
     * @implements ACFWP\Interfaces\Initializable_Interface
     */
    public function initialize() {

        if ( ! \ACFWF()->Helper_Functions->is_module( Plugin_Constants::LOYALTY_PROGRAM_MODULE ) )
            return;

        add_action( 'wp_ajax_acfw_redeem_points_for_user' , array( $this , 'ajax_redeem_points_for_user' ) );
        add_action( 'wp_ajax_acfw_lp_refresh_user_points' , array( $this , 'ajax_user_refresh_points' ) );
    }

    /**
     * Execute Loyalty_Programs class.
     *
     * @since 2.0
     * @access public
     * @inherit ACFWP\Interfaces\Model_Interface
     */
    public function run() {

        // register endpoint even when module is disabled.
        add_action( 'init' , array( $this , 'register_custom_endpoint' ) );
        add_filter( 'query_vars' , array( $this , 'register_endpoint_query_vars' ) );

        if ( ! \ACFWF()->Helper_Functions->is_module( Plugin_Constants::LOYALTY_PROGRAM_MODULE ) )
            return;

        $endpoint = $this->_helper_functions->get_loyalprog_myaccount_endpoint();

        add_filter( 'woocommerce_account_menu_items' , array( $this , 'register_myaccount_menu_item' ) );
        add_filter( 'the_title' , array( $this , 'myaccount_tab_endpoint_title' ) );
        add_action( 'woocommerce_account_' . $endpoint .  '_endpoint', array( $this, 'myaccount_tab_endpoint_content' ) );
        add_action( 'woocommerce_proceed_to_checkout' , array( $this , 'points_earn_message_in_cart' ) , 5 );
        add_action( 'woocommerce_before_checkout_form' , array( $this , 'points_earn_message_in_checkout' ) , 30 );
        add_filter( 'woocommerce_update_order_review_fragments' , array( $this , 'points_earn_message_checkout_fragments' ) );
        add_action( 'woocommerce_single_product_summary' , array( $this , 'points_earn_message_single_product' ) , 35 );
        add_filter( 'woocommerce_available_variation' , array( $this , 'add_price_without_tax_to_variation_data' ) , 10 , 3 );

        add_action( 'woocommerce_coupon_is_valid' , array( $this , 'validate_coupon_user' ) , 10 , 2 );
        add_action( 'woocommerce_payment_complete' , array( $this , 'earn_points_buy_product_action' ) );
        add_action( 'woocommerce_payment_complete' , array( $this , 'earn_points_first_order_action' ) );
        add_action( 'woocommerce_payment_complete' , array( $this , 'earn_points_high_spend_breakpoint' ) );
        add_action( 'woocommerce_payment_complete' , array( $this , 'earn_points_order_within_period_action' ) );
        add_action( 'woocommerce_order_status_changed' , array( $this , 'trigger_earn_points_buy_product_order_status_change' ) , 10 , 3 );
        add_action( 'user_register' , array( $this , 'earn_points_user_register_action' ) );
        
        add_action( 'woocommerce_admin_field_acfw_earn_breakpoints_field' , array( $this , 'render_earn_breakpoints_settings_field' ) );
        add_action( 'woocommerce_admin_field_acfw_earn_orders_period_table_field' , array( $this , 'render_earn_orders_within_period_table_field' ) );
        add_action( 'woocommerce_admin_field_acfw_points_calculation_options_field' , array( $this , 'render_points_calculation_options_field' ) );
        add_filter( 'pre_update_option_' . $this->_constants->LP_EARN_POINTS_BREAKPOINTS , array( $this , 'sort_high_spend_breakpoints_data' ) );
        add_filter( 'pre_update_option_' . $this->_constants->LP_EARN_POINTS_ORDER_PERIOD , array( $this , 'filter_order_within_period_data' ) );
        add_action( 'comment_post' , array( $this , 'trigger_comment_earn_actions_on_insert' ) , 10 , 3 );
        add_action( 'transition_comment_status' , array( $this , 'trigger_comment_earn_actions_on_status_change' ) , 10 , 3 );

        add_action( 'show_user_profile' , array( $this , 'render_manage_user_points_field' ) , 99 );
        add_action( 'edit_user_profile' , array( $this , 'render_manage_user_points_field' ) , 99 );
        add_action( 'edit_user_profile_update' , array( $this , 'admin_adjust_update_user_points' ) );
        add_action( 'personal_options_update' , array( $this , 'admin_adjust_update_user_points' ) );

        add_action( 'template_redirect' , array( $this , 'redirect_to_my_account_for_invalid_users' ) );
    }

}
