<?php
namespace ACFWP\Models;

use ACFWP\Abstracts\Abstract_Main_Plugin_Class;

use ACFWP\Interfaces\Model_Interface;
use ACFWP\Interfaces\Initiable_Interface;

use ACFWP\Models\Objects\Advanced_Coupon;


use ACFWP\Helpers\Plugin_Constants;
use ACFWP\Helpers\Helper_Functions;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Model that houses the logic of extending the coupon system of woocommerce.
 * It houses the logic of handling coupon url.
 * Public Model.
 *
 * @since 2.0
 */
class BOGO_Deals implements Model_Interface , Initiable_Interface {

    /*
    |--------------------------------------------------------------------------
    | Class Properties
    |--------------------------------------------------------------------------
    */

    /**
     * Property that holds the single main instance of URL_Coupon.
     *
     * @since 2.0
     * @access private
     * @var BOGO_Deals
     */
    private static $_instance;

    /**
     * Model that houses all the plugin constants.
     *
     * @since 2.0
     * @access private
     * @var Plugin_Constants
     */
    private $_constants;

    /**
     * Property that houses all the helper functions of the plugin.
     *
     * @since 2.0
     * @access private
     * @var Helper_Functions
     */
    private $_helper_functions;


    

    /*
    |--------------------------------------------------------------------------
    | Class Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Class constructor.
     *
     * @since 2.0
     * @access public
     *
     * @param Abstract_Main_Plugin_Class $main_plugin      Main plugin object.
     * @param Plugin_Constants           $constants        Plugin constants object.
     * @param Helper_Functions           $helper_functions Helper functions object.
     */
    public function __construct( Abstract_Main_Plugin_Class $main_plugin , Plugin_Constants $constants , Helper_Functions $helper_functions ) {

        $this->_constants        = $constants;
        $this->_helper_functions = $helper_functions;

        $main_plugin->add_to_all_plugin_models( $this );
        $main_plugin->add_to_public_models( $this );

    }

    /**
     * Ensure that only one instance of this class is loaded or can be loaded ( Singleton Pattern ).
     *
     * @since 2.0
     * @access public
     *
     * @param Abstract_Main_Plugin_Class $main_plugin      Main plugin object.
     * @param Plugin_Constants           $constants        Plugin constants object.
     * @param Helper_Functions           $helper_functions Helper functions object.
     * @return BOGO_Deals
     */
    public static function get_instance( Abstract_Main_Plugin_Class $main_plugin , Plugin_Constants $constants , Helper_Functions $helper_functions ) {

        if ( !self::$_instance instanceof self )
            self::$_instance = new self( $main_plugin , $constants , $helper_functions );

        return self::$_instance;

    }



    /*
    |--------------------------------------------------------------------------
    | Implementation
    |--------------------------------------------------------------------------
    */

    /**
     * Get matched deals in cart.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array           $matched   List of matched deals.
     * @param array           $deals      Deals data.
     * @param string          $deals_type Deals type.
     * @param Advanced_Coupon $coupon     Advanced coupon object.
     * @return array Filtered list of matched deals.
     */
    public function get_matched_deals_in_cart( $matched , $deals , $deals_type , $coupon ) {

        $cart_items = \WC()->cart->get_cart_contents();

        switch ( $deals_type ) {

            case 'combination-products' :

                $products = array_column( $deals[ 'products' ] , 'product_id' );
                $deal_id  = implode( '_' , $products );
                $temp     = array();

                foreach ( $cart_items as $item ) {

                    if ( ! ACFWF()->BOGO_Deals->is_item_valid( $item ) ) continue;

                    $key     = $item[ 'key' ];
                    $temp_id = isset( $item[ 'variation_id' ] ) && $item[ 'variation_id' ] ? $item[ 'variation_id' ] : $item[ 'product_id' ];
                    $temp_id = apply_filters( 'acfw_filter_cart_item_product_id' , $temp_id );

                    if ( ! in_array( $temp_id , $products ) ) continue;

                    // set initial quantity value for deal item to prevent errors. 
                    if ( ! isset( $temp[ $temp_id ] ) )
                        $temp[ $temp_id ] = 0;

                    // get quantity of items that are already matched for other deals.
                    $in_display_quantity = \ACFWF()->BOGO_Deals->isset_price_display( $key ) ? \ACFWF()->BOGO_Deals->get_price_display( $key )[ 'quantity' ] : 0;
                    
                    // append exact quantity value for current deal.
                    $temp[ $temp_id ] += $item[ 'quantity' ] - $in_display_quantity;

                    // save deal id to deals list.
                    \ACFWF()->BOGO_Deals->set_deals_prop( $key , $deal_id );
                }

                $matched[ $deal_id ] = array(
                    'items'    => $temp,
                    'quantity' => $deals[ 'quantity' ],
                    'discount' => $deals[ 'discount_value' ],
                    'type'     => $deals[ 'discount_type' ]
                );

                break;

            case 'product-categories' :

                $temp = array_map( function( $deal ) {
                    $deal[ 'products' ] = $this->_get_products_under_category( $deal[ 'category_id' ] );
                    return $deal;
                } , $deals );

                foreach ( $cart_items as $item ) {

                    if ( ! ACFWF()->BOGO_Deals->is_item_valid( $item ) ) continue;

                    $product_id = apply_filters( 'acfw_filter_cart_item_product_id' , $item[ 'product_id' ] );
                    
                    // check if current item is matched to current deal.
                    $filtered = array_values( array_filter( $temp , function( $deal ) use ( $product_id ) {
                        return in_array( $product_id , $deal[ 'products' ] );
                    } ) );

                    if ( ! isset( $filtered[0] ) ) continue;

                    $key     = $item[ 'key' ];
                    $deal_id = $filtered[0][ 'category_id' ];
                    
                    // create matched deal data prototype.
                    if ( ! isset( $matched[ $deal_id ] ) ) {
                        $matched[ $deal_id ] = array(
                            'items' => array(),
                            'quantity' => $filtered[0][ 'quantity' ],
                            'discount' => $filtered[0][ 'discount_value' ],
                            'type'     => $filtered[0][ 'discount_type' ]
                        );
                    }

                    // set initial quantity value for deal item to prevent errors.
                    if ( ! isset( $matched[ $deal_id ][ 'items' ][ $product_id ] ) )
                        $matched[ $deal_id ][ 'items' ][ $product_id ] = 0;

                    // save deal id to deals list.
                    \ACFWF()->BOGO_Deals->set_deals_prop( $key , $deal_id );

                    // get quantity of items that are already matched for other deals.
                    $in_display_quantity = \ACFWF()->BOGO_Deals->isset_price_display( $key ) ? \ACFWF()->BOGO_Deals->get_price_display( $key )[ 'quantity' ] : 0;

                    // append exact quantity value for current deal.
                    $matched[ $deal_id ][ 'items' ][ $product_id ] += $item[ 'quantity' ] - $in_display_quantity;
                }

                // add missing deals in cart to matched deals list.
                foreach ( $deals as $deal ) {

                    if ( in_array( $deal[ 'category_id' ] , \ACFWF()->BOGO_Deals->get_deals_data() ) ) continue;

                    $deal_id = $deal[ 'category_id' ];
                    $matched[ $deal_id ] = array(
                        'items'    => array(),
                        'quantity' => $deal[ 'quantity' ],
                        'discount' => $deal[ 'discount_value' ],
                        'type'     => $deal[ 'discount_type' ]
                    );
                }
                
                break;
        }

        return $matched;
    }

    /**
     * Verify BOGO Deals cart condition.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array  $data           Verified cart condition data.
     * @param array  $conditions     BOGO trigger conditions.
     * @param string $condition_type BOGO trigger type.
     * @param array  $matched        Matched deals in cart.
     * @return array Filtered verified cart condition data.
     */
    public function verify_cart_condition( $data , $conditions , $condition_type , $matched ) {

        switch ( $condition_type ) {

            case 'combination-products' :

                $product_ids   = isset( $conditions[ 'products' ] ) && ! empty( $conditions[ 'products' ] ) ? array_column( $conditions[ 'products' ] , 'product_id' ) : array();
                $product_ids   = array_unique( array_merge( $product_ids , $this->_get_variation_ids_for_variable_products( $product_ids ) ) );
                $cond_id       = implode( '_' , $product_ids );
                $quantity      = isset( $conditions[ 'quantity' ] ) && $conditions[ 'quantity' ] ? absint( $conditions[ 'quantity' ] ) : 1;
                $cart_quantity = array_sum( \ACFWF()->BOGO_Deals->get_quantities_of_condition_products_in_cart( $product_ids , $cond_id , true ) );

                $data[ 'concurrence' ]      = \ACFWF()->BOGO_Deals->calculate_concurrence( $product_ids , $quantity , $cart_quantity , $matched );
                $data[ 'cond_concurrence' ] = floor( $cart_quantity / $quantity );
                
                $data[ 'matched_conditions' ][ $cond_id ] = array(
                    'ids'      => $product_ids,
                    'quantity' => $quantity
                );

                break;

            case 'product-categories' :

                $category_ids = array_column( $conditions , 'category_id' ); 
                $quantities   = array_column( $conditions , 'quantity' , 'category_id' );
                $temp         = array();
                $cc_temp      = array();

                foreach ( $category_ids  as $category_id ) {

                    $product_ids   = $this->_get_products_under_category( $category_id );
                    $cond_id       = implode( '_' , $product_ids );
                    $quantity      = isset( $quantities[ $category_id ] ) ? (int) $quantities[ $category_id ] : 1;
                    $cart_quantity = array_sum( \ACFWF()->BOGO_Deals->get_quantities_of_condition_products_in_cart( $product_ids , $cond_id ) );
                    $temp[]        = \ACFWF()->BOGO_Deals->calculate_concurrence( $product_ids , $quantity , $cart_quantity , $matched );
                    $cc_temp[]     = floor( $cart_quantity / $quantity );

                    $data[ 'matched_conditions' ][ $cond_id ] = array(
                        'ids'      => $product_ids,
                        'quantity' => $quantity
                    );

                    // get the minimum concurrence of both condtions and deals in cart.
                    if ( ! empty( $temp ) ) $data[ 'concurrence' ] = min( $temp );

                    // get condition concurrence.
                    if ( ! empty( $cc_temp ) ) $data[ 'cond_concurrence' ] = min( $cc_temp );
                }
        }

        return $data;
    }

    /**
     * Get variation ids for given variable product ids.
     * 
     * @since 2.2
     * @access private
     * 
     * @param array $product_ids Variable product IDs.
     * @return array Relative product variation IDs.
     */
    private function _get_variation_ids_for_variable_products( $product_ids ) {

        global $wpdb;

        $imploded_ids = implode( ',' , $product_ids );
        $query = "SELECT ID FROM {$wpdb->posts}
            WHERE post_parent IN (
                SELECT tr.object_id FROM {$wpdb->term_relationships} AS tr 
                INNER JOIN {$wpdb->terms} AS t ON (t.slug = 'variable')
                INNER JOIN {$wpdb->term_taxonomy} AS tx ON (t.term_id = tx.term_id AND tx.taxonomy = 'product_type')
                WHERE tr.term_taxonomy_id = t.term_id
                    AND tr.object_id IN ({$imploded_ids})
        )";

        return array_map( 'absint' , $wpdb->get_col( $query ) );
    }




    /*
    |--------------------------------------------------------------------------
    | Edit BOGO Deals
    |--------------------------------------------------------------------------
    */

    /**
     * Register trigger and apply type descriptions.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array $descs Descriptions
     * @return array Filtered descriptions.
     */
    public function register_trigger_apply_type_descs( $descs ) {

        $premium = array(
            'combination-products' => __( 'Combination of Products – good when dealing with variable products or multiple products' , 'advanced-coupons-for-woocommerce' ),
            'product-categories'   => __( 'Product Categories – good when you want to trigger or apply a range of products from a particular category or set of categories' , 'advanced-coupons-for-woocommerce' )
        );

        return array_merge( $descs , $premium );
    }
    
    /**
     * Register trigger and apply type options.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array $options Field options list.
     * @return array Filtered field options list.
     */
    public function register_trigger_apply_type_options( $options ) {
        
        $premium = array(
            'combination-products' => __( 'Any Combination of Products' , 'advanced-coupons-for-woocommerce' ),
            'product-categories'   => __( 'Product Categories' , 'advanced-coupons-for-woocommerce' )
        );

        return array_merge( $options, $premium );
    }

    /**
     * Filter sanitize BOGO data.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array  $sanitized Sanized data.
     * @param array  $data      Raw data.
     * @param string $type      Data type.
     * @return array Sanitized data.
     */
    public function filter_sanitize_bogo_data( $sanitized , $data , $type ) {

        switch ( $type ) {

            case 'combination-products' :
                $sanitized = $this->_sanitize_combined_products_data( $data );
                break;
            case 'product-categories' :
                $sanitized = $this->_sanitize_product_cat_data( $data );
                break;
        }

        return $sanitized;
    }

    /**
     * Sanitize conditions/deals combined products type.
     * 
     * @since 1.1.0
     * @access private
     * 
     * @param array $data Condition/deals data.
     * @return array $data Sanitized condition/deals data.
     */
    private function _sanitize_combined_products_data( $data ) {

        $sanitized = array(
            'products' => array(),
            'quantity' => isset( $data[ 'quantity' ] ) && intval($data[ 'quantity' ]) > 0 ? absint( $data[ 'quantity' ] ) : 1
        );

        if ( isset( $data[ 'products' ] ) && is_array( $data[ 'products' ] ) )
            foreach ( $data[ 'products' ] as $product )
                $sanitized[ 'products' ][] = array_map( 'sanitize_text_field' , $product );
        
        if ( isset( $data[ 'discount_type' ] ) )
            $sanitized[ 'discount_type' ] = sanitize_text_field( $data[ 'discount_type' ] );

        if ( isset( $data[ 'discount_value' ] ) )
            $sanitized[ 'discount_value' ] = (float) wc_format_decimal( $data[ 'discount_value' ] );

        return $sanitized;
    }

    /**
     * Sanitize conditions/deals product category data.
     * 
     * @since 2.0
     * @access private
     * 
     * @param array $data Product data.
     * @return array Sanitized product data.
     */
    private function _sanitize_product_cat_data( $data ) {

        $sanitized = array();

        if ( is_array( $data ) ) {

            foreach ( $data as $key => $row ) {

                if ( ! isset( $row[ 'category_id' ] ) || ! isset( $row[ 'quantity' ] ) )
                    continue;

                $sanitized[ $key ] = array(
                    'category_id'    => intval( $row[ 'category_id' ] ),
                    'quantity'       => intval( $row[ 'quantity' ] ) > 1 ? absint( $row[ 'quantity' ] ) : 1,
                    'category_label' => sanitize_text_field( $row[ 'category_label' ] )
                );

                if ( isset( $row[ 'discount_type' ] ) )
                    $sanitized[ $key ][ 'discount_type' ] = sanitize_text_field( $row[ 'discount_type' ] );

                if ( isset( $row[ 'discount_value' ] ) )
                    $sanitized[ $key ][ 'discount_value' ] = (float) wc_format_decimal( $row[ 'discount_value' ] );
        
                if ( isset( $row[ 'condition' ] ) )
                    $sanitized[ $key ][ 'condition' ] = sanitize_text_field( $row[ 'condition' ] );
        
                if ( isset( $row[ 'condition_label' ] ) )
                    $sanitized[ $key ][ 'condition_label' ] = sanitize_text_field( $row[ 'condition_label' ] );
        
            } 
        }

        return $sanitized;
    }

    /**
     * Get products IDs under a specific category.
     * NOTE: We use this function so we data is explicitly fetched without filtering from 3rd party plugins.
     * 
     * @since 2.3
     * @access private
     */
    private function _get_products_under_category( $category_id ) {

        global $wpdb;

        $category_id = absint( $category_id );
        $query = "SELECT p.ID FROM {$wpdb->posts} AS p
            INNER JOIN {$wpdb->term_relationships} AS tr ON (p.ID = tr.object_id)
            INNER JOIN {$wpdb->term_taxonomy} AS tx ON (tr.term_taxonomy_id = tx.term_taxonomy_id)
            INNER JOIN {$wpdb->terms} AS t ON (tr.term_taxonomy_id = t.term_id)
            WHERE t.term_id = {$category_id}
                AND tx.taxonomy = 'product_cat'
        ";

        $product_ids = array_map( 'absint' , $wpdb->get_col( $query ) );

        return apply_filters( 'acfwp_bogo_get_products_under_category' , $product_ids , $category_id ); 
    }

    /**
     * Auto add deal products to cart.
     * 
     * @since 2.4
     * @access public
     * 
     * @param array           $cond_quantities Pairs of cart item key and quantity for condition items.
     * @param array           $matched_deals   Matched deals data.
     * @param int             $concurrence     BOGO Deal concurrence.
     * @param Advanced_Coupon $coupon          Coupon object.
     */
    public function auto_add_deal_products_to_cart( $matched_deals , $cond_quantites , $concurrence , $coupon ) {

        // use ACFWP version of Advanced_Coupon
        $coupon = $coupon instanceof Advanced_Coupon ? $coupon : new Advanced_Coupon( $coupon );

        /// Don't proceed when setting is not enabled.
        if ( ! $coupon->get_advanced_prop( 'bogo_auto_add_products' ) ) return $matched_deals;

        $bogo_deals = $coupon->get_advanced_prop( 'bogo_deals' );
        $deals_type = isset( $bogo_deals[ 'deals_type' ] ) ? $bogo_deals[ 'deals_type' ] : 'specific-products';

        // Don't proceed when deals type is not specific products.
        if ( $deals_type !== 'specific-products' ) return $matched_deals;

        $condition_items = array();
        $cart_items      = \WC()->cart->get_cart_contents();

        // loop through condition quantities and identify matching item id for each.
        foreach ( $cond_quantites as $key => $quantity ) {

            $items = array_values( array_filter( $cart_items , function($i) use ($key) {
                return $i[ 'key' ] === $key;
            } ) );
            $item = isset( $items[0] ) ? $items[0] : null;

            if ( ! $item ) continue;
            
            $item_id = isset( $item[ 'variation_id' ] ) && $item[ 'variation_id' ] ? $item[ 'variation_id' ] : $item[ 'product_id' ];
            $item_id = apply_filters( 'acfw_filter_cart_item_product_id' , $item_id );
            
            $condition_items[ $item_id ] = $quantity;
        }

        // calculate quantity of deals that needs to be added and process add to cart.
        foreach ( $matched_deals as $deal_id => $deal ) {
            
            $in_condition_qty = isset( $condition_items[ $deal_id ] ) ? $condition_items[ $deal_id ] : 0;
            $in_cart_qty      = array_sum( $deal[ 'items' ] ) - $in_condition_qty;
            $qty_to_add       = max( 0 , ( $deal[ 'quantity' ] * $concurrence ) - $in_cart_qty );

            if ( $qty_to_add > 0 ) {

                if ( 'product_variation' === get_post_type( $deal_id ) ) {
                    $variation_id = $deal_id;
                    $product_id   = wp_get_post_parent_id( $variation_id );
                } else {
                    $product_id   = $deal_id;
                    $variation_id = 0;
                }
                
                $variation = apply_filters( 'acfw_add_product_variation_data' , array() , $variation_id );

                // add product to cart.
                $item_key  = \WC()->cart->add_to_cart( $product_id , $qty_to_add , $variation_id , $variation );
                $cart_item = \WC()->cart->get_cart_item( $item_key );
                $deal_key  = isset( $cart_item[ 'key' ] ) ? $cart_item[ 'key' ] : '';

                // update matched deals data for deal and set deals prop in ACFWF BOGO_Deals instance.
                // NOTE: this is important to make sure that the deal's discount is applied on first view.
                if ( $deal_key ) {
                    $matched_deals[ $deal_id ][ 'items' ] = array_sum( $deal[ 'items' ] ) + $qty_to_add;
                    \ACFWF()->BOGO_Deals->set_deals_prop( $deal_key , $deal_id );
                }
                
            }
        }

        return $matched_deals;
    }

    /**
     * Display additional BOGO coupon settings.
     * 
     * @since 2.4
     * @access public
     * 
     * @param array           $bogo_deals Coupon BOGO Deals data.
     * @param Advanced_Coupon $coupon     Advanced coupon object.
     */
    public function display_additional_coupon_bogo_settings( $bogo_deals , $coupon ) {

        $auto_add_products = $coupon->get_advanced_prop( 'bogo_auto_add_products' );
        $deals_type        = isset( $bogo_deals[ 'deals_type' ] ) ? $bogo_deals[ 'deals_type' ] : 'specific-products';
        
        include $this->_constants->VIEWS_ROOT_PATH . 'coupons/view-coupon-bogo-additional-settings.php';
    }

    /**
     * Prevent eligible deals notice to show if coupon is set to auto add deal products.
     * 
     * @since 2.4
     * @access public
     * 
     * @param bool            $value     Filter value.
     * @param int             $remaining Quantity remaining for deal products.
     * @param Advanced_Coupon $coupon    coupon object.
     * @return bool Filtered value.
     */
    public function prevent_eligible_deals_notice_for_auto_add_deal_products( $value , $remaining , $coupon ) {

        // use ACFWP version of Advanced_Coupon
        $coupon = $coupon instanceof Advanced_Coupon ? $coupon : new Advanced_Coupon( $coupon );

        if ( $coupon->get_advanced_prop( 'bogo_auto_add_products' ) ) {

            $bogo_deals = $coupon->get_advanced_prop( 'bogo_deals' );
            $deals_type = isset( $bogo_deals[ 'deals_type' ] ) ? $bogo_deals[ 'deals_type' ] : 'specific-products';

            // explicity return false if deals type matches.
            if ( $deals_type === 'specific-products' ) return false;
        }

        return $value;
    }

    /**
     * Filter BOGO Deals is item valid utility function.
     * 
     * @since 2.4
     * @access public
     * 
     * @param bool  $is_valid Filter value.
     * @param array $item     Cart item.
     * @return bool Filtered value.
     */
    public function filter_bogo_is_item_valid( $is_valid , $item ) {

        // explicity return false if item is added via "Add Products" with discount.
        if ( isset( $item[ 'acfw_add_product' ] ) ) return false;

        return $is_valid;
    }

    /**
     * Format BOGO Deals data for editing.
     * 
     * @since 2.4.1
     * @access public
     */
    public function format_bogo_deals_data_for_edit( $formatted_deals , $bogo_deals ) {

        if ( "combination-products" === $bogo_deals['deals_type'] ) {
            $formatted_deals = $bogo_deals['deals'];
            $formatted_deals['discount_value'] = wc_format_localized_price($formatted_deals['discount_value']);
        } elseif ( "product-categories" === $bogo_deals['deals_type'] ) {
            $formatted_deals = array_map(function ($r) {
                $r['discount_value'] = wc_format_localized_price($r['discount_value']);
                return $r;
            }, $bogo_deals['deals']);
        }

        return $formatted_deals;
    }

    /**
     * AJAX save coupon BOGO additional settings fields.
     * 
     * @since 2.4
     * @access public
     */
    public function ajax_save_additional_settings() {

        if ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX )
            $response = array( 'status' => 'fail' , 'error_msg' => __( 'Invalid AJAX call' , 'advanced-coupons-for-woocommerce' ) );
        elseif ( ! current_user_can( apply_filters( 'acfw_ajax_save_bogo_deals' , 'manage_woocommerce' ) ) )
            $response = array( 'status' => 'fail' , 'error_msg' => __( 'You are not allowed to do this' , 'advanced-coupons-for-woocommerce' ) );
        elseif ( ! isset( $_POST[ 'coupon_id' ] ) )
            $response = array( 'status' => 'fail' , 'error_msg' => __( 'Missing required post data' , 'advanced-coupons-for-woocommerce' ) );
        else {

            $coupon_id         = intval( $_POST[ 'coupon_id' ] );
            $auto_add_products = isset( $_POST[ 'auto_add_products' ] ) && $_POST[ 'auto_add_products' ] === "yes";
            
            update_post_meta( $coupon_id , $this->_constants->META_PREFIX . 'bogo_auto_add_products' , $auto_add_products );

            $response = array(
                'status' => 'success'
            );
        }

        @header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
        echo wp_json_encode( $response );
        wp_die();
    }




    /*
    |--------------------------------------------------------------------------
    | Fulfill implemented interface contracts
    |--------------------------------------------------------------------------
    */

    /**
     * Execute codes that needs to run plugin activation.
     *
     * @since 2.0
     * @access public
     * @implements ACFWP\Interfaces\Initializable_Interface
     */
    public function initialize() {

        add_action( 'wp_ajax_acfw_save_bogo_additional_settings' , array( $this , 'ajax_save_additional_settings' ) );
    }

    /**
     * Execute BOGO_Deals class.
     *
     * @since 2.0
     * @access public
     * @inherit ACFWP\Interfaces\Model_Interface
     */
    public function run() {

        add_filter( 'acfw_get_matched_bogo_deals_in_cart' , array( $this , 'get_matched_deals_in_cart' ) , 10 , 4 );
        add_filter( 'acfw_bogo_deals_verify_cart_condition' , array( $this , 'verify_cart_condition' ) , 10 , 4 );
        add_filter( 'acfw_bogo_trigger_apply_type_descs' , array( $this , 'register_trigger_apply_type_descs' ) );
        add_filter( 'acfw_bogo_trigger_apply_type_options' , array( $this , 'register_trigger_apply_type_options' ) );
        add_filter( 'acfw_sanitize_bogo_deals_data' , array( $this , 'filter_sanitize_bogo_data' ) , 10 , 3 );
        add_filter( 'acfw_filter_bogo_matched_deals' , array( $this , 'auto_add_deal_products_to_cart' ) , 10 , 4 );
        add_filter( 'acfw_format_bogo_apply_data' , array( $this , 'format_bogo_deals_data_for_edit' ) , 10 , 2 );

        add_action( 'acfw_bogo_before_additional_settings' , array( $this , 'display_additional_coupon_bogo_settings' ) , 10 , 2 );
        add_filter( 'acfw_bogo_deals_is_eligible_notice' , array( $this , 'prevent_eligible_deals_notice_for_auto_add_deal_products' ) , 10 , 3 );
        add_filter( 'acfw_bogo_is_item_valid' , array( $this , 'filter_bogo_is_item_valid' ) , 10 , 2 );
    }

}
