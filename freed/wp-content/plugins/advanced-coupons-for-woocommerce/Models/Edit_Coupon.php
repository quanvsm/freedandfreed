<?php
namespace ACFWP\Models;

use ACFWP\Abstracts\Abstract_Main_Plugin_Class;

use ACFWP\Interfaces\Model_Interface;
use ACFWP\Interfaces\Initiable_Interface;

use ACFWP\Models\Objects\Advanced_Coupon;

use ACFWP\Helpers\Plugin_Constants;
use ACFWP\Helpers\Helper_Functions;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Model that houses the logic of extending the coupon system of woocommerce.
 * It houses the logic of handling coupon url.
 * Public Model.
 *
 * @since 2.0
 */
class Edit_Coupon implements Model_Interface , Initiable_Interface {

    /*
    |--------------------------------------------------------------------------
    | Class Properties
    |--------------------------------------------------------------------------
    */

    /**
     * Property that holds the single main instance of URL_Coupon.
     *
     * @since 2.0
     * @access private
     * @var Edit_Coupon
     */
    private static $_instance;

    /**
     * Model that houses all the plugin constants.
     *
     * @since 2.0
     * @access private
     * @var Plugin_Constants
     */
    private $_constants;

    /**
     * Property that houses all the helper functions of the plugin.
     *
     * @since 2.0
     * @access private
     * @var Helper_Functions
     */
    private $_helper_functions;

    /*
    |--------------------------------------------------------------------------
    | Class Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Class constructor.
     *
     * @since 2.0
     * @access public
     *
     * @param Abstract_Main_Plugin_Class $main_plugin      Main plugin object.
     * @param Plugin_Constants           $constants        Plugin constants object.
     * @param Helper_Functions           $helper_functions Helper functions object.
     */
    public function __construct( Abstract_Main_Plugin_Class $main_plugin , Plugin_Constants $constants , Helper_Functions $helper_functions ) {

        $this->_constants        = $constants;
        $this->_helper_functions = $helper_functions;

        $main_plugin->add_to_all_plugin_models( $this );
        $main_plugin->add_to_public_models( $this );

    }

    /**
     * Ensure that only one instance of this class is loaded or can be loaded ( Singleton Pattern ).
     *
     * @since 2.0
     * @access public
     *
     * @param Abstract_Main_Plugin_Class $main_plugin      Main plugin object.
     * @param Plugin_Constants           $constants        Plugin constants object.
     * @param Helper_Functions           $helper_functions Helper functions object.
     * @return Edit_Coupon
     */
    public static function get_instance( Abstract_Main_Plugin_Class $main_plugin , Plugin_Constants $constants , Helper_Functions $helper_functions ) {

        if ( !self::$_instance instanceof self )
            self::$_instance = new self( $main_plugin , $constants , $helper_functions );

        return self::$_instance;

    }




    /*
    |--------------------------------------------------------------------------
    | One click apply notifications data
    |--------------------------------------------------------------------------
    */

    /**
     * Add "Apply notification" data tab to woocommerce coupon admin data tabs.
     * 
     * @since 2.0
     * @access public
     *
     * @param array $coupon_data_tabs Array of coupon admin data tabs.
     * @return array Modified array of coupon admin data tabs.
     */
    public function apply_notification_admin_data_tab( $coupon_data_tabs ) {

        $coupon_data_tabs[ 'acfw_apply_notification' ] = array(
            'label'  => __( 'One Click Apply Notification', 'advanced-coupons-for-woocommerce' ),
            'target' => 'acfw_apply_notification',
            'class'  => ''
        );

        return $coupon_data_tabs;
    }

    /**
     * Add "Apply notification" data panel to woocommerce coupon admin data panels.
     * 
     * @since 2.0
     * @access public
     *
     * @param int $coupon_id WC_Coupon ID.
     */
    public function apply_notification_admin_data_panel( $coupon_id ) {

        $panel_id            = 'acfw_apply_notification';
        $coupon              = \ACFWF()->Edit_Coupon->get_shared_advanced_coupon( $coupon_id );
        $apply_notifications = \ACFWF()->Helper_Functions->get_option( $this->_constants->APPLY_NOTIFICATION_CACHE , array() );
        $additional_classes  = 'toggle-enable-fields';
        $title               = __( 'One Click Apply Notification' , 'advanced-coupons-for-woocommerce' );
        $fields              = apply_filters( 'acfw_apply_notification_admin_data_panel_fields' , array(

            array(
                'cb'   => 'woocommerce_wp_checkbox',
                'args' => array(
                    'id'          => $this->_constants->META_PREFIX . 'enable_apply_notification',
                    'class'       => 'toggle-trigger-field',
                    'label'       => __( 'Enable one click apply notifications' , 'advanced-coupons-for-woocommerce' ),
                    'description' => __( 'When checked, this will enable one click apply notifications for this coupon which will then show a notification for customers in the cart when the coupon can be applied.' , 'advanced-coupons-for-woocommerce' ),
                    'value'       => in_array( $coupon_id , $apply_notifications ) ? 'yes' : ''
                )
            ),

            array(
                'cb'   => 'woocommerce_wp_textarea_input',
                'args' => array(
                    'id'          => $this->_constants->META_PREFIX . 'apply_notification_message',
                    'label'       => __( 'Notification message' , 'advanced-coupons-for-woocommerce' ),
                    'description' => __( "The notification message that will be displayed after checking that the coupon is elegible in the customer's cart." , 'advanced-coupons-for-woocommerce' ),
                    'desc_tip'    => true,
                    'type'        => 'text',
                    'placeholder' => __( "Your current cart is eligible for a coupon." , 'advanced-coupons-for-woocommerce' ),
                    'value'       => $coupon->get_advanced_prop( 'apply_notification_message' )
                )
            ),

            array(
                'cb'   => 'woocommerce_wp_text_input',
                'args' => array(
                    'id'          => $this->_constants->META_PREFIX . 'apply_notification_btn_text',
                    'label'       => __( 'Apply Button Text' , 'advanced-coupons-for-woocommerce' ),
                    'description' => __( "The text for the button to apply the coupon." , 'advanced-coupons-for-woocommerce' ),
                    'desc_tip'    => true,
                    'type'        => 'text',
                    'placeholder' => __( "Apply Coupon" , 'advanced-coupons-for-woocommerce' ),
                    'value'       => $coupon->get_advanced_prop( 'apply_notification_btn_text' )
                )
            ),

            array(
                'cb'   => 'woocommerce_wp_select',
                'args' => array(
                    'id'          => $this->_constants->META_PREFIX . 'apply_notification_type',
                    'label'       => __( 'Notification type' , 'advanced-coupons-for-woocommerce' ),
                    'description' => __( "The type of notification to display." , 'advanced-coupons-for-woocommerce' ),
                    'desc_tip'    => true,
                    'type'        => 'text',
                    'placeholder' => __( "Select..." , 'advanced-coupons-for-woocommerce' ),
                    'value'       => $coupon->get_advanced_prop( 'apply_notification_type' ),
                    'options'     => array(
                        'notice'  => __( 'Info' , 'advanced-coupons-for-woocommerce' ),
                        'success' => __( 'Success' , 'advanced-coupons-for-woocommerce' ),
                        'error'   => __( 'Error' , 'advanced-coupons-for-woocommerce' )
                    )
                )
            )
        ) );

        include $this->_constants->VIEWS_ROOT_PATH . 'coupons' . DIRECTORY_SEPARATOR . 'view-generic-admin-data-panel.php';
    }




    /*
    |--------------------------------------------------------------------------
    | Exclude Coupons
    |--------------------------------------------------------------------------
    */

    /**
     * Display exlcude coupons field inside "Usage restriction" tab.
     *
     * @since 2.0
     * @access public
     *
     * @param int $coupon_id WC_Coupon ID.
     */
    public function display_exclude_coupons_field( $coupon_id ) {

        $coupon   = \ACFWF()->Edit_Coupon->get_shared_advanced_coupon( $coupon_id );
        $excluded = $coupon->get_advanced_prop( 'excluded_coupons' , array() );

        ?>
        <div class="options_group">
            <p class="form-field">
                <label for="acfw_exclude_coupons"><?php _e( 'Exclude coupons' , 'advanced-coupons-for-woocommerce' ); ?></label>
                <select class="wc-product-search" multiple style="width: 50%;" name="_acfw_exclude_coupon_ids[]" 
                    data-placeholder="<?php esc_attr_e( 'Search coupons&hellip;' , 'advanced-coupons-for-woocommerce' ); ?>"
                    data-action="acfw_search_coupons"
                    data-exclude="<?php echo esc_attr( json_encode( array( $coupon_id ) ) ); ?>">
                    <?php foreach ( $excluded as $excluded_id ) : ?>
                        <option value="<?php echo $excluded_id; ?>" selected><?php echo wc_get_coupon_code_by_id( $excluded_id ); ?></option>
                    <?php endforeach ?>
                </select>
                <?php echo wc_help_tip( __( 'This is the advanced version of the "Individual use only" field. Coupons listed here cannot be used in conjunction with this coupon.' , 'advanced-coupons-for-woocommerce' ) ); ?>
            </p>
        </div>
        <?php
    }



    
    /*
    |--------------------------------------------------------------------------    
    | Add Products Data
    |--------------------------------------------------------------------------
    */

    /**
     * Add new "add free products" data tab to woocommerce coupon admin data tabs.
     *
     * @since 2.0
     * @access public
     *
     * @param array $coupon_data_tabs Array of coupon admin data tabs.
     * @return array Modified array of coupon admin data tabs.
     */
    public function add_products_admin_data_tab( $coupon_data_tabs ) {

        $coupon_data_tabs[ 'acfw_add_products' ] = array(
            'label'  => __( 'Add Products', 'advanced-coupons-for-woocommerce' ),
            'target' => 'acfw_add_products',
            'class'  => ''
        );

        return $coupon_data_tabs;
    }

    /**
     * Add "add free products" data panel to woocommerce coupon admin data panels.
     *
     * @since 2.0
     * @access public
     *
     * @param int $coupon_id WC_Coupon ID.
     */
    public function add_products_admin_data_panel( $coupon_id ) {

        $panel_id     = 'acfw_add_products';
        $coupon       = \ACFWF()->Edit_Coupon->get_shared_advanced_coupon( $coupon_id );
        $spinner_img  = $this->_constants->IMAGES_ROOT_URL . 'spinner-2x.gif';
        $add_products = $coupon->get_add_products_data( 'edit' );
        $exclude      = is_array( $add_products ) ? array_column( $add_products , 'product_id' ) : array();

        include $this->_constants->VIEWS_ROOT_PATH . 'coupons' . DIRECTORY_SEPARATOR . 'view-add-products-data-panel.php';
    }




    /*
    |--------------------------------------------------------------------------    
    | Auto Apply Data
    |--------------------------------------------------------------------------
    */

    /**
     * Register auto apply coupon metabox.
     * 
     * @since 2.0
     * @access public
     * 
     * @param string  $post_type Post type.
     * @param WP_Post $post      Post object.
     */
    public function register_auto_apply_metabox( $post_type , $post ) {

        if ( $post_type !== 'shop_coupon' ) return;

        $metabox = function( $post ) {

            $auto_apply_coupons = \ACFWF()->Helper_Functions->get_option( $this->_constants->AUTO_APPLY_COUPONS , array() );
            $input_name         = $this->_constants->META_PREFIX . 'auto_apply_coupon';
            $checked            = checked( in_array( $post->ID , $auto_apply_coupons ) , true , false );

            ?>
            <label>
                <input id="acfw_auto_apply_coupon_field" type="checkbox" name="<?php echo esc_attr( $input_name ); ?>" value="yes" <?php echo $checked ?>>
                <?php _e( 'Enable auto apply for this coupon.' , 'advanced-coupons-for-woocommerce' ); ?>
            </label>
            <div class="auto-apply-warning">
                <?php _e( '<strong>Note:</strong> coupon cannot be auto applied when "Allowed emails", "Usage limit per coupon" and/or "Usage limit per user" option is set.' , 'advanced-coupons-for-woocommerce' ); ?>
            </div>
            <?php
        };

        add_meta_box( 'acfw-auto-apply-coupon' , __( 'Auto Apply Coupon' , 'advanced-coupons-for-woocommerce' ) , $metabox , 'shop_coupon' , 'side' );
    }



    
    /*
    |--------------------------------------------------------------------------
    | Shipping Overrides
    |--------------------------------------------------------------------------
    */

    /**
     * Add shipping ovverides data tab to woocommerce coupon admin data tabs.
     *
     * @since 2.0
     * @access public
     *
     * @param array $coupon_data_tabs Array of coupon admin data tabs.
     * @return array Modified array of coupon admin data tabs.
     */
    public function shipping_overrides_admin_data_tab( $coupon_data_tabs ) {

        $coupon_data_tabs[ 'acfw_shipping_overrides' ] = array(
            'label'  => __( 'Shipping Overrides', 'advanced-coupons-for-woocommerce' ),
            'target' => 'acfw_shipping_overrides',
            'class'  => ''
        );

        return $coupon_data_tabs;
    }
    
    /**
     * Add "Shipping Overrides" data panel to woocommerce coupon admin data panels.
     *
     * @since 2.0
     * @access public
     *
     * @param int $coupon_id WC_Coupon ID.
     */
    public function shipping_overrides_admin_data_panel( $coupon_id ) {
        
        $panel_id     = 'acfw_shipping_overrides';
        $coupon       = \ACFWF()->Edit_Coupon->get_shared_advanced_coupon( $coupon_id );
        $spinner_img  = $this->_constants->IMAGES_ROOT_URL . 'spinner-2x.gif';
        $zone_methods = apply_filters( 'acfw_shipping_override_selectable_options' , array() );
        $overrides    = $coupon->get_shipping_overrides_data_edit();
        $exclude      = array_map( function( $row ) {
            return array( 'zone' => $row[ 'shipping_zone' ] , 'method' => $row[ 'shipping_method' ] );
        } , $overrides );

        include $this->_constants->VIEWS_ROOT_PATH . 'coupons' . DIRECTORY_SEPARATOR . 'view-shipping-overrides-data-panel.php';
    }




    /*
    |--------------------------------------------------------------------------
    | Scheduler Data
    |--------------------------------------------------------------------------
    */

    /**
     * Add new scheduler data tab to woocommerce coupon admin data tabs.
     *
     * @since 2.0
     * @access public
     *
     * @param array $coupon_data_tabs Array of coupon admin data tabs.
     * @return array Modified array of coupon admin data tabs.
     */
    public function scheduler_admin_data_tab( $coupon_data_tabs ) {

        $coupon_data_tabs[ 'acfw_scheduler' ] = array(
            'label'  => __( 'Scheduler', 'advanced-coupons-for-woocommerce' ),
            'target' => 'acfw_scheduler',
            'class'  => ''
        );

        return $coupon_data_tabs;
    }

    /**
     * Add scheduler data panel to woocommerce coupon admin data panels.
     *
     * @since 2.0
     * @access public
     *
     * @param int $coupon_id WC_Coupon ID.
     */
    public function scheduler_admin_data_panel( $coupon_id ) {

        $panel_id = 'acfw_scheduler';
        $coupon   = \ACFWF()->Edit_Coupon->get_shared_advanced_coupon( $coupon_id );
        $title    = __( 'Scheduler' , 'advanced-coupons-for-woocommerce' );
        $fields   = apply_filters( 'acfw_scheduler_admin_data_panel_fields' , array(
            array(
                'cb'   => array( ACFWP()->Scheduler , 'scheduler_input_field' ),
                'args' => array(
                    'id'          => $this->_constants->META_PREFIX . 'schedule_start',
                    'label'       => __( 'Coupon start date' , 'advanced-coupons-for-woocommerce' ),
                    'description' => __( "The exact date the coupon will be available from. Based on the timezone in this WordPress installation's settings." , 'advanced-coupons-for-woocommerce' ),
                    'desc_tip'    => true,
                    'type'        => 'text',
                    'value'       => $coupon->get_advanced_prop( 'schedule_start' ),
                    'placeholder' => 'YYYY-MM-DD',
                    'custom_attributes' => array( 'pattern' => '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])' )
                )
            ),
            array(
                'cb'   => 'woocommerce_wp_textarea_input',
                'args' => array(
                    'id'          => $this->_constants->META_PREFIX . 'schedule_start_error_msg',
                    'label'       => __( 'Coupon start error message' , 'advanced-coupons-for-woocommerce' ),
                    'description' => __( "Show a custom error message to customers that try to apply this coupon before it is available." , 'advanced-coupons-for-woocommerce' ),
                    'desc_tip'    => true,
                    'type'        => 'text',
                    'value'       => $coupon->get_advanced_prop( 'schedule_start_error_msg' ),
                    'placeholder' => \ACFWF()->Helper_Functions->get_option( $this->_constants->SCHEDULER_START_ERROR_MESSAGE , __( "This coupon has not started yet." , 'advanced-coupons-for-woocommerce' ) )
                )
            ),
            array(
                'cb'   => array( ACFWP()->Scheduler , 'scheduler_input_field' ),
                'args' => array(
                    'id'          => $this->_constants->META_PREFIX . 'schedule_expire',
                    'label'       => __( 'Coupon expiry date' , 'advanced-coupons-for-woocommerce' ),
                    'description' => __( "The exact date the coupon will be expired. Based on the timezone in this WordPress installation's settings." , 'advanced-coupons-for-woocommerce' ),
                    'desc_tip'    => true,
                    'type'        => 'text',
                    'value'       => $coupon->get_advanced_prop( 'schedule_end' ),
                    'placeholder' => 'YYYY-MM-DD',
                    'custom_attributes' => array( 'pattern' => '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])' )
                )
            ),
            array(
                'cb'   => 'woocommerce_wp_textarea_input',
                'args' => array(
                    'id'          => $this->_constants->META_PREFIX . 'schedule_expire_error_msg',
                    'label'       => __( 'Coupon expire error message' , 'advanced-coupons-for-woocommerce' ),
                    'description' => __( "Show a custom error message to customers that try to apply this coupon after it has expired." , 'advanced-coupons-for-woocommerce' ),
                    'desc_tip'    => true,
                    'type'        => 'text',
                    'value'       => $coupon->get_advanced_prop( 'schedule_expire_error_msg' ),
                    'placeholder' => \ACFWF()->Helper_Functions->get_option( $this->_constants->SCHEDULER_EXPIRE_ERROR_MESSAGE , __( "This coupon has expired." , 'advanced-coupons-for-woocommerce' ) )
                )
            ),
        ) );
        
        include $this->_constants->VIEWS_ROOT_PATH . 'coupons' . DIRECTORY_SEPARATOR . 'view-generic-admin-data-panel.php';
        
    }




    
    /*
    |--------------------------------------------------------------------------
    | Advanced Usage Limits.
    |--------------------------------------------------------------------------
    */

    /**
     * Advanced usage limits fields.
     * 
     * @since 2.0
     * @access public
     * 
     * @param int       $coupon_id Coupon ID.
     * @param WC_Coupon $coupon    Coupon object.
     */
    public function advanced_usage_limits_fields( $coupon_id , $coupon ) {

        $period = get_post_meta( $coupon_id , $this->_constants->META_PREFIX . 'reset_usage_limit_period' , true );
        $reset  = get_post_meta( $coupon_id , $this->_constants->META_PREFIX . 'usage_limit_reset_time' , true );

        woocommerce_wp_select( array(
            'id'      => $this->_constants->META_PREFIX . 'reset_usage_limit_period',
            'label'   => __( 'Reset usage count every:' , 'advanced-coupons-for-woocommerce' ),
            'options' => array(
                'none'    => __( 'Never reset' , 'advanced-coupons-for-woocommerce' ),
                'yearly'  => __( 'Every year' , 'advanced-coupons-for-woocommerce' ),
                'monthly' => __( 'Every month' , 'advanced-coupons-for-woocommerce' ),
                'weekly'  => __( 'Every week' , 'advanced-coupons-for-woocommerce' ),
                'daily'   => __( 'Every day' , 'advanced-coupons-for-woocommerce' ),
            ),
            'description' => __( 'Set the time period to reset the usage limit count. <strong>Yearly:</strong> resets at start of the year. <strong>Monthly:</strong> resets at start of the month. <strong>Weekly:</strong> resets at the start of every week (day depends on the <em>"Week Starts On"</em> setting). <strong>Daily:</strong> resets everyday. Time is always set at 12:00am of the local timezone settings.' , 'advanced-coupons-for-woocommerce' ),
            'desc_tip'    => true,
            'value'       => $period
        ) );
        
        if ( $period && $period != 'none' && $reset ) :

            $timezone = new \DateTimeZone( $this->_helper_functions->get_site_current_timezone() );
            $dateobj  = new \DateTime();
    
            $dateobj->setTimestamp( $reset );
            $dateobj->setTimezone( $timezone );
        
        ?>
            <div class="acfw-usage-limit-info">
                <p class="reset-time"><?php echo sprintf( 'Next reset for coupon usage count is on <code>%s %s</code>' , $dateobj->format( 'F j, Y g:i a' ), $timezone->getName() ); ?></p>
            </div>
        <?php endif;
    }




    /*
    |--------------------------------------------------------------------------
    | Loyalty Programs
    |--------------------------------------------------------------------------
    */

    /**
     * Register Loyalty Program metabox.
     * 
     * @since 2.0
     * @access public
     * 
     * @param string  $post_type Post type.
     * @param WP_Post $post      Post object.
     */
    public function register_loyalty_program_metabox( $post_type , $post ) {

        if ( $post_type !== 'shop_coupon' ) return;

        $coupon  = \ACFWF()->Edit_Coupon->get_shared_advanced_coupon( $post->ID );
        $user_id = $coupon->get_advanced_prop( 'loyalty_program_user' );
        $user    = $user_id ? get_userdata( $user_id ) : false;

        if ( ! $user ) return;

        $metabox = function( $post ) use ( $coupon , $user ) {

            $usage  = (int) get_post_meta( $post->ID , 'usage_count' , true );
            $status = $usage > 0 ? __( 'Used' , 'advanced-coupons-for-woocommerce' ) : __( 'Not yet used' , 'advanced-coupons-for-woocommerce' );

            ?>
            <dl>
                <dt><?php _e( 'User:' , 'advanced-coupons-for-woocommerce' ); ?></dt>
                <dd><a href="<?php echo get_edit_user_link( $user->ID ); ?>"><?php echo $user->user_nicename; ?></a></dd>
                
                <dt><?php _e( 'Points:' , 'advanced-coupons-for-woocommerce' ); ?></dt>
                <dd><?php echo $coupon->get_advanced_prop( 'loyalty_program_points' ); ?></dd>

                <dt><?php _e( 'Status:' , 'advanced-coupons-for-woocommerce' ); ?></dt>
                <dd><?php echo $status; ?></dd>
            </dl>

            <?php
        };

        add_meta_box( 'acfw-loyalty-program-metabox' , __( 'Loyalty Program' , 'advanced-coupons-for-woocommerce' ) , $metabox , 'shop_coupon' , 'side' , 'high' );
    }




    /*
    |--------------------------------------------------------------------------
    | Save coupon
    |--------------------------------------------------------------------------
    */

    /**
     * Save coupon data.
     * 
     * @since 2.0
     * @since 2.1 Delete _acfw_schedule_expire meta on save.
     * @access public
     * 
     * @param int             $coupon_id Coupon ID.
     * @param Advanced_Coupon $coupon    Advanced coupon object.
     */
    public function save_coupon_data( $coupon_id , $coupon ) {

        $allowed_html = wp_kses_allowed_html( 'post' );

        // Exclude coupons
        $excluded_coupons = isset( $_POST[ $this->_constants->META_PREFIX . 'exclude_coupon_ids' ] ) ? $_POST[ $this->_constants->META_PREFIX . 'exclude_coupon_ids' ] : array();
        $excluded_coupons = is_array( $excluded_coupons ) && ! empty( $excluded_coupons ) ? array_map( 'sanitize_text_field' , $excluded_coupons ) : array();
        $coupon->set_advanced_prop( 'excluded_coupons' , $excluded_coupons );

        $schedule_expire = isset( $_POST[ 'expiry_date' ] ) ? sanitize_text_field( $_POST[ 'expiry_date' ] ) : '';
        update_post_meta( $coupon_id , 'expiry_date' , $schedule_expire );
        
        // ACFWP-111: Delete _acfw_schedule_expire when coupon is saved. This is for cloned coupons that generated this meta due to a bug.
        $check = delete_post_meta( $coupon_id , '_acfw_schedule_expire' );

        // Scheduler module
        if ( \ACFWF()->Helper_Functions->is_module( Plugin_Constants::SCHEDULER_MODULE ) ) {

            $schedule_start            = isset( $_POST[ $this->_constants->META_PREFIX . 'schedule_start' ] ) ? $this->_helper_functions->get_scheduler_date_field_value( $_POST[ $this->_constants->META_PREFIX . 'schedule_start' ] ) : '';
            $schedule_expire           = isset( $_POST[ $this->_constants->META_PREFIX . 'schedule_expire' ] ) ? $this->_helper_functions->get_scheduler_date_field_value( $_POST[ $this->_constants->META_PREFIX . 'schedule_expire' ] ) : '';
            $schedule_start_error_msg  = isset( $_POST[ $this->_constants->META_PREFIX . 'schedule_start_error_msg' ] ) ? wp_kses( $_POST[ $this->_constants->META_PREFIX . 'schedule_start_error_msg' ] , $allowed_html ) : '';
            $schedule_expire_error_msg = isset( $_POST[ $this->_constants->META_PREFIX . 'schedule_expire_error_msg' ] ) ? wp_kses( $_POST[ $this->_constants->META_PREFIX . 'schedule_expire_error_msg' ] , $allowed_html ) : '';

            $coupon->set_advanced_prop( 'schedule_start' , $schedule_start );
            $coupon->set_advanced_prop( 'schedule_end' , $schedule_expire );
            $coupon->set_advanced_prop( 'schedule_start_error_msg' , $schedule_start_error_msg );
            $coupon->set_advanced_prop( 'schedule_expire_error_msg' , $schedule_expire_error_msg );

            if ( $schedule_expire ) {
                $timezone = new \DateTimeZone( $this->_helper_functions->get_site_current_timezone() );
                $datetime = \DateTime::createFromFormat( 'Y-m-d H:i:s' , $schedule_expire , $timezone );
                $datetime->setTime( 0 , 0 , 0 );
                update_post_meta( $coupon_id , 'date_expires' , $datetime->getTimestamp() );
                update_post_meta( $coupon_id , 'expiry_date' , $datetime->format( 'Y-m-d' ) );
            } else {
                update_post_meta( $coupon_id , 'date_expires' , '' );
                update_post_meta( $coupon_id , 'expiry_date' , '' );
            }

        }

        // Set coupon as auto apply
        if ( \ACFWF()->Helper_Functions->is_module( Plugin_Constants::AUTO_APPLY_MODULE ) ) {

            $auto_apply_coupon = isset( $_POST[ $this->_constants->META_PREFIX . 'auto_apply_coupon' ] ) && $_POST[ $this->_constants->META_PREFIX . 'auto_apply_coupon' ] == 'yes';
            $coupon->set_advanced_prop( 'auto_apply_coupon' , $auto_apply_coupon );
        }

        // apply notification module 
        if ( \ACFWF()->Helper_Functions->is_module( Plugin_Constants::APPLY_NOTIFICATION_MODULE ) ) {

            $enable_apply_notification   = isset( $_POST[ $this->_constants->META_PREFIX . 'enable_apply_notification' ] ) && $_POST[ $this->_constants->META_PREFIX . 'enable_apply_notification' ] == 'yes';            
            $apply_notification_message  = isset( $_POST[ $this->_constants->META_PREFIX . 'apply_notification_message' ] ) ? wp_kses( $_POST[ $this->_constants->META_PREFIX . 'apply_notification_message' ] , $allowed_html ) : '';
            $apply_notification_btn_text = isset( $_POST[ $this->_constants->META_PREFIX . 'apply_notification_btn_text' ] ) ? sanitize_text_field( $_POST[ $this->_constants->META_PREFIX . 'apply_notification_btn_text' ] ) : '';
            $apply_notification_btn_type = isset( $_POST[ $this->_constants->META_PREFIX . 'apply_notification_type' ] ) ? sanitize_text_field( $_POST[ $this->_constants->META_PREFIX . 'apply_notification_type' ] ) : '';
            
            $coupon->set_advanced_prop( 'enable_apply_notification' , $enable_apply_notification );
            
            if ( $enable_apply_notification == 'yes' ) {

                $coupon->set_advanced_prop( 'apply_notification_message' , $apply_notification_message );
                $coupon->set_advanced_prop( 'apply_notification_btn_text' , $apply_notification_btn_text );
                $coupon->set_advanced_prop( 'apply_notification_type' , $apply_notification_btn_type );
            }
        }
        
        // Usage limits module
        if ( \ACFWF()->Helper_Functions->is_module( Plugin_Constants::USAGE_LIMITS_MODULE ) ) {
            $reset_usage_limit_period = isset( $_POST[ $this->_constants->META_PREFIX . 'reset_usage_limit_period' ] ) && $_POST[ $this->_constants->META_PREFIX . 'reset_usage_limit_period' ] ? sanitize_text_field( $_POST[ $this->_constants->META_PREFIX . 'reset_usage_limit_period' ] ) : 'none';
            $coupon->set_advanced_prop( 'reset_usage_limit_period' , $reset_usage_limit_period );
        }
    }

    /**
     * Remove coupon from apply notifications cache on delete.
     * 
     * @since 2.0
     * @access public
     * 
     * @param int $coupon_id Coupon ID.
     */
    public function remove_coupon_from_global_options_cache( $coupon_id ) {

        if ( get_post_type( $coupon_id ) !== 'shop_coupon' )
            return;

        remove_action( 'save_post' , array( \ACFWF()->Edit_Coupon , 'save_url_coupons_data' ) , 10 );

        foreach ( $this->_constants->CACHE_OPTIONS() as $cache_option ) {

            $cache = \ACFWF()->Helper_Functions->get_option( $cache_option , array() );
            $key   = array_search( $coupon_id , $cache );

            if ( $key !== false && in_array( $coupon_id , $cache ) ) {

                if ( $key >= 0 ) unset( $cache[ $key ] );

                update_option( $cache_option , $cache );
            }
            
        }
    }




    /*
    |--------------------------------------------------------------------------
    | Fulfill implemented interface contracts
    |--------------------------------------------------------------------------
    */

    /**
     * Execute codes that needs to run plugin activation.
     *
     * @since 2.0
     * @access public
     * @implements ACFWP\Interfaces\Initializable_Interface
     */
    public function initialize() {
    }

    /**
     * Execute Edit_Coupon class.
     *
     * @since 2.0
     * @access public
     * @inherit ACFWP\Interfaces\Model_Interface
     */
    public function run() {

        add_action( 'acfw_before_save_coupon' , array( $this , 'save_coupon_data' ) , 10 , 2 );
        add_action( 'woocommerce_coupon_options_usage_restriction' , array( $this , 'display_exclude_coupons_field' ) );

        // Auto apply
        if ( \ACFWF()->Helper_Functions->is_module( Plugin_Constants::AUTO_APPLY_MODULE ) )
            add_action( 'add_meta_boxes' , array( $this , 'register_auto_apply_metabox' ) , 10 , 2 );

        // Shipping Overrides module
        if ( \ACFWF()->Helper_Functions->is_module( Plugin_Constants::SHIPPING_OVERRIDES_MODULE ) ) {

            add_filter( 'woocommerce_coupon_data_tabs' ,   array( $this , 'shipping_overrides_admin_data_tab' ) , 70 , 1 );
            add_action( 'woocommerce_coupon_data_panels' , array( $this , 'shipping_overrides_admin_data_panel' ) );
        }
        
        // Scheduler module
        if ( \ACFWF()->Helper_Functions->is_module( Plugin_Constants::SCHEDULER_MODULE ) ) {

            add_filter( 'woocommerce_coupon_data_tabs' ,   array( $this , 'scheduler_admin_data_tab' ) , 40 , 1 );
            add_action( 'woocommerce_coupon_data_panels' , array( $this , 'scheduler_admin_data_panel' ) );
        }
        
        // Usage limit
        if ( \ACFWF()->Helper_Functions->is_module( Plugin_Constants::USAGE_LIMITS_MODULE ) ) {
            add_action( 'woocommerce_coupon_options_usage_limit' , array( $this , 'advanced_usage_limits_fields' ) , 10 , 2 );
        }

        // Loyalty programs
        if ( \ACFWF()->Helper_Functions->is_module( Plugin_Constants::LOYALTY_PROGRAM_MODULE ) ) {
            add_action( 'add_meta_boxes' , array( $this , 'register_loyalty_program_metabox' ) , 10 , 2 );
        }

        // Add products
        if ( \ACFWF()->Helper_Functions->is_module( Plugin_Constants::ADD_PRODUCTS_MODULE ) ) {

            add_filter( 'woocommerce_coupon_data_tabs' ,   array( $this , 'add_products_admin_data_tab' ) , 30 , 1 );
            add_action( 'woocommerce_coupon_data_panels' , array( $this , 'add_products_admin_data_panel' ) );
        }

        // Apply notification module
        if ( \ACFWF()->Helper_Functions->is_module( Plugin_Constants::APPLY_NOTIFICATION_MODULE ) ) {

            add_filter( 'woocommerce_coupon_data_tabs' ,   array( $this , 'apply_notification_admin_data_tab' ) , 70 , 1 );
            add_action( 'woocommerce_coupon_data_panels' , array( $this , 'apply_notification_admin_data_panel' ) );
        }

        // Remove coupon on global options cache when trashed or deleted.
        add_action( 'wp_trash_post' ,      array( $this , 'remove_coupon_from_global_options_cache' ) );
        add_action( 'before_delete_post' , array( $this , 'remove_coupon_from_global_options_cache' ) );
        add_action( 'untrashed_post' ,     array( $this , 'remove_coupon_from_global_options_cache' ) );
    }

}
