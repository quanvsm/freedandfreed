<?php
namespace ACFWP\Models;

use ACFWP\Abstracts\Abstract_Main_Plugin_Class;

use ACFWP\Interfaces\Model_Interface;
use ACFWP\Interfaces\Initiable_Interface;

use ACFWP\Models\Objects\Advanced_Coupon;


use ACFWP\Helpers\Plugin_Constants;
use ACFWP\Helpers\Helper_Functions;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Model that houses the logic of extending the coupon system of woocommerce.
 * It houses the logic of handling coupon url.
 * Public Model.
 *
 * @since 2.0
 */
class Apply_Notification implements Model_Interface , Initiable_Interface {

    /*
    |--------------------------------------------------------------------------
    | Class Properties
    |--------------------------------------------------------------------------
    */

    /**
     * Property that holds the single main instance of URL_Coupon.
     *
     * @since 2.0
     * @access private
     * @var Apply_Notification
     */
    private static $_instance;

    /**
     * Model that houses all the plugin constants.
     *
     * @since 2.0
     * @access private
     * @var Plugin_Constants
     */
    private $_constants;

    /**
     * Property that houses all the helper functions of the plugin.
     *
     * @since 2.0
     * @access private
     * @var Helper_Functions
     */
    private $_helper_functions;

    /*
    |--------------------------------------------------------------------------
    | Class Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Class constructor.
     *
     * @since 2.0
     * @access public
     *
     * @param Abstract_Main_Plugin_Class $main_plugin      Main plugin object.
     * @param Plugin_Constants           $constants        Plugin constants object.
     * @param Helper_Functions           $helper_functions Helper functions object.
     */
    public function __construct( Abstract_Main_Plugin_Class $main_plugin , Plugin_Constants $constants , Helper_Functions $helper_functions ) {

        $this->_constants        = $constants;
        $this->_helper_functions = $helper_functions;

        $main_plugin->add_to_all_plugin_models( $this );
        $main_plugin->add_to_public_models( $this );

    }

    /**
     * Ensure that only one instance of this class is loaded or can be loaded ( Singleton Pattern ).
     *
     * @since 2.0
     * @access public
     *
     * @param Abstract_Main_Plugin_Class $main_plugin      Main plugin object.
     * @param Plugin_Constants           $constants        Plugin constants object.
     * @param Helper_Functions           $helper_functions Helper functions object.
     * @return Apply_Notification
     */
    public static function get_instance( Abstract_Main_Plugin_Class $main_plugin , Plugin_Constants $constants , Helper_Functions $helper_functions ) {

        if ( !self::$_instance instanceof self )
            self::$_instance = new self( $main_plugin , $constants , $helper_functions );

        return self::$_instance;

    }




    /*
    |--------------------------------------------------------------------------
    | One click appy notification implementation
    |--------------------------------------------------------------------------
    */

    /**
     * One click apply notification implementation for a single coupon.
     * 
     * @since 2.0
     * @access private
     * 
     * @param int          $coupon_id  Coupon ID.
     * @param WC_Discounts $discounts  WooCommerce discounts object.
     */
    private function _apply_notification_single_coupon( $coupon_id , $discounts ) {

        if ( get_post_type( $coupon_id ) !== 'shop_coupon' ) return;

        $coupon = new Advanced_Coupon( $coupon_id );
        $code   = $coupon->get_code();

        // if coupon is already applied or returns a WP_Error object, then don't proceed.
        if ( in_array( $code , \WC()->cart->get_applied_coupons() ) || get_post_status( $coupon_id ) !== 'publish' || is_wp_error( $discounts->is_coupon_valid( $coupon ) ) )
            return;
        
        $message     = $coupon->get_advanced_prop( 'apply_notification_message' , __( "Your current cart is eligible for a coupon." , 'advanced-coupons-for-woocommerce' ) );
        $button      = '<button class="acfw_apply_notification button" value="' . esc_attr( $code ) . '">' . $coupon->get_advanced_prop( 'apply_notification_btn_text' , __( "Apply Coupon" , 'advanced-coupons-for-woocommerce' ) ) . '</button>';
        $notice_type = $coupon->get_advanced_prop( 'apply_notification_type' , 'notice' );

        wc_print_notice( $message . $button , $notice_type );
    }

    /**
     * Print one click apply notification javascript.
     * 
     * @since 2.0
     * @access private
     */
    private function _print_apply_notification_javascript() {
        ?>
        <script type="text/javascript">
        jQuery( document ).ready( function($) {
            $( '.woocommerce' ).on( 'click' , '.acfw_apply_notification' , function() {

                $( 'input[name="coupon_code"]' ).val( $(this).val() );
                $( '.woocommerce-cart-form :input[type=submit]' ).trigger( 'click' );
            });
        });
        </script>
        <?php
    }

    /**
     * Implement apply notifications.
     * 
     * @since 2.0
     * @access public
     */
    public function implement_apply_notifications() {

        $apply_notifications = \ACFWF()->Helper_Functions->get_option( $this->_constants->APPLY_NOTIFICATION_CACHE , array() );

        if ( ! is_array( $apply_notifications ) || empty( $apply_notifications ) )
            return;

        $discounts = new \WC_Discounts( \WC()->cart );
        foreach ( $apply_notifications as $coupon_id )
            $this->_apply_notification_single_coupon( $coupon_id , $discounts );

        $this->_print_apply_notification_javascript();
    }

    /**
     * Clear auto apply cache.
     * 
     * @since 2.0
     */
    private function _clear_apply_notification_cache() {

        update_option( $this->_constants->APPLY_NOTIFICATION_CACHE , array() );
    }

    /**
     *  Rebuild auto apply cache.
     * 
     * @since 2.0
     * 
     * @return array $verified List of apply notification coupons.
     */
    private function _rebuild_apply_notification_cache() {

        $apply_notifications = get_option( $this->_constants->APPLY_NOTIFICATION_CACHE , array() );
        $verified            = array_filter( $apply_notifications , function( $c ) {
            return get_post_type( $c ) == 'shop_coupon' && get_post_status( $c ) == 'publish';
        } );

        update_option( $this->_constants->APPLY_NOTIFICATION_CACHE , array_unique( $verified ) );
        return $verified;
    }

    /**
     * Render clear auto apply cache settings field.
     * 
     * @since 2.0
     * @access public
     * 
     * @param array $value Field value data.
     */
    public function render_rebuild_apply_notification_cache_setting_field( $value ) {
        ?>

        <tr valign="top">
            <th scope="row">
                <label><?php echo sanitize_text_field( $value[ 'title' ] ); ?></label>
            </th>
            <td>
                <div class="btn-wrap" id="<?php echo $value['id']; ?>" data-nonce="<?php echo wp_create_nonce( 'acfw_rebuild_apply_notification_cache' ); ?>">
                    <button type="button" class="button-primary rebuild_apply_notification_cache" value="rebuild">
                        <?php _e( 'Rebuild apply notification coupons cache' , 'advanced-coupons-for-woocommerce' ); ?>
                    </button>
                    <button type="button" class="button clear_apply_notification_cache" value="clear">
                        <?php _e( 'Clear apply notification coupons cache' , 'advanced-coupons-for-woocommerce' ); ?>
                    </button>
                    <span class="acfw-spinner" style="display:none;">
                        <img src="<?php echo $this->_constants->IMAGES_ROOT_URL . 'spinner.gif'; ?>">
                    </span>
                </div>
                <p class="acfw-notice" style="display:none; color: #46B450;"></p>
                <p class="description"><?php echo $value[ 'desc' ]; ?></p>
            </td>
        </tr>

        <script type="text/javascript">
        jQuery(document).ready(function($){

            $('#<?php echo $value['id']; ?> button').on( 'click', function() {

                var $button  = $(this),
                    $parent  = $button.closest('.btn-wrap'),
                    $spinner = $parent.find('.acfw-spinner'),
                    $row     = $button.closest('tr'),
                    $notice  = $row.find( '.acfw-notice' );

                $button.prop( 'disabled' , true );
                $spinner.show();

                $.post( ajaxurl , {
                    action : 'acfw_rebuild_apply_notification_cache',
                    nonce  : $parent.data('nonce'),
                    type   : $button.val()
                }, function( response ) {

                    if ( response.status == 'success' ) {

                        $notice.text( response.message );
                        $notice.show();

                        setTimeout(function() {
                            $notice.fadeOut('fast');
                        }, 5000);

                    } else 
                        alert( response.err_msg );

                }, 'json' ).always(function() {
                    $button.prop( 'disabled' , false );
                    $spinner.hide();
                });
            });
        });
        </script>

        <?php
    }




    /*
    |--------------------------------------------------------------------------
    | AJAX Functions
    |--------------------------------------------------------------------------
    */

    /**
     * AJAX rebuild auto apply cache.
     * 
     * @since 2.0
     * @access public
     */
    public function ajax_rebuild_apply_notification_cache() {

        if ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX )
            $response = array( 'status' => 'fail' , 'error_msg' => __( 'Invalid AJAX call' , 'advanced-coupons-for-woocommerce' ) );
        elseif ( ! isset( $_POST[ 'nonce' ] ) || ! wp_verify_nonce( $_POST[ 'nonce' ] , 'acfw_rebuild_apply_notification_cache' ) || ! current_user_can( 'manage_woocommerce' ) )
            $response = array( 'status' => 'fail' , 'error_msg' => __( 'You are not allowed to do this' , 'advanced-coupons-for-woocommerce' ) );
        else {

            if ( $_POST[ 'type' ] == 'clear' ) {

                $this->_clear_apply_notification_cache();
                $response = array(
                    'status' => 'success',
                    'message' => __( 'Appy notification coupons cache have been cleared successfully.' , 'advanced-coupons-for-woocommerce' )
                );

            } else {

                $verified = $this->_rebuild_apply_notification_cache();
                $response = array(
                    'status'  => 'success',
                    'message' => sprintf( __( 'Appy notification coupons cache has been rebuilt successfully. %s coupon(s) have been validated.' , 'advanced-coupons-for-woocommerce' ) , count( $verified ) )
                );
            }            
        }

        @header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
        echo wp_json_encode( $response );
        wp_die();
    }





    /*
    |--------------------------------------------------------------------------
    | Fulfill implemented interface contracts
    |--------------------------------------------------------------------------
    */

    /**
     * Execute codes that needs to run plugin activation.
     *
     * @since 2.0
     * @access public
     * @implements ACFWP\Interfaces\Initializable_Interface
     */
    public function initialize() {

        add_action( 'wp_ajax_acfw_rebuild_apply_notification_cache' , array( $this , 'ajax_rebuild_apply_notification_cache' ) );
    }

    /**
     * Execute Apply_Notification class.
     *
     * @since 2.0
     * @access public
     * @inherit ACFWP\Interfaces\Model_Interface
     */
    public function run() {

        add_action( 'woocommerce_admin_field_acfw_rebuild_apply_notification_cache' , array( $this , 'render_rebuild_apply_notification_cache_setting_field' ) );

        if ( ! \ACFWF()->Helper_Functions->is_module( Plugin_Constants::APPLY_NOTIFICATION_MODULE ) )
            return;

        add_action( 'woocommerce_before_cart' , array( $this , 'implement_apply_notifications' ) , 20 );
    }

}
