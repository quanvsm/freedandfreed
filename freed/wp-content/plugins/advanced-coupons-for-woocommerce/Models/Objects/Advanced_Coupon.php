<?php

namespace ACFWP\Models\Objects;

use ACFWP\Helpers\Plugin_Constants;

/**
 * Model that houses the data model of an advanced coupon object.
 *
 * @since 2.0
 */
class Advanced_Coupon extends \ACFWF\Models\Objects\Advanced_Coupon {

    /*
    |--------------------------------------------------------------------------
    | Class Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Class constructor.
     *
     * @since 2.0
     * @access public
     *
     * @param mixed $code WC_Coupon ID, code or object.
     */
    public function __construct( $code ) {

        // construct parent object and set the code.
        parent::__construct( $code );
    }

    /**
     * Return extra default advanced data.
     * 
     * @since 2.0
     * @access protected
     * 
     * @return array Extra default advanced data.
     */
    protected function extra_default_advanced_data() {

        return array(
            'bogo_auto_add_products'      => false,
            'add_before_conditions'       => false,
            'add_products_data'           => array(),
            'excluded_coupons'            => array(),
            'shipping_overrides'          => array(),
            'schedule_start'              => '',
            'schedule_end'                => '',
            'schedule_expire'             => '',
            'schedule_start_error_msg'    => '',
            'schedule_expire_error_msg'   => '',
            'auto_apply_coupon'           => false,
            'enable_apply_notification'   => false,
            'apply_notification_message'  => '',
            'apply_notification_btn_text' => '',
            'apply_notification_type'     => 'info',
            'reset_usage_limit_period'    => 'none',
            'loyalty_program_user'        => 0,
            'loyalty_program_points'      => 0,
            'cart_condition_display_notice_auto_apply' => ''
        );
    }

    /**
     * Advanced read property.
     * 
     * @since 2.0
     * @access protected
     * 
     * @param mixed  $raw_data     Property raw data value.
     * @param string $prop         Property name.
     * @param string $default_data Default data value.
     * @param array  $meta_data    Coupon metadata list.
     * @return mixed Data value.
     */
    protected function advanced_read_property( $raw_data , $prop , $default_data , $meta_data ) {

        $data = null;

        switch ( $prop ) {

            case 'schedule_start_error_msg' :
            case 'schedule_expire_error_msg' :
            case 'apply_notification_message' :
            case 'apply_notification_btn_text' :
            case 'apply_notification_type' :
            case 'reset_usage_limit_period' :
            case 'cart_condition_display_notice_auto_apply' :
            case 'loyalty_program_user' :
            case 'loyalty_program_points' :
                $data = ! empty( $raw_data ) ? $raw_data : $default_data;
                break;

            case 'schedule_end' :
                $data = ! empty( $raw_data ) ? $raw_data : $this->_get_schedule_expire( $meta_data );
                break;

            case 'schedule_expire' :
                $data = $this->_get_schedule_expire( $meta_data );
                break;

            case 'add_products_data' :
            case 'excluded_coupons' :
            case 'shipping_overrides' :
                $data = ( is_array( $raw_data ) && ! empty( $raw_data ) ) ? $raw_data : $default_data;
                break;
            
            case 'auto_apply_coupon' :
                $auto_apply_coupons = $this->_helper_functions->get_option( ACFWP()->Plugin_Constants->AUTO_APPLY_COUPONS , array() );
                $data               = in_array( $this->id , $auto_apply_coupons );
                break;

            case 'enable_apply_notification' :
                $enable_apply_notification = $this->_helper_functions->get_option( ACFWP()->Plugin_Constants->APPLY_NOTIFICATION_CACHE , array() );
                $data                      = in_array( $this->id , $enable_apply_notification );
                break;
        }

        return $data ? $data : $raw_data;
    }

    /**
     * Get extra get advanced prop global value.
     * 
     * @since 2.0
     * @access protected
     * 
     * @param string $prop Property name.
     * @return string Property global option name.
     */
    protected function get_extra_advanced_prop_global_value( $prop ) {

        $option = '';

        switch ( $prop ) {

            case 'schedule_start_error_msg' :
                $option = ACFWP()->Plugin_Constants->SCHEDULER_START_ERROR_MESSAGE;
                break;

            case 'schedule_expire_error_msg' :
                $option = ACFWP()->Plugin_Constants->SCHEDULER_EXPIRE_ERROR_MESSAGE;
                break;
        }

        return $option;
    }

    /**
     * Check if to skip saving the advanced prop value as post meta.
     * 
     * @since 2.0
     * @since 2.1 Prevent saving _acfw_schedule_expire meta.
     * @access protected
     * 
     * @param mixed  $value Property value.
     * @param string $prop  Property name.
     * @param bool True if skip, false otherwise.
     */
    protected function is_skip_save_advanced_prop( $value , $prop ) {
        
        if ( $prop === 'auto_apply_coupon' && $this->_helper_functions->is_module( Plugin_Constants::AUTO_APPLY_MODULE ) ) {
            $this->save_prop_to_global_option_cache( ACFWP()->Plugin_Constants->AUTO_APPLY_COUPONS , $value );
            return true;
        }
        
        if ( $prop == 'enable_apply_notification' && $this->_helper_functions->is_module( Plugin_Constants::APPLY_NOTIFICATION_MODULE ) ) {
            $this->save_prop_to_global_option_cache( ACFWP()->Plugin_Constants->APPLY_NOTIFICATION_CACHE , $value );
            return true;
        }

        if ( $prop === 'schedule_expire' ) return true;

        return false;
    }

    /**
     * Get the "Add Products" data with backwards compatiblity for the "Add Free Products" data.
     * 
     * @since 2.0
     * @access public
     */
    public function get_add_products_data($context = 'view') {

        $add_products  = $this->get_advanced_prop( 'add_products_data' , array() );
        $free_products = $this->get_advanced_prop( 'add_free_products' , array() );

        if ( ( ! is_array( $add_products ) || empty( $add_products ) ) && ! empty( $free_products ) ) {

            foreach( $free_products as $product_id => $quantity ) {

                $product        = wc_get_product( $product_id );
                $add_products[] = array(
                    'product_id'    => $product_id,
                    'product_label' => $product->get_formatted_name(),
                    'quantity'      => $quantity,
                    'discount_type'    => 'override',
                    'discount_value'   => 0
                );
            }
        }

        // format discount value to localized version for editing context.
        if ( 'edit' === $context ) {
            $add_products = array_map( function($p) {
                $p['discount_value'] = wc_format_localized_price($p['discount_value']);
                return $p;
            } , $add_products );
        }

        return $add_products;
    }

    /**
     * Get schedule expire value.
     * 
     * @since 2.0
     * @access private
     * 
     * @param array $meta_data Coupon meta data.
     * @param string Date in Y-m-d format.
     */
    private function _get_schedule_expire( $meta_data ) {

        if ( isset( $meta_data[ 'expiry_date' ][0] ) && $meta_data[ 'expiry_date' ][0] )
            return $meta_data[ 'expiry_date' ][0];

        if ( isset( $meta_data[ 'date_expires' ][0] ) && $meta_data[ 'date_expires' ][0] ) {

            $timezone = new \DateTimeZone( $this->_helper_functions->get_site_current_timezone() );
            $datetime = new \DateTime( "today" , $timezone );

            $datetime->setTimestamp( $meta_data[ 'date_expires' ][0] );
            return $datetime->format( 'Y-m-d' );
        }

        return '';
    }

    /**
     * Get shipping overrides data for editing context.
     * 
     * @since 
     */
    public function get_shipping_overrides_data_edit() {
        
        $overrides = array_map( function($o) {
            $o[ 'discount_value' ] = wc_format_localized_price( $o[ 'discount_value' ] );
            return $o;
        } , $this->get_advanced_prop( 'shipping_overrides' , array() ) );

        return $overrides;
    }
}