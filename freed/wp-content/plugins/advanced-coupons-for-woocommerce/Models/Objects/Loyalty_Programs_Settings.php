<?php
namespace ACFWP\Models\Objects;

use ACFWP\Helpers\Plugin_Constants;
use ACFWP\Helpers\Helper_Functions;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Loyalty_Programs_Settings extends \WC_Settings_Page {

    /*
    |--------------------------------------------------------------------------
    | Class Properties
    |--------------------------------------------------------------------------
    */

    /**
     * Model that houses all the plugin constants.
     *
     * @since 2.2
     * @access private
     * @var Plugin_Constants
     */
    private $_constants;

    /**
     * Property that houses all the helper functions of the plugin.
     *
     * @since 2.2
     * @access private
     * @var Helper_Functions
     */
    private $_helper_functions;




    /*
    |--------------------------------------------------------------------------
    | Class Methods
    |--------------------------------------------------------------------------
    */

    /**
     * ACFW_Settings constructor.
     *
     * @since 2.2
     * @access public
     *
     * @param Plugin_Constants $constants        Plugin constants object.
     * @param Helper_Functions $helper_functions Helper functions object.
     */
    public function __construct( Plugin_Constants $constants , Helper_Functions $helper_functions ) {

        $this->_constants        = $constants;
        $this->_helper_functions = $helper_functions;
        $this->id                = 'acfw_loyalty_programs';
        $this->label             = __( 'Loyalty Programs' , 'advanced-coupons-for-woocommerce' );

        add_action( 'woocommerce_settings_save_' . $this->id , array( $this , 'save' ) );

        do_action( 'acfw_loyalty_programs_settings_construct' );
    }

    /**
     * Output the settings.
     *
     * @since 2.2
     * @access public
     */
    public function output() {

        \WC_Admin_Settings::output_fields( $this->_get_loyalty_program_section_options() );
    }

    /**
     * Save settings.
     *
     * @since 2.2
     * @access public
     */
    public function save() {

        global $current_section;

        $settings = $this->_get_loyalty_program_section_options();
        \WC_Admin_Settings::save_fields( $settings );
    }

        /**
     * Get Loyalty Programs section options.
     *
     * @since 2.2
     * @access private
     *
     * @return array
     */
    private function _get_loyalty_program_section_options() {

        $currency      = get_woocommerce_currency_symbol();
        $dollar        = wc_price( 1 );
        $decimal       = wc_get_price_decimal_separator();
        $price_pattern = '^[+]?([' . $decimal . ']\d+|\d+([' . $decimal . ']\d+)?)$';

        return array(

            array(
                'title' => '',
                'type'  => 'title',
                'id'    => 'acfw_loyalty_prog_main_title'
            ),

            array(
                'title'    => __( 'Price to points earned ratio' , 'advanced-coupons-for-woocommerce' ),
                'type'     => 'text',
                'desc_tip' => sprintf( __( 'Define the ratio of points earned for each %s spent. Example: Setting a ratio of 1 means 1 point is earned for every %s spent. Setting a ratio 5 means 5 points are earned for every %s spent.' , 'advanced-coupons-for-woocommerce' ) , $currency , $dollar , $dollar ),
                'default'  => 1,
                'class'    => 'wc_input_price',
                'id'       => $this->_constants->LP_COST_POINTS_RATIO,
                'custom_attributes' => array(
                    'pattern' => $price_pattern,
                    'required' => true,
                )
            ),

            array(
                'title'    => __( 'Points to price redeemed ratio' , 'advanced-coupons-for-woocommerce' ),
                'type'     => 'number',
                'desc_tip' => sprintf( __( 'Define the worth of each point. Example: Setting a points to price redeemed ratio of 1 means 1 point is worth %s. Setting a ratio of 10 means 10 points is worth %s.' , 'advanced-coupons-for-woocommerce' ) , $dollar , $dollar ),
                'default'  => 10,
                'id'       => $this->_constants->LP_REDEEM_POINTS_RATIO,
                'custom_attributes' => array(
                    'min' => 1,
                    'required' => true
                )
            ),

            array(
                'title'       => __( 'Points name' , 'advanced-coupons-for-woocommerce' ),
                'type'        => 'text',
                'desc_tip'    => __( 'By default, points are called “Points” throughout the store. You can override the name of your points here.' , 'advanced-coupons-for-woocommerce' ),
                'placeholder' => 'Points',
                'id'          => $this->_constants->LP_POINTS_NAME
            ),

            array(
                'title'    => __( 'Points calculation options' , 'advanced-coupons-for-woocommerce' ),
                'type'     => 'acfw_points_calculation_options_field',
                'id'       => $this->_constants->LP_POINTS_CALCULATION_OPTIONS,
                'options'  => array(
                    'discounts' => __( 'Discounts' , 'advanced-coupons-for-woocommerce' ),
                    'tax'       => __( 'Tax' , 'advanced-coupons-for-woocommerce' ),
                    'shipping'  => __( 'Shipping' , 'advanced-coupons-for-woocommerce' ),
                    'fees'      => __( 'Fees' , 'advanced-coupons-for-woocommerce' )
                ),
                'default' => array(
                    'discounts' => 'yes',
                    'tax'       => 'yes',
                    'shipping'  => 'no',
                    'fees'      => 'no'
                ),
                'tooltips' => array(
                    'discounts' => __( 'If this option is checked, points will be calculated on orders with the discount amount included as part of the calculation.' , 'advanced-coupons-for-woocommerce' ),
                    'tax'       => __( 'If this option is checked, points will be calculated on orders with the tax amount included as part of the calculation.' , 'advanced-coupons-for-woocommerce' ),
                    'shipping'  => __( 'If this option is checked, points will be calculated on orders with the shipping amount included as part of the calculation.' , 'advanced-coupons-for-woocommerce' ),
                    'fees'      => __( 'If this option is checked, points will be calculated on orders with the fee amount included as part of the calculation.' , 'advanced-coupons-for-woocommerce' )
                )
            ),

            array(
                'title'    => __( 'Disallow points accumulations for roles' , 'advanced-coupons-for-woocommerce' ),
                'type'     => 'multiselect',
                'desc_tip' => __( 'Choose which roles should NOT accumulate points for purchases. If users with those roles make a purchase, they will not accumulate points, nor will they see the points section on their My Account or Checkout pages.' , 'advanced-coupons-for-woocommerce' ),
                'id'       => $this->_constants->LP_DISALLOWED_ROLES,
                'class'    => 'wc-enhanced-select',
                'options'  => $this->_helper_functions->get_all_user_roles()
            ),

            array(
                'title'    => __( 'Minimum threshold to earn points' , 'advanced-coupons-for-woocommerce' ),
                'type'     => 'text',
                'class'    => 'wc_input_price',
                'desc_tip' => __( 'Set a minimum spend for a customer to be eligible to accumulate points for an order. Once an order is eligible, the customer will receive points for the entire subtotal.' , 'advanced-coupons-for-woocommerce' ),
                'id'       => $this->_constants->LP_MINIMUM_POINTS_THRESHOLD,
                'default'  => 0,
                'custom_attributes' => array(
                    'pattern' => $price_pattern,
                    'required' => true
                )
            ),

            array(
                'title'    => __( 'Minimum points allowed for redemption' , 'advanced-coupons-for-woocommerce' ),
                'type'     => 'number',
                'desc_tip' => __( 'Set the minimum number of points allowed to be redeemed as a discount a coupon.' , 'advanced-coupons-for-woocommerce' ),
                'id'       => $this->_constants->LP_MINIMUM_POINTS_REDEEM,
                'default'  => 0,
                'custom_attributes' => array(
                    'min' => 0,
                    'required' => true
                )
            ),

            array(
                'title'    => __( 'Points to earn message in cart' , 'advanced-coupons-for-woocommerce' ),
                'type'     => 'textarea',
                'desc_tip' => __( 'Shows a message on the cart page indicating how many points the current order will earn. Use {points} placeholder in your message for displaying the points amount. Leave blank to disable.' , 'advanced-coupons-for-woocommerce' ),
                'default'  => sprintf( __( 'This order will earn %s points.' , 'advanced-coupons-for-woocommerce' ) , '{points}' ),
                'id'       => $this->_constants->LP_POINTS_EARN_CART_MESSAGE
            ),

            array(
                'title'    => __( 'Points to earn message in checkout' , 'advanced-coupons-for-woocommerce' ),
                'type'     => 'textarea',
                'title'    => __( 'Points to earn message in checkout' , 'advanced-coupons-for-woocommerce' ),
                'desc_tip' => __( 'Shows a message on the checkout page indicating how many points the current order will earn. Use {points} placeholder in your message for displaying the points amount. Leave blank to disable.' , 'advanced-coupons-for-woocommerce' ),
                'default'  => sprintf( __( 'This order will earn %s points.' , 'advanced-coupons-for-woocommerce' ) , '{points}' ),
                'id'       => $this->_constants->LP_POINTS_EARN_CHECKOUT_MESSAGE
            ),

            array(
                'title'    => __( 'Points to earn message in single product page' , 'advanced-coupons-for-woocommerce' ),
                'type'     => 'textarea',
                'desc_tip' => __( 'Shows a message on the single product page indicating how many points this particular product will earn. Use the {points} placeholder in your message for displaying the points amount. Leave blank to disable.' , 'advanced-coupons-for-woocommerce' ),
                'id'       => $this->_constants->LP_POINTS_EARN_PRODUCT_MESSAGE
            ),

            array(
                'title'    => __( "Number of days of inactivity for a user's points will expire" , 'advanced-coupons-for-woocommerce' ),
                'type'     => 'number',
                'desc_tip' => __( "Number of days for a user's points will expire after being inactive." , 'advanced-coupons-for-woocommerce' ),
                'desc'     => __( 'Days' , 'advanced-coupons-for-woocommerce' ),
                'id'       => $this->_constants->LP_INACTIVE_DAYS_POINTS_EXPIRE,
                'default'  => 365,
                'custom_attributes' => array(
                    'min'      => 0,
                    'required' => true
                )
            ),
            
            array(
                'title'    => __( 'Redeemed coupon expiration period' , 'advanced-coupons-for-woocommerce' ),
                'type'     => 'number',
                'desc_tip' => __( 'Number of days a coupon should be a valid after redeeming.' , 'advanced-coupons-for-woocommerce' ),
                'desc'     => __( 'Days' , 'advanced-coupons-for-woocommerce' ),
                'id'       => $this->_constants->LP_COUPON_EXPIRE_PERIOD,
                'default'  => 365,
                'custom_attributes' => array(
                    'min'      => 0,
                    'required' => true
                )
            ),

            array(
                'title'    => __( 'Points expiry message' , 'advanced-coupons-for-woocommerce' ),
                'type'     => 'textarea',
                'desc_tip' => __( "Shows a message on the user's My Points page indicating when their points will expire after being inactive. Use the {date_expire} placeholder in your message for displaying the expiry date. Leave blank to disable." , 'advanced-coupons-for-woocommerce' ),
                'id'       => $this->_constants->LP_POINTS_EXPIRY_MESSAGE,
                'default'  => sprintf( __( 'Points is valid until %s. Redeem or earn more points to extend validity.' , 'advanced-coupons-for-woocommerce' ) , '{date_expire}' )
            ),

            /**
             * Settings below are commented out for now and will be worked on individually in different JIRA tickets.
             */

            // Checkbox start.

            array(
                'title'         => __( 'Actions for earning points' , 'advanced-coupons-for-woocommerce' ),
                'type'          => 'checkbox',
                'id'            => $this->_constants->LP_EARN_ACTION_BUY_PRODUCT,
                'desc'          => __( 'Purchasing products' , 'advanced-coupons-for-woocommerce' ),
                'default'       => 'yes',
                'checkboxgroup' => 'start',
            ),

            array(
                'type'          => 'checkbox',
                'id'            => $this->_constants->LP_EARN_ACTION_PRODUCT_REVIEW,
                'desc'          => __( 'Leaving a product review' , 'advanced-coupons-for-woocommerce' ),
                'class'         => 'checkbox-toggle',
                'checkboxgroup' => '',
                'custom_attributes' => array(
                    'data-toggle' => $this->_constants->LP_EARN_POINTS_PRODUCT_REVIEW
                )
            ),

            array(
                'type'          => 'checkbox',
                'id'            => $this->_constants->LP_EARN_ACTION_BLOG_COMMENT,
                'desc'          => __( 'Commenting on a blog post' , 'advanced-coupons-for-woocommerce' ),
                'class'         => 'checkbox-toggle',
                'checkboxgroup' => '',
                'custom_attributes' => array(
                    'data-toggle' => $this->_constants->LP_EARN_POINTS_BLOG_COMMENT
                )
            ),

            array(
                'type'          => 'checkbox',
                'id'            => $this->_constants->LP_EARN_ACTION_USER_REGISTER,
                'desc'          => __( 'Registering as a user/customer' , 'advanced-coupons-for-woocommerce' ),
                'class'         => 'checkbox-toggle',
                'default'       => 'yes',
                'checkboxgroup' => '',
                'custom_attributes' => array(
                    'data-toggle' => $this->_constants->LP_EARN_POINTS_USER_REGISTER
                )
            ),

            array(
                'type'          => 'checkbox',
                'id'            => $this->_constants->LP_EARN_ACTION_FIRST_ORDER,
                'desc'          => __( 'After completing first order' , 'advanced-coupons-for-woocommerce' ),
                'class'         => 'checkbox-toggle',
                'default'       => 'yes',
                'checkboxgroup' => '',
                'custom_attributes' => array(
                    'data-toggle' => $this->_constants->LP_EARN_POINTS_FIRST_ORDER
                )
            ),

            array(
                'type'          => 'checkbox',
                'id'            => $this->_constants->LP_EARN_ACTION_BREAKPOINTS,
                'desc'          => __( 'Spending over a certain amount (breakpoints)' , 'advanced-coupons-for-woocommerce' ),
                'class'         => 'checkbox-toggle',
                'default'       => 'yes',
                'checkboxgroup' => '',
                'custom_attributes' => array(
                    'data-toggle' => $this->_constants->LP_EARN_POINTS_BREAKPOINTS
                )
            ),

            array(
                'type'          => 'checkbox',
                'id'            => $this->_constants->LP_EARN_ACTION_ORDER_PERIOD,
                'desc'          => __( 'Extra points to earn during period' , 'advanced-coupons-for-woocommerce' ),
                'class'         => 'checkbox-toggle',
                'checkboxgroup' => 'end',
                'custom_attributes' => array(
                    'data-toggle' => $this->_constants->LP_EARN_POINTS_ORDER_PERIOD
                )
            ),

            // Checkbox end.

            array(
                'title'   => __( 'Points earned when leaving a product review' , 'advanced-coupons-for-woocommerce' ),
                'type'    => 'number',
                'id'      => $this->_constants->LP_EARN_POINTS_PRODUCT_REVIEW,
                'desc'    => '',
                'custom_attributes' => array(
                    'min' => 1
                )
            ),

            array(
                'title'   => __( 'Points earned when commenting on a blog post' , 'advanced-coupons-for-woocommerce' ),
                'type'    => 'number',
                'id'      => $this->_constants->LP_EARN_POINTS_BLOG_COMMENT,
                'desc'    => '',
                'custom_attributes' => array(
                    'min' => 1
                )
            ),

            array(
                'title'   => __( 'Points earned after registering as a user/customer' , 'advanced-coupons-for-woocommerce' ),
                'type'    => 'number',
                'id'      => $this->_constants->LP_EARN_POINTS_USER_REGISTER,
                'desc'    => '',
                'default' => 10,
                'custom_attributes' => array(
                    'min'      => 1,
                    'required' => true
                )
            ),

            array(
                'title'   => __( 'Points earned after completing the first order' , 'advanced-coupons-for-woocommerce' ),
                'type'    => 'number',
                'id'      => $this->_constants->LP_EARN_POINTS_FIRST_ORDER,
                'desc'    => '',
                'default' => 10,
                'custom_attributes' => array(
                    'min'      => 1,
                    'required' => true
                )
            ),

            array(
                'title'    => __( 'Points earned for spending over set amount breakpoints' , 'advanced-coupons-for-woocommerce' ),
                'type'     => 'acfw_earn_breakpoints_field',
                'id'       => $this->_constants->LP_EARN_POINTS_BREAKPOINTS,
                'desc'     => __( 'Rows with incomplete and/or duplicate breakpoint values will be removed upon saving.' , 'advanced-coupons-for-woocommerce' ),
                'desc_tip' => __( 'An extra amount of points that will be given on top of the regular amount given for an order which total amount (after discounts) exceeds the set amount breakpoint.' , 'advanced-coupons-for-woocommerce' ),
            ),

            array(
                'title'    => __( 'Extra points to earn during period' , 'advanced-coupons-for-woocommerce' ),
                'type'     => 'acfw_earn_orders_period_table_field',
                'id'       => $this->_constants->LP_EARN_POINTS_ORDER_PERIOD,
                'desc_tip' => __( 'An extra amount of points that will be given on top of the regular amount given for an order during the promotional period defined. Note that when dates between rows are overlapping, only the first one matched will be applied.' , 'advanced-coupons-for-woocommerce' ),
                'desc'     => __( 'Rows with incomplete and/or duplicate date and time values will be removed upon saving.' , 'advanced-coupons-for-woocommerce' ),
            ),

            array(
                'type' => 'sectionend',
                'id'   => 'acfw_loyalty_prog_sectionend'
            )
        );
    }


}