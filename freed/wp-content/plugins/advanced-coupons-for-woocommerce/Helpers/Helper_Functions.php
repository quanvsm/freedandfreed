<?php
namespace ACFWP\Helpers;

use ACFWP\Abstracts\Abstract_Main_Plugin_Class;

use ACFWP\Models\Objects\Advanced_Coupon;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Model that houses all the helper functions of the plugin.
 *
 * 1.0.0
 */
class Helper_Functions {

    /*
    |--------------------------------------------------------------------------
    | Class Properties
    |--------------------------------------------------------------------------
    */

    /**
     * Property that holds the single main instance of Helper_Functions.
     *
     * @since 2.0
     * @access private
     * @var Helper_Functions
     */
    private static $_instance;

    /**
     * Model that houses all the plugin constants.
     *
     * @since 2.0
     * @access private
     * @var Plugin_Constants
     */
    private $_constants;
    



    /*
    |--------------------------------------------------------------------------
    | Class Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Class constructor.
     *
     * @since 2.0
     * @access public
     *
     * @param Abstract_Main_Plugin_Class $main_plugin Main plugin object.
     * @param Plugin_Constants           $constants   Plugin constants object.
     */
    public function __construct( Abstract_Main_Plugin_Class $main_plugin = null , Plugin_Constants $constants ) {

        $this->_constants = $constants;
        
        if ( $main_plugin )
            $main_plugin->add_to_public_helpers( $this );

    }

    /**
     * Ensure that only one instance of this class is loaded or can be loaded ( Singleton Pattern ).
     * 
     * @since 2.0
     * @access public
     * 
     * @param Abstract_Main_Plugin_Class $main_plugin Main plugin object.
     * @param Plugin_Constants           $constants   Plugin constants object.
     * @return Helper_Functions
     */
    public static function get_instance( Abstract_Main_Plugin_Class $main_plugin = null , Plugin_Constants $constants ) {

        if ( !self::$_instance instanceof self )
            self::$_instance = new self( $main_plugin , $constants );
        
        return self::$_instance;

    }
    


    
    /*
    |--------------------------------------------------------------------------
    | Helper Functions
    |--------------------------------------------------------------------------
    */

    /**
     * Write data to plugin log file.
     *
     * @since 2.0
     * @access public
     *
     * @param mixed Data to log.
     */
    public function write_debug_log( $log )  {

        error_log( "\n[" . current_time( 'mysql' ) . "]\n" . $log . "\n--------------------------------------------------\n" , 3 , $this->_constants->LOGS_ROOT_PATH . 'debug.log' );

    }

    /**
     * Check if current user is authorized to manage the plugin on the backend.
     * 
     * @since 2.0
     * @access public
     *
     * @param WP_User $user WP_User object.
     * @return boolean True if authorized, False otherwise.
     */
    public function current_user_authorized( $user = null ) {

        // Array of roles allowed to access/utilize the plugin
        $admin_roles = apply_filters( 'ucfw_admin_roles' , array( 'administrator' ) );

        if ( is_null( $user ) )
            $user = wp_get_current_user();

        if ( $user->ID )
            return count( array_intersect( ( array ) $user->roles , $admin_roles ) ) ? true : false;
        else
            return false;
        
    }

    /**
     * Returns the timezone string for a site, even if it's set to a UTC offset
     * 
     * Adapted from http://www.php.net/manual/en/function.timezone-name-from-abbr.php#89155
     * 
     * Reference:
     * http://www.skyverge.com/blog/down-the-rabbit-hole-wordpress-and-timezones/
     * 
     * @since 2.0
     * @access public
     * 
     * @return string Valid PHP timezone string
     */
    public function get_site_current_timezone() {

        // if site timezone string exists, return it
        if ( $timezone = trim( get_option( 'timezone_string' ) ) )
            return $timezone;

        // get UTC offset, if it isn't set then return UTC
        $utc_offset = trim( get_option( 'gmt_offset', 0 ) );

        if ( filter_var( $utc_offset , FILTER_VALIDATE_INT ) === 0 || $utc_offset === '' || is_null( $utc_offset ) )
            return 'UTC';

        return $this->convert_utc_offset_to_timezone( $utc_offset );

    }

    /**
     * Convert UTC offset to timezone.
     *
     * @since 1.2.0
     * @access public
     *
     * @param float/int/string $utc_offset UTC offset.
     * @return string valid PHP timezone string
     */
    public function convert_utc_offset_to_timezone( $utc_offset ) {

        // adjust UTC offset from hours to seconds
        $utc_offset *= 3600;

        // attempt to guess the timezone string from the UTC offset
        if ( $timezone = timezone_name_from_abbr( '' , $utc_offset , 0 ) )
            return $timezone;
        
        // last try, guess timezone string manually
        $is_dst = date( 'I' );

        foreach ( timezone_abbreviations_list() as $abbr )
            foreach ( $abbr as $city )
                if ( $city[ 'dst' ] == $is_dst && $city[ 'offset' ] == $utc_offset )
                    return $city[ 'timezone_id' ];
        
        // fallback to UTC
        return 'UTC';

    }

    /**
     * Get all user roles.
     *
     * @since 2.0
     * @access public
     *
     * @global WP_Roles $wp_roles Core class used to implement a user roles API.
     *
     * @return array Array of all site registered user roles. User role key as the key and value is user role text.
     */
    public function get_all_user_roles() {

        global $wp_roles;
        return $wp_roles->get_names();

    }

    /**
     * Check validity of a save post action.
     *
     * @since 2.0
     * @access private
     *
     * @param int    $post_id   Id of the coupon post.
     * @param string $post_type Post type to check.
     * @return bool True if valid save post action, False otherwise.
     */
    public function check_if_valid_save_post_action( $post_id , $post_type ) {

        if ( wp_is_post_autosave( $post_id ) || wp_is_post_revision( $post_id ) || !current_user_can( 'edit_page' , $post_id ) || get_post_type() != $post_type || empty( $_POST ) )
            return false;
        else
            return true;
        
    }

    /**
     * Utility function that determines if a plugin is active or not.
     * Reference: https://developer.wordpress.org/reference/functions/is_plugin_active/
     * 
     * @since 2.0
     * @access public
     *
     * @param string $plugin_basename Plugin base name. Ex. woocommerce/woocommerce.php
     * @return boolean Returns true if active, false otherwise.
     */
    public function is_plugin_active( $plugin_basename ) {

        // Makes sure the plugin is defined before trying to use it
        if ( !function_exists( 'is_plugin_active' ) )
            include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

        return is_plugin_active( $plugin_basename );

    }

    /**
     * Utility function that determines whether the plugin is active for the entire network.
     * Reference: https://developer.wordpress.org/reference/functions/is_plugin_active_for_network/
     *
     * @since 2.0
     * @access public
     * 
     * @param string $plugin_basename Plugin base name. Ex. woocommerce/woocommerce.php
     * @return boolean Returns true if active for the entire network, false otherwise.
     */
    public function is_plugin_active_for_network( $plugin_basename ) {

        // Makes sure the function is defined before trying to use it
        if ( ! function_exists( 'is_plugin_active_for_network' ) )
            require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

        $network_wide = is_plugin_active_for_network( $plugin_basename );

    }

    /**
     * Gets the ACFWF helper class.
     * 
     * @since 2.0
     * @access public
     * 
     * @return \ACFWF\Helpers\Helper_Function
     */
    public function acfwf_helper() {

        return \ACFWF()->helpers[ 'Helper_Functions' ];
    }

    /**
     * Get advanced coupon object.
     * 
     * @since 2.0
     * @access public
     * 
     * @param mixed $code WC_Coupon ID, code or object.
     * @return Advanced_Coupon
     */
    public function get_advanced_coupon( $code ) {

        return new Advanced_Coupon( $code );
    }

    /**
     * Get my account endpoint.
     * 
     * @since 1.9
     * @access private
     * 
     * @return string Loyalty program my account endpoint.
     */
    public function get_loyalprog_myaccount_endpoint() {

        $default  = 'acfw-loyalty-program';
        $endpoint = apply_filters( 'acfw_loyalty_program_myaccount_endpoint' , $default );

        return $endpoint ? $endpoint : $default;
    }

    /**
     * Generate random string.
     * 
     * @since 1.9
     * @access public
     * 
     * @param int $length String length.
     * @return string Random string.
     */
    public function random_str( $length ) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
    
    /**
     * Get scheduler date field value.
     * 
     * @since 2.1
     * @access public
     * 
     * @param array|string $field Field post value.
     * @return string Date string value (Y-m-d H:i:s).
     */
    public function get_scheduler_date_field_value( $field ) {

        $date   = is_array( $field ) && isset( $field[ 'date' ] ) ? $field[ 'date' ] : '';
        $hour   = $date && isset( $field[ 'hour' ] ) ? sprintf( '%02d' , (int) $field[ 'hour' ] ) : '00';
        $minute = $date && isset( $field[ 'minute' ] ) ? sprintf( '%02d' , (int) $field[ 'minute' ] ) : '00';
        $second = $date && isset( $field[ 'second' ] ) ? sprintf( '%02d' , (int) $field[ 'second' ] ) : '00';

        return $date ? sanitize_text_field( sprintf( '%s %s:%s:%s' , $date , $hour , $minute , $second ) ) : '';
    }

}
