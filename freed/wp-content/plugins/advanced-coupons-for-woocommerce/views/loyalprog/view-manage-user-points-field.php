<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<h2><?php _e( 'Loyalty Programs: Manage User Points' , 'advanced-coupons-for-woocommerce' ); ?></h2>

<table class="form-table">
    <tr>
        <th><?php _e( 'User points' , 'advanced-coupons-for-woocommerce' ); ?></th>
        <td id="acfw-lp-manager-user-points" data-refreshnonce="<?php echo wp_create_nonce( 'acfw_loyalprog_user_refresh_points' ); ?>">
            <label>
                <input class="user-points" type="number" name="<?php echo $field_name; ?>" value="<?php echo $user_points; ?>" min="0" data-current="<?php echo $user_points; ?>"> 
                <span><?php echo $points_name; ?></span>
            </label>
            <div class="expire-message"></div>
            <div class="adjustment-message"></div>
        </td>
    </tr>
</table>

<style type="text/css">
#acfw-lp-manager-user-points .expire-message {
    margin: 5px 0;
}
#acfw-lp-manager-user-points .adjustment-message {
    display: none;
    margin: 5px 0;
    max-width: 350px;
    padding: 5px 10px;
    color: #ffffff;
}
#acfw-lp-manager-user-points .adjustment-message.add {
    display: block;
    background: #46B450;
}
#acfw-lp-manager-user-points .adjustment-message.deduct {
    display: block;
    background: #DC3232;
}
</style>

<script type="text/javascript">
jQuery( document ).ready( function($) {

    var $wrap = $( '#acfw-lp-manager-user-points' );

    var funcs = {

        init : function() {

            funcs.refresh_user_points();
            $wrap.on( "change keyup" , "input.user-points" , funcs.calc_points_change );
        },

        refresh_user_points : function() {
            
            var $input = $wrap.find( "input.user-points" );

            $.post( ajaxurl , {
                action : 'acfw_lp_refresh_user_points',
                nonce  : $wrap.data( 'refreshnonce' ),
                user   : <?php echo $user->ID ?>,
                admin  : true
            } , function( response ) {

                if ( response.status == 'success' ) {

                    $input.val( response.user_points );
                    $input.data( "current" , response.user_points );
                    $wrap.find( '.expire-message' ).html( response.expire_msg );
                    
                } else {
                    // TODO: change to vex.
                    alert( response.error_msg );
                }

            } , 'json' );
        },

        calc_points_change : function() {

            var $this    = $(this),
                $message = $wrap.find( '.adjustment-message' ),
                points   = parseInt( $this.val() ),
                current  = parseInt( $this.data( "current" ) ),
                adjust, type;

            if ( current == points ) {

                $message.html( '' );
                $message.removeClass( 'add deduct' );

            } else if ( current > points ) {
                
                adjust = current - points;
                $message.html( '<strong>' + adjust + '</strong> <?php _e( 'Point(s) will be deducted' , 'advanced-coupons-for-woocommerce' ); ?>' );
                $message.removeClass( 'add' ).addClass( 'deduct' );

            } else {

                adjust = points - current;
                $message.html( '<strong>' + adjust + '</strong> <?php _e( 'Point(s) will be added' , 'advanced-coupons-for-woocommerce' ); ?>' );
                $message.removeClass( 'deduct' ).addClass( 'add' );
            }
        }
    };

    funcs.init();
} );
</script>