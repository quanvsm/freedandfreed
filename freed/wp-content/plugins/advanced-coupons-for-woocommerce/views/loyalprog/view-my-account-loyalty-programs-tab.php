<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<div id="acfw-loyalty-programs-myaccount" class="woocommerce">

    <div class="user-points">
        <?php echo $points_name . ':'; ?>
        <strong><?php echo $user_points ? $user_points : 0; ?></strong>
        <em><?php echo sprintf( __( '(worth %s)' , 'advanced-coupons-for-woocommerce' ) , $points_worth ); ?></em>
        <div class="expiry-note"></div>
    </div>

    <div class="redeem-points-form">
        <form id="redeem-points-form" method="post">
            <label class="instruction"><?php echo sprintf( __( 'Redeem %s: (%s1 = %s)' , 'advanced-coupons-for-woocommerce' ) , $points_name , $minimum_text , $single_point ); ?></label>
            <p class="form-row form-row-first" id="redeem_points_field">
                <span class="woocommerce-input-wrapper">
                    <input type="number" class="input-text" name="redeem_points" id="redeem_points" autocomplete="off" value="0" min="<?php echo $minimum; ?>" max="<?php echo $user_points; ?>">
                </span>
                <span class="equals">=</span>
                <span class="woocommerce-input-wrapper amount-preview-field">
                    <input type="text" class="input-text wc_input_price" id="redeem_points_worth" autocomplete="off" value="">
                    <span class="currency"><?php echo get_woocommerce_currency_symbol(); ?></span>
                </span>
            </p>
            <p class="form-row form-row-last" id="redeem_submit_field">
                <span class="woocommerce-input-wrapper">
                    <button class="button alt" type="submit" disabled><?php _e( 'Redeem' , 'advanced-coupons-for-woocommerce' ); ?></button>
                </span>
            </p>
            <?php wp_nonce_field( 'acfw_redeem_points_for_user' ); ?>
            <input type="hidden" name="action" value="acfw_redeem_points_for_user">
        </form>
    </div>

    <div class="user-redeemed-coupons">
        <table>
            <thead>
                <tr>
                    <th class="coupon_code"><?php _e( 'Coupon Code' , 'advanced-coupons-for-woocommerce' ); ?></th>
                    <th class="coupon_amount"><?php _e( 'Discount Amount' , 'advanced-coupons-for-woocommerce' ); ?></th>
                    <th class="coupon_created"><?php _e( 'Date Created' , 'advanced-coupons-for-woocommerce' ); ?></th>
                    <th class="actions"></th>
                </tr>
            </thead>
            <tbody>
                <?php if ( is_array( $user_coupons ) && ! empty( $user_coupons ) ) : ?>
                    <?php foreach( $user_coupons as $coupon ) : ?>
                        <tr>
                            <td class="coupon_code">
                                <?php echo esc_html( $coupon->code ); ?>
                            </td>
                            <td class="coupon_amount">
                                <?php echo wc_price( apply_filters( 'acfw_filter_amount' , $coupon->amount ) ); ?>
                            </td>
                            <td class="coupon_created">
                                <?php echo date( 'F j, Y g:i a', strtotime( $coupon->date ) ); ?>
                            </td>
                            <td class="actions">
                                <?php if ( $is_uc_module ) : ?>
                                    <a class="button" href="<?php echo get_permalink( $coupon->ID ); ?>">
                                        <?php _e( 'Apply' , 'advanced-coupons-for-woocommerce' ); ?>
                                    </a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr class="no-coupon-tr">
                        <td class="no-coupon" colspan="4"><?php _e( 'No coupons available.' , 'advanced-coupons-for-woocommerce' ); ?></td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>

    <div class="acfw-overlay" style="background-image: url(<?php echo $spinner_img; ?>);"></div>
</div>