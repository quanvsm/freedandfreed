<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<div class="loyalprog-earn-message" <?php echo $display; ?>>
    <?php wc_print_notice( $notice , 'notice' ); ?>
</div>

<?php if ( is_a( $product , 'WC_Product_Variable' ) ) : ?>
<script type="text/javascript">
jQuery( document ).ready(function($) {

    var $wrapper       = $( '.loyalprog-earn-message' ),
        $notice        = $wrapper.find( ".woocommerce-info" ),
        $form          = $( 'form.variations_form' ),
        message        = '<?php echo $message; ?>',
        multiplier     = <?php echo $multiplier ?>,
        currency_ratio = <?php echo apply_filters( 'acfw_filter_amount' , 1 ); ?>,
        decSymbol      = '<?php echo wc_get_price_decimal_separator(); ?>',
        thouSymbol     = '<?php echo wc_get_price_thousand_separator(); ?>',
        includeTaxCalc = '<?php echo $include_tax; ?>',
        taxDisplay     = '<?php echo get_option( 'woocommerce_tax_display_shop' , 'incl' ); ?>';

    var funcs = {

        init : function() {
            $notice.html( '' );
            $form.on( 'woocommerce_variation_has_changed' , funcs.updateNotice );
            setTimeout(function() {
                $form.trigger( 'woocommerce_variation_has_changed' );
            }, 1000);
        },

        calculatePoints : function( price ) {
            return parseInt( Math.round( ( price / currency_ratio ) * multiplier ) );
        },

        getWholesalePrice : function( variation ) {

            if ( includeTaxCalc == 'yes' && variation.wholesale_price )
                return taxDisplay == 'incl' ? variation.wholesale_price : variation.wholesale_price_raw;
            else if ( includeTaxCalc != 'yes' && variation.wholesale_price_with_no_tax )
                return variation.wholesale_price_with_no_tax;

            return false;
        },

        getPrice : function( variation ) {

            if ( includeTaxCalc == 'yes' )
                return variation.display_price_with_tax;
            else
                return variation.display_price_no_tax;
        },

        updateNotice : function() {

            var variationId = $form.find( 'input[name="variation_id"]' ).val();
            var variations   = $form.data( 'product_variations' ).filter( function( v ) {
                return v.variation_id == variationId;
            } );

            if ( ! variationId || ! variations.length ) {
                $notice.html( '' );
                $wrapper.hide();
                return;
            }

            var variation = variations[0];
            var wprice    = funcs.getWholesalePrice( variation );
            var price     = wprice ? wprice : funcs.getPrice( variation );
            var points    = funcs.calculatePoints( price );

            $notice.html( message.replace( '{points}' , points ) );
            $wrapper.show();
        }

    };

    funcs.init();

});
</script>
<?php endif; ?>