<?php if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<div class="wrap woocommerce">

    <h1><?php _e( 'Loyalty Program' , 'advanced-coupons-for-woocommerce' ); ?></h1>
    <p><?php _e( 'Run a loyalty program on your store to reward your customers with points and reward them with coupons.' , 'advanced-coupons-for-woocommerce' ); ?></p>
    
    <form method="post" id="mainform" action="" enctype="multipart/form-data">
        <?php \WC_Admin_Settings::show_messages(); ?>
        <?php $settings->output(); ?>
        <p class="submit">
            <button 
                name="save" 
                class="button-primary woocommerce-save-button" 
                type="submit" 
                value="<?php esc_attr_e( 'Save changes', 'advanced-coupons-for-woocommerce' ); ?>"
            >
                <?php esc_html_e( 'Save changes', 'advanced-coupons-for-woocommerce' ); ?>
            </button>
            <?php wp_nonce_field( 'woocommerce-settings' ); ?>
        </p>
    </form>
</div>

<script type="text/javascript">
jQuery(document).ready(function($) {
    $('body').trigger( 'wc-enhanced-select' );
    $( document.body ).on( 'init_tooltips', function() {
		$( '.woocommerce-help-tip' ).tipTip( {
			'attribute': 'data-tip',
			'fadeIn': 50,
			'fadeOut': 50,
			'delay': 200,
			'defaultPosition': 'top'
		} );
	} ).trigger( 'init_tooltips' );
});
</script>