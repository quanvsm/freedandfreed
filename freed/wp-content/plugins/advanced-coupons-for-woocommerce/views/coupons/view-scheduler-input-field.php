<?php if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$hour   = isset( $time[0] ) ? esc_attr( $time[0] ) : '';
$minute = isset( $time[1] ) ? esc_attr( $time[1] ) : '';
$second = isset( $time[2] ) ? esc_attr( $time[2] ) : '00';
?>

<p class="form-field <?php echo esc_attr( $field[ 'id' ] ); ?>_field <?php echo esc_attr( $field[ 'wrapper_class' ] ); ?>">

    <label for="<?php echo esc_attr( $field[ 'id' ] ); ?>"><?php echo wp_kses_post( $field['label'] ); ?></label>

    <?php  if ( ! empty( $field['description'] ) && false !== $field['desc_tip'] ) :  
        echo wc_help_tip( $field[ 'description' ] );
    endif; ?>

    <span 
        class="date-time-field"
        data-date="<?php echo esc_attr( $date ); ?>"
        data-hour="<?php echo $hour; ?>"
        data-minute="<?php echo $minute; ?>"
        data-second="<?php echo $second; ?>"
    >
        <input type="<?php echo esc_attr( $field[ 'type' ] ); ?>" 
            class="date-field <?php echo esc_attr( $field[ 'class' ] ); ?>" 
            style="<?php echo esc_attr( $field['style'] ); ?>" 
            name="<?php echo esc_attr( $field[ 'name' ] ); ?>[date]" 
            id="<?php echo esc_attr( $field[ 'id' ] ); ?>" 
            value="<?php echo esc_attr( $date ); ?>" 
            placeholder="<?php echo esc_attr( $field['placeholder'] ); ?>"
            <?php echo implode( ' ', $custom_attributes ); ?>
            autocomplete="off" />
        <span>@</span>
        <input type="number" class="date-hour" placeholder="h" name="<?php echo esc_attr( $field[ 'name' ] ); ?>[hour]" min="0" max="23" step="1" value="<?php echo $hour; ?>" pattern="([01]?[0-9]{1}|2[0-3]{1})">
        <input type="number" class="date-minute" placeholder="m" name="<?php echo esc_attr( $field[ 'name' ] ); ?>[minute]" min="0" max="59" step="1" value="<?php echo $minute; ?>" pattern="[0-5]{1}[0-9]{1}">
        <input type="hidden" name="<?php echo esc_attr( $field[ 'name' ] ); ?>[second]" value="<?php echo $second; ?>">
    </span>

    <?php if ( ! empty( $field[ 'description' ] ) && false === $field[ 'desc_tip' ] ) : ?>
        <span class="description"><?php echo wp_kses_post( $field['description'] ); ?></span>
    <?php endif; ?>

    <a 
        class="clear-scheduler-fields dashicons-before dashicons-no" 
        href="javascript:void(0);" 
        alt="<?php _e( 'Clear field values' , 'advanced-coupons-for-woocommerce' ); ?>"
        title="<?php _e( 'Clear field values' , 'advanced-coupons-for-woocommerce' ); ?>"
    ></a>
</p>